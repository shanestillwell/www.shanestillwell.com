---
date: '2024-08-09T09:49:32-05:00'
title: Database is Culture
description: Why database design is so important
pubDate: '2024-08-09T09:49:32-05:00'
---

> the database is more than a place to store data, it is the culture maker

https://jimmyhmiller.github.io/ugliest-beautiful-codebase
