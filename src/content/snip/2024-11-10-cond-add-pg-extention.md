---
date: '2024-11-10T09:49:32-05:00'
title: Conditionally Add Postgres Extension
description: Conditionally Add Postgres Extension
pubDate: '2024-11-10T09:49:32-05:00'
tags:
- PostgreSQL
---

When working with AWS Aurora, you might want to qualify if an extension is available. In this instance, we are checking if the extension is available from the catalog of extensions.

```sql
DO $$
BEGIN
   IF EXISTS (
      SELECT
      FROM pg_available_extensions
      WHERE name = 'aws_s3') THEN
      CREATE EXTENSION IF NOT EXISTS aws_s3 CASCADE;
   END IF;
END
$$;
```
