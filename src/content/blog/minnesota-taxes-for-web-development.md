---
date: '2008-07-18T14:01:46-05:00'
description: Minnesota Taxes for Web Development
id: 24
pubDate: '2008-07-18T14:01:46-05:00'
tags:
- Good Ideas
title: Minnesota Taxes for Web Development
---

From the Minnesota Department of Revenue on Web Development Taxes

> **Web page development** is not taxable. The service provider must pay sales or use tax on items purchased to produce the web page, such as photography, software packages, graphics, sound, or video.

The document can be found at 
[http://www.taxes.state.mn.us/taxes/sales/publications/fact_sheets_by_name/content/BAT_1100086.pdf](http://www.taxes.state.mn.us/taxes/sales/publications/fact_sheets_by_name/content/BAT_1100086.pdf "")