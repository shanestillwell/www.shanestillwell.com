---
date: '2008-12-04T12:38:57-05:00'
description: Jquery Slideshow Plugin for Drupal
id: 40
pubDate: '2008-12-04T12:38:57-05:00'
tags:
- jQuery
- Programming
title: Jquery Slideshow Plugin for Drupal
---

I am now a module maintainer for a Drupal plugin I wrote ([Jquery Slideshow](http://drupal.org/project/jquery_slideshow)). It takes CCK Imagefields and creates a nice JavaScript slide show from them using the [Cycle](http://malsup.com/jquery/cycle/) plugin for Jquery.

It took some time to write, but it's not that complicated of a module. The module has some settings for using [Imagecache](http://drupal.org/project/imagecache) and different types and speeds of transitions.

Currently it only works on Drupal 5 with Imagecache 1.x, but I'll upgrade it to Drupal 6 using CCK 2 and Imagecache 2.