---
date: '2010-11-29T13:48:35-05:00'
description: Doctrine Query Cache driver memcache when using ZFDoctrine
id: 118
pubDate: '2010-11-29T13:48:35-05:00'
tags:
- Programming
title: Doctrine Query Cache driver memcache when using ZFDoctrine
---

According to Doctrine's documentation, you should always utilize a Query Cache.

`http://www.doctrine-project.org/documentation/manual/1_2/en/caching:query-cache-&amp;-result-cache` Defunct Link

You should always use query cache in a production environment. That said, you can easily use it during development, too. Whenever you change a DQL query and execute it the first time Doctrine sees that it has been modified and will therefore create a new cache entry, so you don't even need to invalidate the cache.

That said, you need to add the following configuration options in
<pre>application.ini</pre>
If you have a standard Memcached running locally on port 11211 then the following should work.
`
resources.doctrine.manager.attributes.attr_query_cache.driver = memcache
resources.doctrine.manager.attributes.attr_query_cache.options.servers.host = localhost
resources.doctrine.manager.attributes.attr_query_cache.options.servers.port = 11211
resources.doctrine.manager.attributes.attr_query_cache.options.servers.persistent = true
resources.doctrine.manager.attributes.attr_query_cache.options.compression = false
`