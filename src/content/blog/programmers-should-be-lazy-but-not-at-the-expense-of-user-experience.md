---
date: '2012-08-07T20:31:24-05:00'
description: Programmers should be lazy, but not at the expense of user experience
id: 356
pubDate: '2012-08-07T20:31:24-05:00'
tags:
- Programming
title: Programmers should be lazy, but not at the expense of user experience
---

Has this every happened to you. A form wants you to enter in your phone number... OK, let's be honest, there are 20 different ways you can enter in your phone, but as far as a computer is concerned it's just interested in the numbers.

So that means stripping out the non-numeric characters is trivial.... 

So why do so many sites do like this stupid site.... make you enter in a phone the way THEY want you.

![](/images/programmers-should-be-lazy-but-not-at-the-expense-of-user-experience-Screen-Shot-2012-08-07-at-3.10.56-PM.png)

This example is so trivial to solve... a quick search can find you the answer in any language ([PHP](http://stackoverflow.com/a/4708314/179335), [JavaScript](http://stackoverflow.com/a/8760108/179335))

Please stop punishing users who sometimes have no choice but to use your software.
