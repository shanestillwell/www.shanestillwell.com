---
date: '2013-09-20T13:29:40-05:00'
description: Good Bye WordPress, Hello Hexo
pubDate: '2013-09-20T13:29:40-05:00'
tags: []
title: Good Bye WordPress, Hello Hexo
---

## ShaneStillwell.com Moves to [Hexo](https://hexo.io/)
Posts from here onward will be done using the blog engine Hexo. Read on for more of the journey, or just skip to some [cat videos](http://www.google.com/search?q=cat%20videos).

## A Little Blog History

When [www.shanestillwell.com](http://www.shanestillwell.com) first appeared on the inter-webs, it was actually a Wiki. I can't remember how long that lasted in its form, but maybe around 2006, it became a [Drupal](http://drupal.org) blog. The intent was to log problems I had on my journey through the techno-sphere. It was only for me, a sort of log book of issues that I had worked hard to solve and didn't want to lose that knowledge (or resolve the same problems... raise your hand if you've done that). Deja Vu. 

Under Drupal the blog thrived and flourished. I was writing posts almost once a week and after a few years had about 100 people a day visiting the blog. I was happy to help others and even thrilled that my blog topped the Google results for certain key phrases. 

Then life got busy. It happens. I started blogging less, I started using Drupal much less. Finally, a year or two ago I migrated my blog to [WordPress](http://www.wordpress.org) since in my opinion WordPress had won the CMS war with a much more polished admin back-end and easier to grok plugins, themes, etc. But by this time in my life I was coding custom Zend Framework apps and didn't really fancy too much with WordPress. It served a purpose, but I was not excited about it.

Then I learned _Vim_, and fell out of love with writing in any other editor (especially web based WYSIWYGs and the like). I wanted to code in Vim, and so I converted from using PHPStorm (a great IDE by the way) to Vim and life was peachy. I then learned how easy, fun, and forward thinking Markdown was to write. 

So let's cut this long history short by saying that I've found a blog engine that allows me to accomplish the following objectives  

* Use Vim to edit my blog posts 
* Use Markdown as the "markup" of choice 
* Host it off of a static based site (S3, Github, Heroku)
* Node.js based (ideal, but not required)

## Contenders

I've been toying with the idea of switching the blog for months. I kept looking for an engine that would fit the bill, but alas I didn't see one that I liked. I even entertained the idea of writing my own (that's a common thought in a programmer's everyday life). Other developers were using Octopress, Jekyll, and some other Ruby based static sties, but they didn't get me excited. I then stumbled upon Poet, it look interesting, but it was an ExpressJS based site, and I _really_ wanted to have a statically generated site.  

## Hexo 

I'm really happy I found Hexo. One of the worst occurrences I run into is not being able to find what I'm looking for on the web. There might be an awesome piece of software that fits your needs perfectly, but if you can't find it, well, it might as well not exist. 

## What I like about Hexo 

### Node.js 

This was not really a requirement, but a HUGE plus in my mind. Node.js is so freaking hot right now, it's painful just touching the keys while working in a Node.js project.

### Markdown 

Yeah, this was a requirement, and it does it well. Better yet, it uses the `marked` module which supports [Github Flavored Markdown]()

### Statically Generated HTML

This just means, that you run the command `hexo generate` and it makes an HTML version of the site for you. No server side language needed. That way you can host off of Github Pages, Heroku, even S3. This makes the page blazingly fast (nothing really beats the performance of static files served by a web server, except an Single Page App). Also, static pages are more suitable for permalink / long lasting posts (No Database Needed).

### Built In Server

You can run `hexo server` and get a local web server to view your site live, so you can preview what your post will look like. I've saved and previewed this article at least 10 times so far to this point.


### WordPress Migration Plugin

For the record, it was a lot more painful going from Drupal -> WordPress, than from WordPress ->  Hexo. Hexo provides a plugin to help you migrate your blogs. For WordPress, it's as simple as the following. 

1. Go into the WordPress admin. Select `Tools -> Export`.
    1. This will export your data to an XML file locally to your computer
1. Go to the source directory of your hexo blog and run the command `hexo migrate WordPress SOURCE`, where SOURCE is the XML file you downloaded from WordPress.
1. I had a few problem lines in my XML. After I manually deleted them, the migration worked flawlessly.  

## Conclusion

I'm excited to start blogging again, hence the long post. Look for more blog posts from me in the coming future now that I can write them in Vim, Markdown and push with or without an internet connection. (For the record, I'm 38K feet in the air right now with no interweb connection).


## Update  

The new blogging platform [Ghost](http://ghost.org) looks very interesting, but I'm married to Vim.
