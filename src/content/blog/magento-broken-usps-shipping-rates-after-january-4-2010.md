---
date: '2010-01-18T21:40:08-05:00'
description: Magento broken USPS shipping rates after January 4, 2010
id: 83
pubDate: '2010-01-18T21:40:08-05:00'
tags:
- Programming
- Magento
title: Magento broken USPS shipping rates after January 4, 2010
---

Our great USPS decided to make some slight name changes to their delivery methods.  So you need to change a core file
`app/code/core/Mage/Usa/etc/config.xml`

You'll need to change any instance of 'First Class' -> 'First-Class' and get ride of '(EMS)'

```
<usps>
    <active>0</active>
    <sallowspecific>0</sallowspecific>
    <allowed_methods>Bound Printed Matter,Express Mail,Express Mail Flat-Rate Envelope,Express Mail Flat-Rate Envelope Hold For Pickup,Express Mail Flat-Rate Envelope Sunday/Holiday Guarantee,Express Mail Hold For Pickup,Express Mail International,Express Mail International (EMS) Flat-Rate Envelope,Express Mail PO to PO,Express Mail Sunday/Holiday Guarantee,First Class Mail International Large Envelope,First Class Mail International Letters,First Class Mail International Package,First Class,First Class Mail,First Class Mail Flat,First Class Mail International,First Class Mail Letter,First Class Mail Parcel,Global Express Guaranteed,Global Express Guaranteed Non-Document Non-Rectangular,Global Express Guaranteed Non-Document Rectangular,Library Mail,Media Mail,Parcel Post,Priority Mail,Priority Mail Flat-Rate Box,Priority Mail Flat-Rate Envelope,Priority Mail International,Priority Mail International Flat-Rate Box,Priority Mail International Flat-Rate Envelope,Priority Mail International Large Flat-Rate Box,Priority Mail Large Flat-Rate Box,USPS GXG Envelopes</allowed_methods>
    <container>VARIABLE</container>
    <cutoff_cost></cutoff_cost>
    <free_method></free_method>
    <gateway_url>http://production.shippingapis.com/ShippingAPI.dll</gateway_url>
    <handling></handling>
    <machinable>true</machinable>
    <methods>Bound Printed Matter,Express Mail,Express Mail Flat-Rate Envelope,Express Mail Flat-Rate Envelope Hold For Pickup,Express Mail Flat-Rate Envelope Sunday/Holiday Guarantee,Express Mail Hold For Pickup,Express Mail International (EMS),Express Mail International (EMS) Flat-Rate Envelope,Express Mail PO to PO,Express Mail Sunday/Holiday Guarantee,First Class Mail International Large Envelope,First Class Mail International Letters,First Class Mail International Package,First Class,First Class Mail,First Class Mail Flat,First Class Mail International,First Class Mail Letter,First Class Mail Parcel,Global Express Guaranteed,Global Express Guaranteed Non-Document Non-Rectangular,Global Express Guaranteed Non-Document Rectangular,Library Mail,Media Mail,Parcel Post,Priority Mail,Priority Mail Flat-Rate Box,Priority Mail Flat-Rate Envelope,Priority Mail International,Priority Mail International Flat-Rate Box,Priority Mail International Flat-Rate Envelope,Priority Mail International Large Flat-Rate Box,Priority Mail Large Flat-Rate Box,USPS GXG Envelopes</methods>
    <model>usa/shipping_carrier_usps</model>
    <size>REGULAR</size>
    <title>United States Postal Service</title>
    <userid backend_model="adminhtml/system_config_backend_encrypted"/>
    <specificerrmsg>This shipping method is currently unavailable. If you would like to ship using this shipping method, please contact us.</specificerrmsg>
    <max_package_weight>70</max_package_weight>
    <handling_type>F</handling_type>
    <handling_action>O</handling_action>
</usps>
```

change to 

```
<usps>
    <active>0</active>
    <sallowspecific>0</sallowspecific>
    <allowed_methods>Bound Printed Matter,Express Mail,Express Mail Flat-Rate Envelope,Express Mail Flat-Rate Envelope Hold For Pickup,Express Mail Flat-Rate Envelope Sunday/Holiday Guarantee,Express Mail Hold For Pickup,Express Mail International,Express Mail International (EMS) Flat-Rate Envelope,Express Mail PO to PO,Express Mail Sunday/Holiday Guarantee,First-Class Mail International Large Envelope,First-Class Mail International Letters,First-Class Mail International Package,First-Class,First-Class Mail,First-Class Mail Flat,First-Class Mail International,First-Class Mail Letter,First-Class Mail Parcel,Global Express Guaranteed,Global Express Guaranteed Non-Document Non-Rectangular,Global Express Guaranteed Non-Document Rectangular,Library Mail,Media Mail,Parcel Post,Priority Mail,Priority Mail Flat-Rate Box,Priority Mail Flat-Rate Envelope,Priority Mail International,Priority Mail International Flat-Rate Box,Priority Mail International Flat-Rate Envelope,Priority Mail International Large Flat-Rate Box,Priority Mail Large Flat-Rate Box,USPS GXG Envelopes</allowed_methods>
    <container>VARIABLE</container>
    <cutoff_cost></cutoff_cost>
    <free_method></free_method>
    <gateway_url>http://production.shippingapis.com/ShippingAPI.dll</gateway_url>
    <handling></handling>
    <machinable>true</machinable>
    <methods>Bound Printed Matter,Express Mail,Express Mail Flat-Rate Envelope,Express Mail Flat-Rate Envelope Hold For Pickup,Express Mail Flat-Rate Envelope Sunday/Holiday Guarantee,Express Mail Hold For Pickup,Express Mail International (EMS),Express Mail International (EMS) Flat-Rate Envelope,Express Mail PO to PO,Express Mail Sunday/Holiday Guarantee,First-Class Mail International Large Envelope,First-Class Mail International Letters,First-Class Mail International Package,First-Class,First-Class Mail,First-Class Mail Flat,First-Class Mail International,First-Class Mail Letter,First-Class Mail Parcel,Global Express Guaranteed,Global Express Guaranteed Non-Document Non-Rectangular,Global Express Guaranteed Non-Document Rectangular,Library Mail,Media Mail,Parcel Post,Priority Mail,Priority Mail Flat-Rate Box,Priority Mail Flat-Rate Envelope,Priority Mail International,Priority Mail International Flat-Rate Box,Priority Mail International Flat-Rate Envelope,Priority Mail International Large Flat-Rate Box,Priority Mail Large Flat-Rate Box,USPS GXG Envelopes</methods>
    <model>usa/shipping_carrier_usps</model>
    <size>REGULAR</size>
    <title>United States Postal Service</title>
    <userid backend_model="adminhtml/system_config_backend_encrypted"/>
    <specificerrmsg>This shipping method is currently unavailable. If you would like to ship using this shipping method, please contact us.</specificerrmsg>
    <max_package_weight>70</max_package_weight>
    <handling_type>F</handling_type>
    <handling_action>O</handling_action>
</usps>
```

Of course you'll want to reload your cache and then you need to reselect the methods in System > Configuration > Shipping Methods > USPS > Allowed Methods

![Screen shot 2010-01-18 at 9.39.17 AM.png](/images/Screen shot 2010-01-18 at 9_39_17 AM.png)

References:
http://www.magentocommerce.com/boards/viewthread/15659/#t203165
