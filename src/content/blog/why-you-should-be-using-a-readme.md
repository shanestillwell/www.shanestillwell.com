---
date: '2014-10-10T14:44:56-05:00'
description: Why you should be using a README
pubDate: '2014-10-10T14:44:56-05:00'
tags: []
title: Why you should be using a README
---

You're one of the elite and have long since kept your code in [git](http://git-scm.com). You know how to branch, commit, and merge like a [boss](http://knowyourmeme.com/memes/like-a-boss). Yeah, yeah, you comment your code (we known, your code is so solid it doesn't really need comments).

## But What About Process? ##

This is not as easy to describe. I've seen Google Docs, Confluence, and everything under the sun to document the details about an app. For years Github has suggested to create a README.md file when you create a new repo, why not try it sometime.

## Advantages of using a README.md for documentation ##

1. Changes with the code. Docs will always be up-to-date
1. It resides with the code, no hunting around
1. You can use Markdown and Github will render it nicely on the web

## What should be in your README.md ##

First, your README should give a general rundown of the app. Do you have any workflows to deploy the app that need to be followed a certain way? Think about it, you don't remember much, why not write down what you need to do as a reminder to your future self what in the world you were doing.

Another good thing to put in the README is links to other types of documentation. I like to make Google Drawings, then linik to the PNG file in my READMEs, they get updated as you update the Google Drawing (ooops, kind of broke my own advantage about being source controller, crap).

## Bonus

You can have a README in every folder of your app. Maybe talk about what the files in this folder do, why are they grouped together? Are they all models, controllers, config files, or what.


## Resources

* [Readme Driven Development](http://tom.preston-werner.com/2010/08/23/readme-driven-development.html)
