---
date: '2010-02-11T09:20:35-05:00'
description: Fix Slow SSH login time
id: 92
pubDate: '2010-02-11T09:20:35-05:00'
tags:
- Server Admin
title: Fix Slow SSH login time
---

Interesting that since moving to a new ISP my login times to a particular Linux server is 20-30 seconds.  I figured it had to do with reverse DNS somehow.  Sure enough....

/etc/ssh/sshd_config
`
UseDNS no
`

That made the logins quick again and that makes me HAPPY!

Credit:
http://www.netadmintools.com/art605.html