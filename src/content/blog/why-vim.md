---
date: '2014-10-10T20:59:54-05:00'
description: Why Vim
id: 577
pubDate: '2014-10-10T20:59:54-05:00'
tags:
- Programming
- Vim
title: Why Vim
---

A coworker asked me the other day.

> Why Vim, what do you like so much about it?

I would dare say everyone who encounters Vim has a similar story. Many years ago, while on a Linux machine I first uncounted Vim. It was a simple way to edit text on the server. It didn't make sense, but it worked. That was the extent of engagement with Vim for about 10 years, just being able to do simple editing of text. More than a handful of times I would get into some mode that I would not know how to get out of and have to force quit my session.  

> If you feel dumb, you're in a good place, you're learning.

Then a lot of developers that I admired would either show their screen during a presentation or screen cast and they would be using Vim. Maybe it was [Felix Geisendörfer](http://twitter.com/felixge), who said _[While vim may not help you to impress the ladies, it will please our BDFL and your grandpa will also approve](https://web.archive.org/web/20120501015549/http://nodeguide.com/style.html)_. He's not the only prolific developer that uses Vim, there are others and it made me think _"Hey, maybe there is more to this Vim thing than first meets the eye"_.  

The article that by far helped me get started with Vim was [Learn Vim Progressively](http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/). The key is, get the basics down, then gradually learn more. Vim is not a, read the manual once and master it, but rather a 10 year plan. It's old and odd at first, but the odd is there for good reasons.  
G
Back to our question
> Why Vim, what do yo like so much about it?

Here are a few of the features of Vim I use everyday, that make my life so much easier. Some of these you can find in other editors, but Vim really shines.

## 1. Replace inside quotes, parenthesis, brackets, you name it  

Let's say you have some code like such: `var song = "I'm a little tea pot";`, and you want to replace it with `var song = "We will, we will, rock you"`. In Vim you simply place your cursor inside the quotes and press `ci"`. This means `Change (c)`, `In (i)`, `Quotes (")`. This will clear out everything inside the `"` and you can begin typing what you want.  

This works for any enclosing characters, `()`, `[]`, `''`; would be `ci(`, `ci[`, `ci'` respectively.  

## 2. Navigation 

Getting around in Vim is very quick. Use `w` or `W` to jump words. Use `e` or `E` to jump to the end of words. Use `ft` to jump to the next occurrence of `t` in the current line. Use `$` to just to the end of the line (yeah, just like in RegExp). Try `G` to go to the bottom of the file, or `Ctrl + o` to go back to where you were (even in other files).

## 3. Smart search 

This isn't monumental, but you can type `/hello` and it will find *Hello* and *heLLo* and *hELL0*, but if you type `/Hello`, it only find *Hello*, not *hello*. That's because as soon as you type of capital, it turns into case sensitive.

## 4. Plugins, endless configurations

There are way too many plugins to cover them, but suffice to say here are a few that I use and depend on.

* [Vundle](https://github.com/gmarik/vundle) for managing plugins
* [Fugitive](https://github.com/tpope/vim-fugitive) for awesome git integration
* [NERDTree](https://github.com/scrooloose/nerdtree) is a great file/folder explorer
* [Syntastic](https://github.com/scrooloose/syntastic) for syntax highlighting.
* [NERDCommenter](https://github.com/scrooloose/nerdcommenter) for very powerful comment/uncommenting


## The dot `.`

Let's say you type something like I just did `https://github.com/`. One you do that you can use the `.` to do that over and over anywhere. Move to different location, then press `.`, it inserts `https://github.com/`.
