---
date: '2009-06-26T19:21:54-05:00'
description: Speeding up my web server
id: 58
pubDate: '2009-06-26T19:21:54-05:00'
tags:
- Server Admin
title: Speeding up my web server
---

Google has created a campaign to [make the web faster](http://code.google.com/speed/ "Let&#39;s make the web faster - Google Code"), and a corresponding website to help web admins to make it so. Speed is a big factor in customer perception of your site. Google has conditioned the masses to expect instant page loads and sites that take even 2-3 seconds to load will only be tollerated for so long. Couple this with pages become more dynamic and resource hungry everyday.

Here are some changes I've made to my web server to speed things up.

## Page Speed

Install the [Firebug](http://getfirebug.com/ "Firebug - Web Development Evolved") extension [Page Speed](http://code.google.com/speed/page-speed/ "Page Speed Home"). This is a great extension that will essentially prod you through the following steps to speed up your pages. [Yslow](http://developer.yahoo.com/yslow/ "Yahoo! YSlow for Firebug") is another such plugin for [Firebug](http://getfirebug.com/ "Firebug - Web Development Evolved")

## Compress files with [mod_deflate](http://httpd.apache.org/docs/2.2/mod/mod_deflate.html "mod_deflate - Apache HTTP Server") (dumb name)

I already had a declaration in my `http.conf` file for `LoadModule deflate_module modules/mod_deflate.so`. So I created a rule in one of my virtual host files for
`
<Location />
    AddOutputFilterByType DEFLATE text/html text/css text/plain text/xml
</Location>
`
That should effectively compress all html, plain, xml, and css files.

## Leverage browser caching

I want to set an expiration of one month in the future for all my cache-able content. Apache [mod_expires](http://httpd.apache.org/docs/2.2/mod/mod_expires.html "mod_expires - Apache HTTP Server") will do the job nicely. Again in my httpd.conf file I had and entry for `LoadModule expires_module modules/mod_expires.so`.

Then in I created a file `conf.d/expires.conf` and placed the following code in there. 
  `
    <IfModule mod_expires.c>
        ExpiresActive on
        ExpiresDefault "access"
        ExpiresByType text/html "access"
        ExpiresByType text/xml "access"
        ExpiresByType text/css "access plus 1 month"
        ExpiresByType text/plain "access plus 1 month"
        ExpiresByType application/x-javascript "access plus 1 month"
        ExpiresByType application/x-shockwave-flash "access plus 1 month"
        ExpiresByType application/pdf "access plus 1 month"
        ExpiresByType image/gif "access plus 1 month"
        ExpiresByType image/png "access plus 1 month"
        ExpiresByType image/jpeg "access plus 1 month"
        ExpiresByType image/x-icon "access plus 1 month"
        ExpiresByType video/x-flv "access plus 1 month"
        ExpiresByType video/quicktime "access plus 1 month"
    </IfModule>
  `

  This will enable the expires module, then set the default expires to 'access' or 'right now'.  You can tweak it to your liking, but the following types of content have expiration date of 1 month from the time the browsers first sees the content.

## Optimizing Images

You want to scale your JPEG images down to as small as possible.  Some of my images were way too big and I could shave huge amounts of data with virtually no image loss by reducing my JPG images.

I created a script that uses the [jpegtran](http://jpegclub.org/ "JPEGclub.org") program found on Linux machines. I created the following bash script `optimize.sh` that will take a folder of JPG images, copy them to another folder called `new-opt-jpegs` and then compress them.  
  `
    #!/bin/bash

    mkdir new-opt-jpegs
    for filename in $( ls $1/ )
    do 
    jpegtran -opt $1/$filename > new-opt-jpegs/$filename
    done;
  `
  You would use the script like `optimize.sh images/`. This will take all the JPGs in 'images', compress them, then copy them to a folder called 'new-opt-jpegs'.