---
date: 2020-03-22 13:42:00
description: Starting a New Large Project
pubDate: '2020-03-22T13:42:00'
tags:
- Projects
title: Starting a New Large Project
heroImage: '/images/starting-a-new-large-project-thumbnail.jpg'
---

Here I want to talk about what a team should do when they start a new project. I've had the privilege to architect several large projects and to come into several existing projects having a fair amount of girth. These are just a few thoughts about things teams needs to establish sooner rather than later.


## Create a glossary (terms everyone can agree on)

This is good for new comers to the project, getting their head around jargon. Create a place, or even in the README.md of every repo.

* property
* unit
* group
* team
* organization
* project

Just a sample of the many terms people use interchangeably to mean different things. Just the other day, I was on the phone talking to someone trying to sell me some advertising, our terms were not aligned so I was baffled when he quoted me some rates. As soon as we agreed that a `property` was X and a `unit` was Y, it all made much more sense.


## Decide on naming conventions (NPM modules, git repos, filenames)


> There are 2 hard problems in computer science: cache invalidation, naming things, and off-by-1 errors.  
> -- many people on the internet

Yeah, naming things. Seems easy, but a bad name can be confusing, or worse yet, having two or more names for a single idea. A project I was on had, for example, to reference a "Movie" (e.g. Star Wars), the words **title**, **feature**, **movie**, **program**, could all mean a Movie. (╯°□°）╯︵ ┻━┻

### Files and folders

Once I was troubleshooting an issue, it worked fine locally, but would totally fail on the server. It was a filename case issue. On the Mac, `MyFile.js`, worked fine, but on Linux it failed, because the code was looking for `myfile.js`. Do yourself a favor with files. Follow these simple rules

1. lower case
2. use hyphens (also known as kebab-case)
3. letters and numbers
4. No spaces what-so-ever

```
// BAD
MyFiles.js
This Really Bad Name For A File.js
lame_looking_files.js

// Good
my-files.js
this-really-good-name-for-a-long-file.js
better-looking.js
```

## Decide on syntax / linter

Developers have opinions, I'm no different, and if you're honest, you have opinions too. The problem comes when we have opinions on things having little actual value. It's called [Bike Shedding](https://en.wiktionary.org/wiki/bikeshedding), the technical, meaty items receive little argument, but the color of the shed, or tabs versus spaces, or to semi-colon or not-semi-colon ;)  .

Developers like to get opinionated about such things as where `{ }` brackets go, spacing, if you should use a certain coding structure. It's insane, and in Javascript land, it's down right mindblowing.


Free yourself, leave it all behind. Let go of your opinions. Just rely on [one](https://github.com/airbnb/javascript), of [the](https://google.github.io/styleguide/jsguide.html) [many](https://github.com/standard/standard) already well established style guides. Just as a note, a pretty greenhorn move is committing a bunch of changes your editor made automatically.

> If your editor is making updates to the code without your knowledge, then fix your editor or [get a new one](/tags/vim/).

So put linting at the first thing to do by your CI when a PR is opened. If it doesn't pass linting, then fail it immediately.

On the other side of the fence, just because you pick a popular style, don't be afraid to tweak it a little, but careful you don't go overboard. As an example, I'm a big fan of the [dangling comma]( https://eslint.org/docs/rules/comma-dangle) rule. To me it makes sense to just allow it since Javascript has no issue with it, and it makes cleaner git diffs.


## Establish the deployment pipeline quickly

If you've been on [this blog](https://www.shanestillwell.com) for any length of time you will see I'm a huge fan of [GitLab](https://github.com/) 😍. When you start your project, get it deployed quickly. I mean, get your code running the CI / CD pipeline with every push up to your git server. 

### I can do it later, it's not ready to be deployed now

This is wrong thinking. Do this immediately, it will be more difficult later. Right now it's simple to take a fresh greenfield project lint it, test it, package it, and deploy it. If you wait until later I guarantee there will be more hurdles to overcome and circles to run to get your app working correctly through your pipeline.

> Begin as you mean to go on...  
> -- Charles H. Spurgeon, All of Grace 

By putting your new app through this exercise, it will correctly influence how your app will evolve and shape the direction it takes as it grows and develops.

## Resources
* Thank you to you [Johannes Plenio](https://pixabay.com/users/jplenio-7645255/) for the [thumbnail image](https://pixabay.com/photos/panorama-summer-meadow-field-away-3629120/) found on [Pixabay](https://pixabay.com/)
