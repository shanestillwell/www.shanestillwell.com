---
title: Should You Point Bugs
description: Should you point bugs when doing sprint planning?
pubDate: 2024-06-14T16:28:47.173Z
tags: ["leadership", "programming"]
heroImage: /images/should-you-point-bugs.jpg
---
During a recently held sprint planning, we faced an intriguing query - Should we point bugs? This question may seem straightforward, but the answers divide opinions.

Some might say there's a hidden velocity in your team that isn't being accounted for if you don't point bugs. However, if you decide to point bugs, the scale of measurement becomes another conundrum.

After digging further into this question, I stumbled upon an appealing perspective shared by a Reddit user [andrers2b](https://www.reddit.com/user/andrers2b/). They made an excellent point that bugs consist of two distinct parts: 

1. Finding the bug
2. Fixing the bug

Estimating the workload to fix the bug might not be the most challenging task (pun intended), but how can one measure the effort for the first part - finding the bug?

This problem arises because a bug's complexity may hide it beyond layers and layers of code. Sometimes, the bug can be nestled deep within someone else's uncommented legacy code, increasing the difficulty manifold.

Andrers2b shared an anecdote where they spent three days unearthing a bug, and a mere three minutes fixing it. The issue was a missing if-statement, the fix was straightforward, but locating it was the complex part.

To add even more complexity, consider 'Heisenbugs'. These elusive types of bugs seem to alter behavior or vanish altogether when investigated for fixing. For more about these critters, check out [here](https://en.m.wikipedia.org/wiki/Heisenbug).

Given these variables, it appears imprudent to estimate the effort needed to find a bug. However, once the bug is identified and its reasons documented at grooming, the effort needed to fix it can indeed be estimated.

A bug hunt can be an adventure. Take enough time to study your prey and document its behavior. Once you know what you're dealing with, you're better equipped to estimate the effort needed to conquer it.

Let's get back to our keyboards, dev-warriors, and embark on this happy bug hunting journey!

### References:
* [Reddit Discussion on Pointing Bugs](https://www.reddit.com/r/scrum/comments/ulogl7/comment/i7x0vex/)
* [Cover Photo by Markus Spiske on Unsplash](https://unsplash.com/photos/closeup-photo-of-turned-on-computer-monitor-4T5MTKMrjZg?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)
