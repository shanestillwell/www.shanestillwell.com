---
date: '2010-02-25T21:05:31-05:00'
description: Ignoring Files and Folders in Git
id: 94
pubDate: '2010-02-25T21:05:31-05:00'
tags:
- Git
- Programming
title: Ignoring Files and Folders in Git
---

Just a hint, it's a lot easier than Subversion.  My love for Subversion grows colder each day.

In your repository root directory you'll want to create a file called `.gitignore`.  In that file you'll want to specify files and folders to ignore.  For example.

Ignore the entire Magento <pre>/media</pre> folder.
`
media/**/*
`

The root repository .gitignore file will act as a global but can be overridden in subdirectories by creating a <pre>.gitignore</pre>in those directories.

### A Gotcha!

Empty directories do not get tracked by Git. If you want them to be tracked, they need to have something in them. Usually doing a touch .gitignore is enough to keep the folder tracked.

This one stumped me for a little bit, anyway, it pays to read.

Credit:
http://www.gitready.com/beginner/2009/01/19/ignoring-files.html
http://github.com/guides/ignore-for-git