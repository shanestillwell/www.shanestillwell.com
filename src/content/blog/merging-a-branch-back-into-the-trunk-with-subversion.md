---
date: '2009-03-24T11:49:15-05:00'
description: Merging a branch back into the trunk with Subversion
id: 43
pubDate: '2009-03-24T11:49:15-05:00'
tags:
- SVN
- Programming
title: Merging a branch back into the trunk with Subversion
---

I'm just getting the hang of using Subversion for version control and the workflow is starting to gel.  One issue I came against yesterday was merging a branch back into the trunk.  The process is a bit interesting.

1\. Copy your trunk to a branch `svn copy http://path.com/to/trunk  http://path.com/to/branch/example`
2\. Switch your working copy to the branch
3\. Check your URL with `svn info`
4\. Make changes to code
5\. Commit back to branch `svn ci -m "made my changes to branch example"`
5\. Switch your working copy back to TRUNK
6\. Merge the branch with the trunk on your working copy `svn merge http://path.com/to/trunk http://path.com/to/branch/example .`
7\. This only changes the working copy, now you need to commit your working copy to trunk `svn ci -m "Merged branch example into trunk"`