---
date: 2021-11-03 12:58:54
description: Being an Effective Team Lead
pubDate: '2021-11-03T12:58:54'
tags:
- Leadership
title: Being an Effective Team Lead
heroImage: /images/being-a-team-lead.jpg
---

Contrary to popular belief, the team lead on a software team is not the smartest person on the team. They are the party host.


## Throwing an effective party as a Team Lead

## Delegations

Allow someone to tackle a task using their own approach. Allow someone to deliver a solution less ideal than you would, without compromising quality.

It's difficult for anyone to admit that small trivial items have little no affect on the end result of


Example.

Coding Style, in JS land, using a `forEach` when a `map` might be better.... get over it.

## References:

[![How to be a leader](https://img.youtube.com/vi/jMpCF0Z623s/0.jpg)](https://www.youtube.com/watch?v=jMpCF0Z623)
