---
date: '2009-09-02T02:38:14-05:00'
description: Updating Namecheap DDNS using BASH, CURL, and good 'ol know how.
id: 65
pubDate: '2009-09-02T02:38:14-05:00'
tags:
- Server Admin
title: Updating Namecheap DDNS using BASH, CURL, and good 'ol know how.
---

I have a server at home that I use to do snapshot style backups using rsync. As such, I'd like to be able to connect to it from time to time when not at home. This is a simple way to update your Dynamic DNS with NameCheap.

Create a file called `ddns` and place it in `/etc/cron.daily` and set the permissions to `700`

```
#!/bin/bash
# Update NameCheap DDNS 

# Disclaimer: I have no affiliation with jackson.io, it may not work when you read this.  
# It's just fetching your URL as plain text.  If you had a webserver it would be simple
# to write your own whatismyip service.
IP=$(exec wget -q -O - http://jackson.io/ip/)

curl -s -o temp.html "http://dynamicdns.park-your-domain.com/update?domain=YOURDOMAINNAME&password=YOURVERYLONGPASSWORDFROMNAMECHEAP&host=YOURHOSTNAME&ip=${IP}"

```

You'll want to change some of the variables in the long URL. After that, it will update NameCheap DDNS on a nightly basis. No more need for a static IP.
