---
date: '2010-12-13T17:59:52-05:00'
description: Zend_Loader_PluginLoader_Exception
id: 120
pubDate: '2010-12-13T17:59:52-05:00'
tags:
- Programming
title: Zend_Loader_PluginLoader_Exception
---

For the top alerts you get after you've saved a page or some other event, there is the built in Flashmessenger in Zend Framework, but emanaton has put together a great messenger called [PriorityMessenger](http://emanaton.com/code/php/zendprioritymessenger "Zend Framework: View Helper Priority Messenger | emanaton").

I had it running on one of my sites without issue, but when I tried to get it running on another site I get an error of.

I had placed my `PriorityMessenger.php` file in:
`application/views/helpers/PriorityMessenger.php`

Then when I tried to access the controller I received.

<pre>
Plugin by name 'PriorityMessenger' was not found in the registry; used paths:
Zend_View_Helper_Navigation_: Zend/View/Helper/Navigation/
Support_View_Helper_: /var/www/production/application/modules/support/views/helpers/
Zend_View_Helper_: Zend/View/Helper/ 
</pre>

I checked the paths, the class names, etc. still no dice.  

### A Simple solution

In the `application.ini` file I added this line:

`
resources.view.helperPath = APPLICATION_PATH "/views/helpers"
`

This I guess tells Zend Framework to look in `application/views/helpers` for helpers applicable to all modules.

Keep up, keeping up.