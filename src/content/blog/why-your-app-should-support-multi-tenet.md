---
date: 2022-07-04 14:57:05
description: Why Your App Should Support Multi Tenet From The Start
pubDate: '2022-07-04T14:57:05'
tags:
- SAAS
title: Why Your App Should Support Multi Tenet From The Start
heroImage: /images/why-your-app-should-support-multi-tenet.jpg
---

Mostly for testing purposes, you can spin up separate instances of a customer that can be tested in isolation.

I worked at place that didn't have this as an option and seeding and testing the db was a nightmare.

So how do you do this in your db? First, each organization needs to have it's own ID in the db. Then all things should somehow relate to that organization.

The cool thing is you can always add layers above. For me the main layer is a `Property`, but I added `Association` on top of that to represent groupings of properties. This worked well.

## Caveats

* You can never allow a blank "get all" to a database table. All calls need to be qualified with some ID.
* If you are not careful, you could accidentally leak data between the two tenants.
* Makes access control more complex, but not overly complex IMHO.

> All data is related
