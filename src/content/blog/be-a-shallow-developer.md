---
date: 2019-11-22 21:54:50
description: Be Shallow
pubDate: '2019-11-22T21:54:50'
tags:
- Vim
- Programming
- Projects
title: Be Shallow
heroImage: '/images/be-a-shallow-developer-thumbnail.jpg'
---

> Do you want a secret that will make you 10x more productive?

Of course you do. As a programmer, you're designed to look for better, faster, more efficient solutions to all life's problems. It's in your nature, and if you could be 10x faster, you would at least be curious.


## Does This Sound Familiar

You start working on a rather large code base. The previous developers did their best to organize the project into nice subfolders, and more subfolders. Maybe it looks something like a project I worked on many years ago.

```
├── docker
├── public
│   ├── admin
│   └── client
├── reports
└── src
    ├── __tests__
    ├── contract
    │   └── client
    ├── errors
    ├── graphql
    │   ├── admin
    │   │   └── resolvers
    │   └── client
    │       └── resolvers
    ├── middleware
    ├── models
    │   └── __tests__
    ├── samples
    │   ├── admin
    │   └── client
    └── services
        ├── __mocks__
        └── __tests__
```

Organized and neatly separating the different types of files.

## But Why Do This?

Ask yourself. **Does this help you find files faster?**, maybe it helps so you can **Drill into folders with your IDE?**. Is there a real reason to organize files in neat directories. **Is it more performant?**

No.

## Problems with deeply nested directories

* Lots of `../../../server/models/` in your code to pull in that one file from a completely different location. That should be a smell right there. If the code base really was organized well, this would not happen. But in reality, code is not easy to file into hierarchy forms like a directory structure. 🙈

* If you use an IDE, then clicking on folders to open them up to get to your file. This is time consuming and horribly inefficient. [More](#using-the-mouse-trackpad-to-open-files) on this later. ⌛

* For a large enough project, where to place files becomes ambiguous. What if a file is used by both `client` and `admin` code? Now you feel the need to create a `common` directory to hold the files needed by both. 💣

* On the command line, moving around this file structure is tedious. 🕐

* Files can easily be lost, orphaned or duplicated 😱.

----

## A Better Way

----

Remember the **one rule**

> All files should only be one directory deep

```
▸ __tests__/
▸ consumers/
▸ contracts/
▸ coverage/
▸ errors/
▸ gql/
▸ middleware/
▸ migrations/
▸ models/
▸ node_modules/
▸ resolvers/
▸ scripts/
▸ seeds/
▸ server/
▸ services/
▸ static/
▸ templates/
  changelog.json
  config.js
  constants.js
  consumer-helpers.js
  ...
```

* The root of your project will have directories that are only one level deep
* Use **file naming conventions** in your directories to group like files. For example in my models directory you see files
    ```
    ▸ models/
        ├── person-email.js
        ├── person-location.js
        ├── person-note.js
        ├── person-phone.js
        ├── person.js
        ├── pet-rate.js
        ├── phone.js
        ├── property-age-groups.js
        ├── property-amenity.js
        ├── property-email.js
        ├── property-image.js
        ├── property-location.js
        └── property-order-addon.js
    ```
* Now all your files are just one directory away, the most you'll see is `../models/phone.js`
* Refactoring becomes easier since all your similar files are in the same directory. No need to worry about creating complex nested folder searches.
* You'll soon see how easy it is to find files, reference them, create new files, and navigate your project like a pro.

## Using the Mouse / Trackpad to open files?

Remember earlier I said, this trick will make you 10x faster, well, for you *Madam or Sir Clicks-a-Lot*, this suggestion will improve your productivity better than 100x. **Use the keyboard to fuzzy find files to open**. 

* VSCode: `⌘ + p` will open the file finder for the project. It should be fuzzy find so just type a few unique letter combinations
* Vim: You should be using the [FZF](https://github.com/junegunn/fzf.vim)
* WebStorm: `⇧ ⇧`, [Shift Shift](https://www.jetbrains.com/help/idea/searching-everywhere.html#tips) will open up the file finder 

## Conclusion

Be a **Shallow Developer**. Keep your file hierarchy shallow, one level deep. There may be times you need to go two levels, but make it the exception, not the rule.
