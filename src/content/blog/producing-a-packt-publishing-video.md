---
date: 2022-05-20 13:12:41
description: My Experience Producing a Video for Packt Publishing
pubDate: '2022-05-20T13:12:41'
tags:
- rant
title: My Experience Producing a Video for Packt Publishing
heroImage: '/images/producing-a-packt-publishing-video-thumbnail.jpg'
---

Years ago, I was approached by Packt Publishing, with the proposition of producing a video on [MEAN](https://en.wikipedia.org/wiki/MEAN_(solution_stack)) web development.


## Some of my initial thoughts

* **Money** was a motivator. Boy, I could maybe make $10,000 - $20,000 (LOL, read on). I want to say the commission was something like 15% per video purchased.
* **Recognition** was another driving force. I could tout I'm an author, video producer, technical educator and all that.
* **Altruism**, if you're reading this, then you know I enjoy educating others.

## The Effort

The video course was four hours long. Anyone producing videos understands it takes about 100 hours of work for every hour of video. No joke. First you need to write the entire script. Yes! The entire script. Video shooting is done at the very end. The whole course needs to be produced in writing, everything you'll say. Then you need to record each section. The amount of times I flubbed up while speaking and had to redo a section was embarrassing. You learn to record in short 1-2 minute segments in case you mess up. It's less to do over. To this day, I still remember hours of kneeling next to my bed with my [microphone](https://www.bluemic.com/en-us/products/yeti/) sitting inside my [sound box](https://www.youtube.com/watch?v=rWgLCPaOAzo) on the bed with the script on my phone in my hand. I had to ensure no one was home and everything was quiet because the wonderful mic I had would pick up the smallest of sounds. I used the notes on my phone because having paper would make noise.

I can safely say I spent most of my spare time from January 2016 - May 2016 producing the video. Hundreds of hours of work over the span of 4-5 months.

## The Result

Four hours of glorious full stack development in MongoDB, ExpressJS, Angular 2, and Node.js. In this course you learned how to use Bootstrap, Routes, Mongoose Models, Mocha Tests, Form Validation, Protractor E2E testing, JWT Auth tokens, Job Queues, Email Sending, Socket.io messaging, Deploying the whole stack, and so much more. It was a comprehensive course and I'm proud of the work I did.


![Mastering MEAN Web Development](/images/mastering_mean_cover.jpg)

### Recap our inital thoughts

* **Money** laughable. I would have made more money working those hours at McDonald's. Don't ever do it for the money. I made much more per hour writing a [simple article for InfoQ](https://www.infoq.com/articles/turbocharge-react-graphql/) on GraphQL.
* **Recognition** maybe. It's hard to judge a thing like this. It probably helped gain me work and gave me some sort of clout. However, as you'll see below, it has diminishing returns.
* **Altruism** is tricky. I mentioned I like teaching, but take away the reward of seeing the results of your labor and it loses its appeal. What do I mean? When producing a video, you're all alone, typing, speaking into you microphone. Do you see the look of understanding in the eyes of the people you are teaching? No. Do you receive any thanks from the learner? No. I whole heartedly expected to receive many "*hey I have a question about your course*" emails, but only ever ended up getting 2-3 replies about the course. I think it was rated 4 out of 5 stars by 9 people.


## Danny* was smart, be like Danny

Some time after my video was published I receive this email from a guy named Danny*.

```
Hi Shane,

I have been contacted by Packt to produce a video on MEAN. I see you have done one earlier this year. I am doing my due diligence at the moment and thought perhaps I could get your thoughts of your experience working with Packt - time commitment, reward potential, potential problems, etc. It would be much appreciated.

Thanks,
Danny
```

Notice the terms *due diligence*? Yeah, Danny's smart. I was pretty mad at first. It'd had been less than a year after my video was released and Packt was already contacting someone else to improve upon my work. Packt never contact me at all. Did I do a poor job? Packt gave no feedback supporting such a conclusion. It occurred to me this might be Packt's mode of operation. Lure a developer in to work on a video production with the promise of commission which they incorrectly assume is will be worth their while.

This was my reply.

```

Danny,

I'd be glad to give you my honest assessment. I was contacted by Packt last year about this time to do a MEAN video, I had never done a video course before, only a few screencasts here and there. I wish I had done my due diligence like you, but I saw it as an opportunity to get some recognition, maybe earn some money, and learn Angular2 while I was at it.

In short, I would never do another video for Packt again.

Time Commitment: Tremendous
As I mentioned, I had not done a video with them. You first need to come up with a layout or table of contents per se. Then you begin writing out verbatim each chapter, this is really time consuming. Your video has to flow correctly, all your ducks need to be in order and logically progress to make sure you are accurately covering the material. I probably spent 10-15 hours each week for months writing out the video script. Probably in the realm of 200 hours for this part. Then you go into video production which is tedious, and that lasted a few months. Essentially, all my free time for 6 months was wrapped up in creating that video.

Monetary Reward Potential: Laughable
They give you an 'advance' on your royalties, which is nice, but that's all I've seen so far (my video debuted June 1, 2016). Actually at this point I have no idea how many videos have sold or anything. They also were discounting it tremendously for a while. I think my video is about $80 now, but they would run specials making it $20 or less sometimes. Don't do it for the money.

Community Recognition (aka Bragging Rights): OK
So, the cool thing is you basically get to call yourself an author. "I authored a video for Packt Publishing". People automatically think authors are smart, and I'm sure you are, considering you're weighing this decision carefully. Being able to put it on your resume, link to it on LinkedIn, and tell people what you did, it's not something someone does every day. It's kind of like when you meet someone that has climbed Mt. Kilimanjaro. It's impressive, and it was probably a lot of work and took dedication and time. But the reality is, almost anyone can climb Mt. Kilimanjaro, even an 86 year old Russian woman did it. I'm a consultant, and pointing to the video as just one my 'contributions' to the tech world carries a little bit of merit, I think.

I hope this email was a little bit helpful. If you have any further questions, I'd be happy to answer as best I can.

Good luck,
Shane
```


## Am I bitter?

All life is a learning experience. It taught me many valuable lessons and to be fair, I was able to learn a ton about all the technologies I was teaching, because as you know.

> The best way to learn something is to teach it.

Now go and be like Danny*

* The names have been changed to protect the intelligent.
