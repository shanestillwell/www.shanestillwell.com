---
date: '2012-07-20T20:29:10-05:00'
description: Getting AngularJS and Zend Framework POST to play nice
id: 342
pubDate: '2012-07-20T20:29:10-05:00'
tags:
- AngularJS
- Programming
title: Getting AngularJS and Zend Framework POST to play nice
---

I was simply posting data to the server using this code

```javascript
$scope.submit = function() {
    console.log($scope.newTitle + ' is the new title');
    $http({
        url: '/api/idea/method/add',
        method: 'POST',
        data: {
            title: $scope.newTitle,
            description: $scope.newDescription
        },
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .success(function(data, status) {

    })
    .error(function(data, status) {
    });
};
```

But I was getting a JSON object submitted and therefore using Zend's `_getParam('title')` could not pick up the title in the post. 

The answer was to use jQuery's `$.param` function. So now I surround the data JSON with `$.param`

```javascript
$scope.submit = function() {
    console.log($scope.newTitle + ' is the new title');
    $http({
        url: '/api/idea/method/add',
        method: 'POST',
        data: $.param({
            title: $scope.newTitle,
            description: $scope.newDescription
        }),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .success(function(data, status) {

    })
    .error(function(data, status) {
    });
};
```

I know AngularJS has a way to change the data transform function, but I'm too lazy right now to figure out how to do it.