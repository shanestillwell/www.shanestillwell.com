---
date: 2018-05-29 13:08:37
description: Stupid Postgres Tricks
pubDate: '2018-05-29T13:08:37'
tags:
- Postgresql
title: Stupid Postgres Tricks
heroImage: '/images/stupid-postgres-tricks-thumbnail.png'
---

I was curious, how many day's old am I? I suppose I could solve this in many different programming languages, but thought *"PG has really good date management features, let me try that"*. So here it is. 


## Calculate how many days old people are

```sql
select * from (VALUES
	(CURRENT_DATE - '1976/12/18'::date, 'Jill'),
	(CURRENT_DATE - '1980/06/29'::date, 'Jack'),
	(CURRENT_DATE - '2004/06/03'::date, 'Humpty'),
	(CURRENT_DATE - '2006/06/26'::date, 'Cole'),
	(CURRENT_DATE - '2007/11/02'::date, 'Hubbard'),
	(CURRENT_DATE - '2010/09/24'::date, 'Mary')
	) as bday(days_old_today, person);
```
