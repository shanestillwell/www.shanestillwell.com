---
date: '2007-05-22T12:31:00-05:00'
description: Aggregate module special characters
id: 16
pubDate: '2007-05-22T12:31:00-05:00'
tags:
- Programming
title: Aggregate module special characters
---

To keep the aggregate module from displaying HTML entities, I've made this change on Wildpaddle.com

`
Index: aggregator.module
===================================================================
RCS file: /cvs/drupal/drupal/modules/aggregator.module,v
retrieving revision 1.279
diff -u -p -r1.279 aggregator.module
--- aggregator.module	2 May 2006 13:28:04 -0000	1.279
+++ aggregator.module	2 May 2006 17:04:34 -0000
@@ -1332,7 +1332,7 @@ function theme_aggregator_page_item($ite
   }

   $output .= "<div class=\"feed-item\">\n";
-  $output .= '

### [link) .'">'. check_plain($item->title) ."]()
\n";
+  $output .= '

### [link) .'">'. strip_tags($item->title) ."]()
\n";
   $output .= "<div class=\"feed-item-meta\">$source <span class=\"feed-item-date\">$source_date</span></div>\n";

   if ($item->description) {
`