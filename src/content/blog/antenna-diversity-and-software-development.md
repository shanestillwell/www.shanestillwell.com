---
title: Antenna Diversity and Software Development
description: Antenna Diversity and Software Development
pubDate: 2024-11-13T21:45:57.107Z
tags: []
heroImage: /images/blog-placeholder.jpg
draft: true
---


Maybe you're old enough to remember the rabbit ear antennas sitting atop of old television sets. If you position the antennas just right, the picture would come in better. Did you ever stop to wonder, "Why were there two antennas?". Look in the mirror, you have a set of eyes and a set of ears. This is called Antenna Diversity and enables the receiver to pick up better, more clear signals from the sender.

**What does this have to do with Software Development?** We'll arrive there shortly, but first an example.

Several years ago I was approached by a friend in the family fishing resort business. He belong to an association of resort owners on a lake in Minnesota. These resort owners wanted a search engine to search across their cabins for rates and availability, allowing guests to book cabins for their summer stay. My friend warned me "Shane, these resort owners cannot agree on anything, they all do things their own way."

So I proceeded to get to know the business, in doing so I interviewed several dozen of the resort owners about how they calculate their rates, the rules for staying, charges for pets, boat rentals. Everything having to do with the business, I amassed all the diverse ways things were done by these resort owners.

When it came time to develop the system, I knew there were many places needing customization. Luckily, some things were universal, but other things required a design to support different configurations.

