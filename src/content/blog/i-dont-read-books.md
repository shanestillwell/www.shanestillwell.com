---
date: 2024-03-31 14:57:52
description: I don't read books, but I read a lot
pubDate: '2024-03-31T14:57:52'
tags:
- Good Ideas
title: I Don't Read Books Anymore
heroImage: '/images/reading-a-book-thumbnail.jpg'
---

That's right. I don't really sit down and read a physical, or digital, book. This may sound contradictory, since I have a [book page](/books/) dedicated to the books I've read. Let me explain. As a programmer, I'm reading all day long on the computer; code, documentation, StackOverflow, READMEs, and more. So, I read a lot, just not books.

When it comes to books, I have a problem. I can't read fast enough to keep my brain enagaged in the subject matter. No, I'm not a remedial reader, I can read pretty fast, but not fast enough. Some would say, well learn to read faster. I've done that, but there is a physical limit to your eyes, even when using [Bionic reading](https://bionic-reading.com/) or the great techniques found in a book I love [Reading Smart](https://a.co/d/5Yb4ovH).

## Solution

I listen to books. Mostly with Audible, but there is a catch. **I listen to the books at 1.5x or even 2x the speed.** My wife hates it and asks "How can you understand them?". But for me, when I'm listening to a non-fiction book, this is the only way to keep my mind fully engaged. In truth, it does make the author sound like [Chip N Dale](https://www.youtube.com/watch?v=bPEvVEhVIMA), but who cares. Most of the time I listen to books while driving, but I also do this when I'm listening to them riding the bus with my AirPods in or on my bike.

You can listen a lot faster than you can physical read. At times, I feel like this is cheating, but I've come to terms with this little hack that works for me. Your Mileage May Vary (YMMV). I personally think your listening capabilities are far superior to your reading. Even for comprehesion. I tend to retain more when I listen, than when I read. I've physically read a lot of books and I can't say I've retained more by reading than listen, but that might be more in the memory than the intake.

I do this with YouTube material that is teaching a skill, like this [Rust video](https://www.youtube.com/watch?v=BpPEoZW5IiY). A great turtorial on Rust, but if I listened at normal speed, I'd be asleep in 2 minutes. At 2x the speed, I can still follow along and pick up the information

## Exceptions
I don't do this with fiction books that I want to listen, which I find listening to a book lets my imagination free to set the stage. I don't watch movies at 2x the speed, that would be weird.

So there is my public confession, I don't sit down and read a book much these days, my goal is to ingest information, so time is of the essence.

## Links to help with listening to books
* https://www.audible.com/
* https://makeheadway.com/
* https://www.scribd.com/

