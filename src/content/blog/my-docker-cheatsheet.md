---
date: 2021-08-01 16:31:50
description: My Docker Cheatsheet
pubDate: '2021-08-01T16:31:50'
tags:
- docker
title: My Docker Cheatsheet
heroImage: '/images/my-docker-cheatsheet-thumbnail.png'
---

## Simply a reference for you and I



### Cleaning images

* `docker system prune`: Works good enough to reclaim some space.
* `docker rmi $(docker images -q -f dangling=true)`: Removes all the images you are not using now.
* `docker volume rm $(docker volume ls -qf dangling=true)`: Removes all the volumes you are not using now.

## Filtering

https://stackoverflow.com/a/44461712/179335

Docker supports finding images by globs 

```
docker images "pattern-of-images-*"
```

## Size

```
docker system df
```

```
docker system df -v
```

https://medium.com/homullus/how-to-inspect-volumes-size-in-docker-de1068d57f6b



Remove all images that are not used by container. The -a tells Docker to remove all unused images, without it Docker only removes dangling (untagged) images.
```
docker image prune -a
```


### Cool commands I don't think I'll ever use

*  `docker system events`: real time event logging e.g. when you delete an image, it'll show like this.
    ```
    2021-02-23T11:35:14.566623900-06:00 image untag sha256:d0e7e8b4a50ae240e50888f4efe46bbad5583ff1f384e522717aeb981d417e10 (aws.java.sdk.version=1.11.477, name=sha256:d0e7e8b4a50ae240e50888f4efe46bbad5583ff1f384e522717aeb981d417e10)
    2021-02-23T11:35:15.952564700-06:00 image delete sha256:d0e7e8b4a50ae240e50888f4efe46bbad5583ff1f384e522717aeb981d417e10 (name=sha256:d0e7e8b4a50ae240e50888f4efe46bbad5583ff1f384e522717aeb981d417e10)
    ```
*

## References

* https://hackernoon.com/cleaning-up-docker-f14edd6fcf4c
* https://cntnr.io/whats-eating-my-disk-docker-system-commands-explained-d778178f96f1
