---
date: '2010-06-11T01:00:49-05:00'
description: Magento and Zend Server checkout redirect to cart [solved]
id: 111
pubDate: '2010-06-11T01:00:49-05:00'
tags:
- Programming
title: Magento and Zend Server checkout redirect to cart [solved]
---

<div style="float: left; margin-right: 20px;">[![IMG_3623.JPG](http://farm3.static.flickr.com/2673/3850124015_1528c7e83c_m.jpg)](http://www.flickr.com/photos/14396219@N00/3850124015/)</div>

I installed Zend Server CE on one of my machines. The next day I noticed that you could not checkout from the website. It would bring you to the to the Billing Address, then after you hit "continue" it would redirect you back to the checkout page. No errors that I could see.

Then I remember a problem I had many moons ago that was very similar and a forum comment jogged my memory. Magento is looking for a Com.php file buried deep in the bowls of Zend Framework. 

Did you know that Zend Server includes the latest copy of the Zend Framework? Yes and therein lies the problem, the Zend Framework version of Zend Server was overriding the built in Zend Framework of Magento.

## Easy solution

Open up your `.htaccess` file and add this section in there somewhere. This will reset your include path, Magento will build this for you anyway so it shouldn't cause a problem.

`

###########################################
## Reset Include path

   php_value include_path  "."

`

Resources:
http://www.magentocommerce.com/boards/viewthread/28231/#t200692