---
date: '2009-05-17T05:04:29-05:00'
description: Magento Search error "Integrity violation" etc.
id: 52
pubDate: '2009-05-17T05:04:29-05:00'
tags:
- Programming
title: Magento Search error "Integrity violation" etc.
---

Just an FYI, when you upgrade Magento, you may get an error for some searches.  Just rebuild your search index and all should be well.