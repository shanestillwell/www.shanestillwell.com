---
date: '2009-06-22T19:16:07-05:00'
description: After a Magento upgrade, refreshing the Magento cache gives a blank white
  error screen.
id: 56
pubDate: '2009-06-22T19:16:07-05:00'
tags:
- Programming
title: After a Magento upgrade, refreshing the Magento cache gives a blank white error
  screen.
---

So you've successfully upgraded Magento and now you're rolling live on the new code and db.... You did remember to save a copy DB in-case you have to back track? 

You go to the Cache Management page and try to refresh the cache (just because you want to cover all your bases). After clicking the button with confidence you get a white screen, no alerts, no friendly exception report, just white.

Checklist.
1\. Is your `var/` directory writable by the web server?
2\. Is your `app/etc/use_cache.ser` file writable by the web server?

That second one was the culprit this time, why is it that permissions are the problem 11 out of 10 times?