---
date: '2010-01-11T19:55:39-05:00'
description: Getting the Secure (HTTPS) url for Magento
id: 80
pubDate: '2010-01-11T19:55:39-05:00'
tags:
- Programming
title: Getting the Secure (HTTPS) url for Magento
---

Say you want to redirect a customer to a URL. In Magento you'd invoke this little bit of code to produce the URL.
<php>
Mage::getUrl('*/*/post') 
</php>  

This will produce a url like `http://www.example.com/<module>/<controller>/post`. The '*' just inserts the current module and controller.

Now you want to redirect to the secure version. Easy.  Just simply say so

<php>
Mage::getUrl('*/*/post', array('_secure'=>true)) 
</php>

Now that was simple.