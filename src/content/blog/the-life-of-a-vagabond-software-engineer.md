---
date: '2015-06-10T08:09:00-05:00'
description: The Life of a Vagabond Software Engineer
pubDate: '2015-06-10T08:09:00-05:00'
tags: []
title: The Life of a Vagabond Software Engineer
---

# The Life of a Vagabond Software Engineer, Part 1.

> Constant change requires reliable tools


Maybe you're like me, you love to change things up, find new places, explore the horizons. This could be the far flung corners of the earth, or just a part of town you've never been. I'm a die hard explorer, but I'm also a software engineer. I need to be connected, I need power, and I need an environment that works. In this post, we'll explore the tools. In subsequent posts, we will explore other factors, such as finding the right places to work, hints on helps.

## Minimalism

I gravitate towards minimalism (although in practice it may not always appear that way). I take only what I need and usually items should have dual purposes. The items you choose should be of high quality, durable, and fixable if need be.

> Go Light, Go Right.

## Technical Accouterments

So let's get right to it. I need reliable tools. My personal choice for computers is a [MacBook Pro 13"](http://www.apple.com/macbook-pro/). I've been using a Mac for almost 10 years now and have had little to no trouble. I usually buy a new one every two years (having a backup is also crucial in my line of work). I need to protect my Mac while it's in my pack so I use an [Aerostich Padded Laptop Computer Sleeve](http://www.aerostich.com/aerostich-padded-laptop-computer-sleeve.html).

For the technical needs I have a small [Granite Gear Ripstop Stuffsack](http://www.granitegear.com/outdoor/packing-systems/drawcord-stuffsacks/air-bags.html). In this bag you'll find.

![Stuff Bag](/images/the-life-of-a-vagabond-software-engineer-IMG_0911.JPG)

* [Etymotic HF5 Earphones](http://www.etymotic.com/consumer/earphones/hf5.html) (great for noise free listening)
* Simple lens cleaner (I wear glasses)
* MacBook Power cord in a bag (having cords in their own containers/bags really helps reduce tangle)
* MacBook Power Extension rolled up with Velcro ties
* iPhone wall charger
* iPhone Earphones in an old makeup remover case
* Granite Gear Stuff Sack
* iPhone cord
* Microfiber cloth
* [Moleskine Classic Notebook Squared 5x8.25](http://www.amazon.com/Moleskine-Classic-Notebook-Squared-Notebooks/dp/8883701135) (not in stuff sack, but inner pocket of the Deuter Backpack)
* Zebra Ball point pen

I can grab the bag in pack and easily have what I need to power up the Mac or iPhone, clean my glasses, take notes, jump on a conference call and more.


## Ready, Set, Eat!

![Cup and Spork](/images/the-life-of-a-vagabond-software-engineer-IMG_0912.JPG)

For the more culinary, eco friendly needs. I have a simple [GSI Cup](http://amzn.com/B000J086RK) and a [Snowpeak Titanium Spork](http://snowpeak.com/products/titanium-spork). Notice the lanyard tied around the spork, this really helps to hang items, find it a lot easier if you happen to drop it (e.g. in the snow). *Rant* I hate styrofoam cups and plastic forks and spoons. Think of the overflowing landfills that have billions of plastic spoons that were merely used to stir coffee for a few seconds then thrown away forever. *EndRant*

The **Girl Scout** cup is sort of a running joke. My wife picked it up at a garage sale as part of a kit for our son. He didn't want the branded cup, so I took it. Oh, and by the way *I'm a Scoutmaster in the Boy Scouts*, no scout ever wants to borrow my cup :D

## Survival & Emergency

While out of doors in this wonderful world, you will run into situations. It may be as simple as a band-aid, or more life-threatening involving emergency medical care and survival. In such cases there are a few items that can make all the difference.

![Survival Items](/images/the-life-of-a-vagabond-software-engineer-IMG_0913.JPG)

* Para cord rope
* Batteries
* Altoids Mini Container
 * Super glue
 * Cotton ball
 * Dental floss
 * Tweezers
 * Sewing needles
 * Mini compass (I was lost in the woods once, no fun)
 * Safety pin
* Banda ids & Alligator clips
* [Petzl eLite headlamp](http://amzn.com/B008AUBOTS)
* Real compass (yeah, being lost is no fun)
* Lighters (forget the matches)
* Ripstop bag to carry all these items
* [Leatherman Skeletool CX](http://amzn.com/B000XU43IC) (not pictured, on my person)


## The Rucksack

Lastly, you need a way to carry all those tools. I've used a [Duluth Pack](http://www.duluthpack.com/) in that past and it works well, but doesn't have the back support I'm looking for in a pack. My main stay is a [Deuter Trans Alpine 30 AC](http://www.deuter.com/US/us/bike/trans-alpine-30-32223-123.html) (I have an older model than the one in the link). The Deuter backpack has a waist belt to take the weight off your shoulders. Plenty of pockets (but not too many). It has an integrated rain cover if I get caught out in the weather (can't have that expensive Mac getting wet). It's just the right size if I want to repurpose it for a long day hike with the scouts.

## In Short

So that's most of my kit. I didn't mention the [Kermit Kamping Chair](http://www.kermitchair.com/) that I use if I know I'm going to be sitting in the woods or park somewhere, or the smaller more personal items (such as flushable wet wipes for the those times, you know). Stay tuned for the next installment where we'll discuss the ins and outs of finding good Wifi and other great things about working remote, working from anywhere.
