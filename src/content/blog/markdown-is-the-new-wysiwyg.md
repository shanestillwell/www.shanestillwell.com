---
date: '2012-11-09T03:29:52-05:00'
description: Markdown is the new WYSIWYG
id: 524
pubDate: '2012-11-09T03:29:52-05:00'
tags:
- Programming
title: Markdown is the new WYSIWYG
---

If you publish content on the web, get ready, a shift started a while back, but now gaining momentum. I first encountered **[Markdown](http://daringfireball.net/projects/markdown/)** a few years ago and had a fancy for it, but never took the time to learn it. I didn't really have a use for it. Now that I've had to use it on [StackOverflow](http://stackoverflow.com), [GitHub](https://github.com), and other great sites, I'm convinced it's time to learn it.

There is another impetus pushing me, [Markdown may become a standard](http://www.codinghorror.com/blog/2012/10/the-future-of-markdown.html). So from now, I'm going to be upping the ante and blogging in Markdown.