---
date: '2010-01-23T06:52:39-05:00'
description: CSS Styling Input Radio Form Controls with Labels above buttons
id: 84
pubDate: '2010-01-23T06:52:39-05:00'
tags:
- CSS
- Design
title: CSS Styling Input Radio Form Controls with Labels above buttons
---

I just wanted a nice way to style my radio buttons sort of inline, but with the labels above the radios. Here is how I accomplished this.

```css
ul { overflow: auto; padding: 2px 0;}
ul li { float: left; text-align: center; margin: 0 10px}
ul label { display: block;}
```

Line one [clear the floats](http://www.quirksmode.org/css/clearing.html) after the UL.
Line two floats the LI.
Line three displays the label as a block.

The HTML
```html
<ul>
	<li><label for="rtw0">Never</label>
<input id="rtw0" type="radio" name="ridetowork" value="0" /></li>
	<li><label for="rtw1">1</label>
<input id="rtw1" type="radio" name="ridetowork" value="1" /></li>
	<li><label for="rtw2">2</label>
<input id="rtw2" type="radio" name="ridetowork" value="2" /></li>
	<li><label for="rtw3">3</label>
<input id="rtw3" type="radio" name="ridetowork" value="3" /></li>
	<li><label for="rtw4">4</label>
<input id="rtw4" type="radio" name="ridetowork" value="4" /></li>
	<li><label for="rtw5">5</label>
<input id="rtw5" type="radio" name="ridetowork" value="5" /></li>
	<li><label for="rtw7">more than 5</label>
<input id="rtw7" type="radio" name="ridetowork" value="7" /></li>
</ul>
```
This will produce nicely formed radio buttons with labels on top. Screenshot.

![screenshot.png](/images/css-styling-input-radio-form-controls-with-labels-above-buttons-screenshot.png)
