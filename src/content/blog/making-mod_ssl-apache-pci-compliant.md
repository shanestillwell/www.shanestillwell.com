---
date: '2008-11-04T15:48:19-05:00'
description: Making mod_ssl (Apache) PCI compliant
id: 38
pubDate: '2008-11-04T15:48:19-05:00'
tags:
- Server Admin
title: Making mod_ssl (Apache) PCI compliant
---

Here are some configurations in Apache and PHP needed to make your server PCI compliant.

## SSL Configuarations

Only allow secure SSL versions (version 3 and TLS 1)
`
SSLProtocol -ALL +SSLv3 +TLSv1
`

Only allow secure Ciphers
`
SSLCipherSuite ALL:!ADH:RC4+RSA:+HIGH:+MEDIUM:-LOW:-SSLv2:-EXP
`

## PHP Configurations

Keep PHP from giving out info about itself (php.ini)
`
expose_php = Off
`

Also of note, PHP 4 is dying. Need to move to PHP 5 asap.