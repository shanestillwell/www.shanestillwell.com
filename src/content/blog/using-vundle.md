---
date: '2013-09-30T19:23:51-05:00'
description: Using Vundle for Vim Plugin Management
pubDate: '2013-09-30T19:23:51-05:00'
tags: []
title: Using Vundle for Vim Plugin Management
---

Vim is the end all, be all of text editors. I don't care how great Sublime is, or the like. Vim has been around for a long time and for very good reason. You can edit text supremely fast. The plugins just keep getting better. 

## Vim Plugins 

So of course I use plugins. I might try new ones here and there but [I have a number of tried and true plugins](https://github.com/shanestillwell/dotvim) that I always install. Some that must be noted because they must be noted.  

* **[NERDTree](https://github.com/scrooloose/nerdtree)** A file explorer that has more than just that. 
* **[Fugative](http://github.com/tpope/vim-fugitive.git)** A git plugin that will turn you into a git spouting fool
* **[Syntastic](https://github.com/scrooloose/syntastic.git)** Code syntax highlighting that will give you goose bumps
* **[Tabular](https://github.com/godlygeek/tabular.git)** Makes lining up `vars` by their `=` a breeze


## [Pathogen](https://github.com/tpope/vim-pathogen)

This was a great way to install bundles, you just drop them into the folder and you were rolling. You did need to roll your own gitsubmodule setup if you wanted to propagate your setup to another machine. That works, but man, submodules are a pain in the butt.

## [Vundle](https://github.com/gmarik/vundle) 

Vundle goes a step further and lets you manage your plugins right in your `.vimrc` file. After you've set them up in your `.vimrc` file, then just run the command `:BundleInstall`. Vundle will go through your different plugins, go get them and install them. It Just Works. 

## Resources 
* https://github.com/gmarik/vundle
