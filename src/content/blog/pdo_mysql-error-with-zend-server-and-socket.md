---
date: '2010-03-09T02:01:39-05:00'
description: PDO_Mysql error with Zend Server and socket
id: 96
pubDate: '2010-03-09T02:01:39-05:00'
tags:
- Programming
title: PDO_Mysql error with Zend Server and socket
---

I've headed this issue off before, but now I think I know why it happens.

## The problem

With a new install of Zend Server on my Mac and trying to access a DB with my Zend Framework web app I am greeted with an error of 

`
Message: SQLSTATE[HY000] [2002] Can't connect to local MySQL server through socket '/tmp/mysql.sock' (2)" when trying to run our application.
`

This was rather frustrating as I changed all instances of `mysql_default_socket` in the PHP settings (php.ini). It still didn't pick up the location of the socket. Since I'm using PDO, it looks like this is an issue in PHP 5.2 for PDO.

http://forums.zend.com/viewtopic.php?f=44&t=568

The work around for me has been to declare the socket in my application.ini file

`
resources.db.params.unix_socket = "/usr/local/zend/mysql/tmp/mysql.sock"
`

The problem this introduces, having a different application.ini on the server and on then on my local dev machine.

> Ugggg.