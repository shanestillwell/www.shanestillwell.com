---
date: '2010-08-27T04:03:17-05:00'
description: Zend_Tool and APPLICATION_ENV [SOLVED]
id: 115
pubDate: '2010-08-27T04:03:17-05:00'
tags:
- Programming
title: Zend_Tool and APPLICATION_ENV [SOLVED]
---

I've set up my project on a new server getting prepared to go live. With that comes, moving around some files getting things ready. One thing I'm working on it getting the Zend_Tool command line working. 

On the server, I've configured <pre>APPLICATION_ENV</pre> to be <pre>production</pre> via <pre>.htaccess</pre>

## Problem

When I run <pre>zf.sh show doctrine</pre> it errors out because it's trying to use my "development" DB config rather than the "production" one.  I'm not sure how to get around this yet.

## Solution

Not pretty, but you have to edit the source of the Zend Framework.  The file to edit is 
`ZendFramework/library/Zend/Tool/Project/Context/Zf/BootstrapFile.php`

Here is the diff for what needs to be changed.

`
--- BootstrapFile.php (saved version)
+++ (current document)
@@ -106,9 +106,11 @@
                 define('APPLICATION_PATH', $this->_applicationDirectory->getPath());
                 $applicationOptions = array();
                 $applicationOptions['config'] = $this->_applicationConfigFile->getPath();
+                
+                $env = getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development';

                 $this->_applicationInstance = new Zend_Application(
-                    'development',
+                    $env,
                     $applicationOptions
                     );
             }
,

`

Then you need to add the **APPLICATION_ENV** environmental variable to your shell.  

In OS X, I've found this to work:

Insert this at the end of your `.bash_profile`
`
declare -x APPLICATION_ENV="local"
`

Now you can declare an environment in application.ini that works for the Zend Tool (and ZFDoctrine in my case).