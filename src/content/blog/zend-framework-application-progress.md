---
date: '2010-01-03T05:30:31-05:00'
description: Zend Framework Application Progress
id: 75
pubDate: '2010-01-03T05:30:31-05:00'
tags:
- Programming
title: Zend Framework Application Progress
---

Tonight's programming lessons included

*   Removing Decorators (dt, dd HTML tags) from Zend_Form_Element_Hidden types. There are several [Decorators](https://web.archive.org/web/20100419075756/https://framework.zend.com/manual/en/zend.form.elements.html) by using <php>removeDecorator</php>
  `
    $addressid = new Zend_Form_Element_Hidden('addressid');
    $addressid->setAttrib('id', 'form-addressid')
              ->removeDecorator('HtmlTag')
              ->removeDecorator('Label');
  `

*   Escaping output (removing slashes i.e. O'reilly would be stored in the DB as O\'reilly). We want to just show O'reilly to the visitor.
  Normally in the view you would just enter 
  <php>
    $this->escape($data);
  </php>

  But, I was using a [View Helper](https://web.archive.org/web/20100418162930/http://framework.zend.com/manual/en/zend.view.helpers.html "Zend Framework: Documentation") so I needed to add some special sauce to the view helper to get it to know how to escape.
  <php>
    class Zend_View_Helper_AddressHtml {

        public $view;

        public function addressHtml($data) {
            $this->view->escape($data['street']);

        }

        // This will set the view variable so you can use it above
        public function setView(Zend_View_Interface $view)
        {
            $this->view = $view;
        }

    }
  </php>

  Also, to alter the default way to strip out slashes you would use the <php> init()</php> method in the controller
  <php>
    public function init() {
      $this->view->setEscape('stripslashes');
    }
  </php>

  Credits: 
  [Matthew Weier O'Phinney] `devzone.zend.com/article/3412`
*   Force Zend_DB Query to return an object instead of a select
    <php>
      $db->setFetchMode(Zend_db::FETCH_OBJ);
    </php>