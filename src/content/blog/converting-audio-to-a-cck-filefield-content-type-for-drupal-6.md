---
date: '2011-11-03T00:38:28-05:00'
description: Converting Audio to a CCK FileField content type for Drupal 6
id: 152
pubDate: '2011-11-03T00:38:28-05:00'
tags:
- Programming
title: Converting Audio to a CCK FileField content type for Drupal 6
---

This is the script I used to convert our [Audio](http://drupal.org/project/audio) content type to [CCK](http://drupal.org/project/cck) [FileField](http://drupal.org/project/filefield) content type.

&nbsp;

1.  I created a content type called **podcast**
2.  Copy the following script to a file _convert.php_ and place it in your website's DocumentRoot
3.  [https://gist.github.com/1335420](https://gist.github.com/1335420)
4.  Run the script by going to your website `http://example.com/convert.php`
5.  Done... now the audio nodes show be podcast nodes and will work.
<div>For extra fun, I've added [SWFTools](http://drupal.org/project/swftools)</div>

### Resources

This was of limited help
[http://geeksandgod.com/tutorials/computers/cms/drupal/drupal-converting-audio-module-filefield-module](https://web.archive.org/web/20161126165629/http://geeksandgod.com/tutorials/computers/cms/drupal/drupal-converting-audio-module-filefield-module)