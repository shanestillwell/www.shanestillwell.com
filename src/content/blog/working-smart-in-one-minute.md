---
date: '2008-08-06T17:19:46-05:00'
description: Working SMART... in One Minute
id: 29
pubDate: '2008-08-06T17:19:46-05:00'
tags:
- Good Ideas
title: Working SMART... in One Minute
---

I came across a blog post today on working [SMART](https://en.wikipedia.org/wiki/SMART_criteria). When managing people you need to work SMART which is 
* **S**pecific 
* **M**easurable 
* **A**chievable 
* **R**easonable 
* **T**imeboxed 

It reminds me of similar advice from the *One Minute Manager*