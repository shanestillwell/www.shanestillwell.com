---
date: '2008-10-02T22:02:49-05:00'
description: Picking a domain name
id: 34
pubDate: '2008-10-02T22:02:49-05:00'
tags:
- Good Ideas
title: Picking a domain name
---

Do you have the perfect business you are going to start? Is your business long been established with a well known name? In either case you need to make a critical choice when choosing a name for your website. Once you launch your site with a name, it's hard to change without running into some pitfalls. A little bit of fore thought could save lots of problems for you and your visitors.

Here are some tips for picking the perfect name. I've arranged them in order of importance.

## You must own the .com 

You must own the .com extension to your desired name. Even if you plan to use .org or .net, the .com must be owned by you. Why? Keep reading.

I did some work for a large hospital here called SMDC (`www.smdc.org`). Before I started working there I stumbled on their website and noticed some job openings. A few days later I wanted to check the job openings again, so I remember it being smdc.com, I typed `www.smdc.com` in my browser and BAM! Pornography! Oops. What went wrong? I checked the URL, then Googled, looks like it should have been `www.smdc.org`. 

The .com extension is by far the most popular. Most novice web users are not acutely aware there are other extensions that could be entirely different organizations with .net, .org, .us, .cc, etc.

## Short and Sweet

That's right, as short as possible is the best solution. Why keep it short? First of all, people are going to be writing this URL down both to visit the website and if you decide to use it for email. If it's long you can image that mistakes will happen. 

Second, short is more appealing and makes your organization look more profession. What do you think is more authoritative on welding?

`www.larrysweldingsupplyandservice.com`
-or-
`www.cloquetwelding.com`

Yeah, you think the smaller one.

If your business doesn't have a name yet, then you are in a good spot to see what names are available and pick one that suits getting the best match for both the domain and the business name.  When your business is already operating under a name you need to find domain names that can best describe your business without being too long.

## No Numbers or Hyphens

Unless your company has a number in its name (3M) then I would stay away from numbers.  There is also a temptation to use hyphens (-) in the name when your name is not available. For example, let's say you want woodworkers.com, and it's taken, but you find out that wood-workers.com is not. DON'T DO IT!! 

Why? Again, confusion. Let's say you tell a client your website name, they forget, or do not catch the hyphen and end up at a different website all together. You can image the problems that could cause. 

## Be careful of geological references in your name

Huh? I mean if you include a city name, state name, or regional name in your domain, then be sure that is what you want. The problem? Getting established on the web could change where your customers are coming from.  You may expand some day to reach customers that are not familiar with your region.  The web can do that.

Case in point.
There is a motorcycle tour company in Anchorage Alaska called AKrider.com. This tour company has seen some growth and is now doing some tours in different parts of the world.  It's plain that the URL akrider.com does not give the customer confidence that they are also a South America tour guide. 

Even if you are starting small, your vision is to grow, so think big now and prepare for growth when you get that domain and launch out into the web.