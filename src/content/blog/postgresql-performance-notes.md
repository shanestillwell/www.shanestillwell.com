---
date: 2018-02-07 15:29:51
description: PostgreSQL Performance Notes
pubDate: '2018-02-07T15:29:51'
tags:
- PostgreSQL
- Work in Progress
title: PostgreSQL Performance Notes
heroImage: /images/postgresql-performance-notes.png
---

Databases are magic from my perspective. They take in queries, find all the information you tasked it to find, and *POOF*. In the blink of an eye, there it is, all packaged up nice and neat. *Or it blows up in your face, but that never happens, right?* Let's take a look at some steps we can take when things are not returned in the blink of and eye. What can we do to improve our database performance.


## Queries to use

Let's use this query to get a quick overview of what tables in your database are getting scanned too often and could benefit from a carefully placed *index*.

```SQL
SELECT relname, seq_scan-idx_scan AS too_much_seq, case when seq_scan-idx_scan>0 THEN 'Missing Index?' ELSE 'OK' END, pg_relation_size(relname::regclass) AS rel_size, seq_scan, idx_scan
FROM pg_stat_all_tables
WHERE schemaname='public' AND pg_relation_size(relname::regclass)>1000
ORDER BY too_much_seq DESC;
```

That will give us a nice starting point to see if maybe we need to employ some indexes

<table>
	<tr>
		<th>relname</th>
		<th>too_much_seq</th>
		<th>case</th>
		<th>rel_size</th>
		<th>seq_scan</th>
		<th>idx_scan</th>
	</tr>
	<tr>
		<td>people</td>
		<td>12722526</td>
		<td>Missing Index?</td>
		<td>8192</td>
		<td>12722538</td>
		<td>12</td>
	</tr>
	<tr>
		<td>addresses</td>
		<td>11098278</td>
		<td>Missing Index?</td>
		<td>17104896</td>
		<td>13395721</td>
		<td>2297443</td>
	</tr>
	<tr>
		<td>phones</td>
		<td>6065872</td>
		<td>Missing Index?</td>
		<td>294912</td>
		<td>6077693</td>
		<td>11821</td>
	</tr>
	<tr>
		<td>events</td>
		<td>3599326</td>
		<td>Missing Index?</td>
		<td>8192</td>
		<td>3599381</td>
		<td>55</td>
	</tr>
	<tr>
		<td>pterodactyl</td>
		<td>3343980</td>
		<td>Missing Index?</td>
		<td>8192</td>
		<td>3350885</td>
		<td>6905</td>
	</tr>
	<tr>
		<td>trex</td>
		<td>3153419</td>
		<td>Missing Index?</td>
		<td>8192</td>
		<td>3153463</td>
		<td>44</td>
	</tr>
</table>

Thanks to https://github.com/miguelvps for the great query


----


## Tools to Analyze

## Explain Visualizer

If you have a query, you can use the [EXPLAIN](https://www.postgresql.org/docs/current/static/sql-explain.html) directive to understand how Postgres plans a query. This is helpful to identify bottlenecks and areas that are costly to run.

```SQL
EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
SELECT * FROM people
WHERE age > 25;
```

Take the resulting JSON from that query and paste it into a **New Plan** on http://tatiyants.com/pev/#/plans

From there, you can explore which areas of the query are most costly and suggestions for improvement.

## A Quick Database Inspection Tool

If you want to try a quick db inspection tool. Look into https://github.com/ankane/pghero.


## More Links

* https://www.geekytidbits.com/performance-tuning-postgres/
