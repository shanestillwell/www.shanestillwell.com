---
date: '2010-02-02T02:13:43-05:00'
description: Working with SVN branches
id: 88
pubDate: '2010-02-02T02:13:43-05:00'
tags:
- SVN
- Programming
title: Working with SVN branches
---

To keep all may changes different from the main development I've created a subversion branch.  Now I want to keep the branch up to date with changes from the main trunk.

## Get all the history of this particular branch.

`
svn log --stop-on-copy
------------------------------------------------------------------------
r421 | sstillwell | 2010-01-22 18:40:42 -0600 (Fri, 22 Jan 2010) | 1 line

merged changes from trunk to branch, 406:420
------------------------------------------------------------------------
r420 | sstillwell | 2010-01-22 18:30:52 -0600 (Fri, 22 Jan 2010) | 1 line

Still setting up the Sizing Tool Form
------------------------------------------------------------------------
r413 | sstillwell | 2010-01-15 12:51:25 -0600 (Fri, 15 Jan 2010) | 1 line

More changes to Sizing Tool
------------------------------------------------------------------------
r408 | sstillwell | 2010-01-06 17:22:55 -0600 (Wed, 06 Jan 2010) | 1 line

Merging Trunk into Sizingtool branch
------------------------------------------------------------------------
r406 | sstillwell | 2010-01-06 17:09:12 -0600 (Wed, 06 Jan 2010) | 1 line

Initial creation of Sizing Tool module
------------------------------------------------------------------------
r405 | sstillwell | 2010-01-06 16:25:39 -0600 (Wed, 06 Jan 2010) | 1 line

Creating a branch for the Sizingtool
------------------------------------------------------------------------
`

From the command `svn log --stop-on-copy` we can see that the branch was created at version 405.

## Merge updates from the main trunk to my branch

Change directory to the top directory of your particular branch (e.g. application/branches/new-module-dev/)

Now take a dry run at the merge
`
svn merge --dry-run -r 405:421 http://svn.example.com/repos/application/trunk/
`

Everything look OK? remove `--dry-run` and really apply them.

`
svn merge -r 405:421 http://svn.example.com/repos/application/trunk/
`

**Remember**! You're only updating your local copy, you still have to commit these merged updates.

Code Strong!

Credits:
http://wiki.fluidproject.org/display/fluid/Subversion+Tips+and+Tricks