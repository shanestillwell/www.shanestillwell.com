---
date: '2008-05-19T18:57:45-05:00'
description: Drupal Auto Node Title for multiple nodereference
id: 18
pubDate: '2008-05-19T18:57:45-05:00'
tags:
- Programming
title: Drupal Auto Node Title for multiple nodereference
---

This is for a site I've put together that has a node referencing multiple other nodes.  Token doesn't handle multiple values yet so I had to strike out and use standard PHP for this.

This is customized for my site, but the point is there.

<php>
  <?php
    global $form_values;
    $node = (array)$form_values;
  foreach ($node['field_connected'] as $key => $value){
  $z = $value['node_name'];
  if(!empty($z)){ 
  $x[] .= preg_replace('/ \[nid:\d+\]/','',$z);
  }
  }
  return implode(' - ', $x);
  ?>
</php>