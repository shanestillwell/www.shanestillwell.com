---
date: '2009-12-09T04:17:58-05:00'
description: Developing in jQuery? Use $.dump instead of alert
id: 72
pubDate: '2009-12-09T04:17:58-05:00'
tags:
- jQuery
- Programming
title: Developing in jQuery? Use $.dump instead of alert
---

I just ran into the $.dump jQuery plugin.  It sure makes troubleshooting and figuring out jQuery a whole lot easier.  Here is how you include it in your page.  

*   Download the plugin at [here](http://www.netgrow.com.au/assets/files/jquery_plugins/jquery.dump.js)
*   Include it in your HTML like so
```
<script type="text/javascript" src="jquery.dump.js"></script>
```

*   Now just include `$.dump(object)` somewhere  and replace _object_ with an object you want more information on

The pop-window you will get 
![dump](/images/Screen shot 2009-12-08 at 10.15.06 PM.png)
