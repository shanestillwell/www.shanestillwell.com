---
date: '2012-02-02T17:31:45-05:00'
description: Performing a 'like' match in MongoDB
id: 290
pubDate: '2012-02-02T17:31:45-05:00'
tags:
- MongoDB
- Programming
title: Performing a 'like' match in MongoDB
---

So looking through the [Doctrine ODM docs on Query Building](https://web.archive.org/web/20111127201012/http://www.doctrine-project.org/docs/mongodb_odm/1.0/en/reference/query-builder-api.html), I didn't see anything that lent itself to a _like_ query such as that is found in MySQL. There isn't a _like_, option, but using **Regular Expressions** will work just fine for me.

<pre lang="PHP">
$queryBuilder->field('title')->equals(new \MongoRegex('/keyword/i'));
</pre>

That will perform a case-insensitive query for the term '_keyword_' in the field '_title_'

Resources
[http://groups.google.com/group/doctrine-user/browse_thread/thread/daadb4e3030c196c ](http://groups.google.com/group/doctrine-user/browse_thread/thread/daadb4e3030c196c )