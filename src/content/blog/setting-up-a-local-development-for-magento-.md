---
date: '2009-11-08T05:13:14-05:00'
description: Setting up a local development for Magento.
id: 69
pubDate: '2009-11-08T05:13:14-05:00'
tags: []
title: Setting up a local development for Magento.
---

mod_rewrite for /media

Symbolic links for 
.htaccess
local.xml

SVN:ignore for 
.htaccess
local.xml

Install modules via pear on local dev.
