---
date: 2021-06-04 22:35:51
description: The Bad Drives Out The Good
pubDate: '2021-06-04T22:35:51'
tags:
- rant
title: The Bad Drives Out The Good
heroImage: '/images/the-bad-drives-out-the-good-thumbnail.jpg' 
---

In a nutshell, Grisham's law talks about how the more valuable option is held back from circulation, thereby only the less value or **bad** money remains in circulation. If you want to learn more, [this example](https://www.youtube.com/watch?v=vFY_kCzeiBk) on YouTube explains Grisham's Law demonstrated in the Used Car Market. So the Bad Drives out the Good. This law can be observed in different markets and is just an extension of human nature.


> WARNING: Rant ahead, best to eject now while you still can.

## Let's take a different twist on Grisham

I've worked at many different organizations big and small; [Meijer](https://www.meijer.com/), [Under Armour](https://www.ua.com), [Brightcove](https://www.brightcove.com/en/), [Hilton](https://www.hilton.com/en/), [Allergan](https://www.allergan.com/). Over those many opportunities there have been different types of developers and managers I've had the pleasure to work along side. Some I still keep in contact to this day. However, I'm sad to say I worked with bad developers that have been nothing but heachaches for my coworkers and me. Following [Dunning-Kruger Effect](https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect), these developers are unaware they fall below the bar.

The Reddit user [flipstables](https://www.reddit.com/user/flipstables/) posted a list of random thoughts from a Senior Software Engineer called [Drunk Post: Things I've learned as a Sr Engineer](https://www.reddit.com/r/ExperiencedDevs/comments/nmodyl/drunk_post_things_ive_learned_as_a_sr_engineer/). I loved reading the post and a few items stood out to me.

> The best way I've advanced my career is by **changing companies**...
> There's a reason why people recommend job hunting. If **I'm unsatisfied at a job**, it's probably time to move on... I've learned to be honest with my manager. Not too honest, but honest enough where I can be authentic at work. What's the worse that can happen? He fire me? **I'll just pick up a new job in 2 weeks**.

**Do you see the theme?** Senior level software engineers can pick up and find a new gig in a few short days. The Good ones have no trouble jumping ship. The bad ones are more than happy to stay and make things even worse.

## How do you tell your Good Software Devs from the Bad ones?

At first glance this might seem like a very difficult task. Maybe you need to rank the employees from best to worst performers, or look at other indicators such as seniority, experience, productivity, or just your gut. Nah, there is a much easier question to ask yourself.

> How would I feel if they handed in their two weeks notice?

Honestly. Would you go into a cold sweat thinking how it sets your organization's plans back by months? Would it be something more subdued, "sorry to see you go, best of luck."? I think you catch the difference. Your good software engineers are instrumental to your plans of world domination, the bad ones can be replaced.

## My completely biased opinion on what bad devs are guilty of

* Poor communication: Does the dev go dark for days, then resurfaces to say, "still working on it"?
* Introduces new technology without approval or buy in from other developers
* Doesn't understand [YAGNI](https://martinfowler.com/bliki/Yagni.html), likes to implement solutions because of the **benefits** and can't elaborate on the current pain points it solves
* Needs hand holding from other Senior Developers
* Consistently does NOT complete the work assigned
* The Good devs consistently surface issues to you about the bad dev 👈

## How do you keep the good ones happy?

Is it a Foosball Table? Free Food? Beer on tap in the office? Those gimmicks work to bring people in, but not keep them. How do you keep them? Remember, Sr Software Engineers are in extremely high demand and will be for decades to come, they can easily leave and find a new gig next week. We could go into money, autonomy, working on interesting projects, a sense of purpose in your company. The most important thing is to **make their life easier**.

How do you *make their life easier*? Fire the bad ones.

I can hear your objection now, *but software developers are hard to find*. Good Ones are hard to find, bad ones are more than happy to jump in and start causing destruction.

## The other side of the coin

Not only will bad devs drive out good devs from your company, but good devs are hard to find for the very reason that they can find a good job in a short amount of time. The bad ones remain in the market of devs open to new work, while the good ones usually find something quickly and jump ship. This has been my experience, finding new work isn't the problem, it's deciding if I want to work at said company.

/rant

## References

* https://skamille.medium.com/an-incomplete-list-of-skills-senior-engineers-need-beyond-coding-8ed4a521b29f
