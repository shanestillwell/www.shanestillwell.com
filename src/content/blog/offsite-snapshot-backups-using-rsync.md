---
date: '2009-07-22T14:52:47-05:00'
description: Offsite Snapshot Backups using rsync
id: 63
pubDate: '2009-07-22T14:52:47-05:00'
tags:
- Server Admin
title: Offsite Snapshot Backups using rsync
---

###  Creating simple backup snapshots using rsync and mv

#### Summary 

Create the illusion of multiple backed up copies, but in reality only the changes are different thereby saving space on the HDD. Create a cron job to run the following script once a day. The script will keep 4 days of backups, edit the numbers to your liking. Utilizing some built in rsync features it will only store 1 copy of the file on disk unless it has changed.  

#### The script 

` 
#!/bin/bash
rm -rf backup.3
mv /backup/backup.2 /backup/backup.3
mv /backup/backup.1 /backup/backup.2
mv /backup/backup.0 /backup/backup.1
rsync -a --delete --link-dest=../backup.1 -e "ssh -p 75" root@192.168.1.50:/home/ /backup/backup.0/
` 

The last line is the most important.  The format is
 ` rsync -a --delete --link-dest=../backup.1 <source directory> <destination>
` 

If it was on the same host then it would be something like

 ` rsync -a --delete --link-dest=../backup.1 /home/ /backup/backup.0/
` 

The link-dest argument states to just create hard links for files that have not changed.

#### Specifying directories to be included

You could get more fancy with what directories you want to backup.  You can tell the script to sync different directories, here is where rsync gets a little funny.  Here is the new line with the include directive.
`rsync -a --delete --link-dest=../backup.1 -e "ssh -p 75" root@192.168.1.50:/ /backup/backup.0/ --include-from=include.txt`

Then in the same directory as the script you would place a file `include.txt`

`
+ /etc/
+ /etc/**
+ /root/
+ /root/**
+ /home/
+ /home/**
+ /var/
+ /var/www/
+ /var/www/**
+ /var/lib/
+ /var/lib/**
+ /backup/
+ /backup/**
- *
`
Notice how for each directory there are two lines, and at the end, there is a exclude statement.

#### References 

[http://www.mikerubel.org/computers/rsync_snapshots/](http://www.mikerubel.org/computers/rsync_snapshots/ "Easy Automated Snapshot-Style Backups with Rsync")