---
date: '2012-05-29T13:23:06-05:00'
description: Appcelerator Titanium Twitter Birdhouse Failing because of HTTP
id: 332
pubDate: '2012-05-29T13:23:06-05:00'
tags:
- Twitter
- Programming
title: Appcelerator Titanium Twitter Birdhouse Failing because of HTTP
---

This problem caused me much angst and it's partly Twitter's fault (just ask me).

I'm using the great Titanium library for Twitter called [Birdhouse](https://github.com/jpurcell/birdhouse). It does a great job requesting authorization, storing it, and gives you an API to post tweets. So I was using this code to post tweets using [Birdhouse](https://github.com/jpurcell/birdhouse).
```javascript

this.tweet = function(message) {
  Ti.API.info('Im about to post to twitter:');
  Ti.API.info(escape(message));
  BH.api('http://api.twitter.com/1/statuses/update.json', 'POST', 'status=' + escape(message), function(resp) {
    Ti.API.info(resp);
    if (resp !== false) {
      Ti.API.info('Twitter Post Successful');
      if (typeof(callback) == 'function') {
        callback(true);
      }
      return true;
    } else {
      Ti.API.info('Posting to twitter failed:');
      if (typeof(callback) == 'function') {
        callback(false);
      }
      return false;
    }
  }, true);
};
```

The problem with this code is that it would only work sporadically. It drove me crazy thinking maybe the access token was not getting set. Then I printed out the error from twitter and it went like this
<pre>403 Forbidden: The server understood the request, but is refusing to fulfill it.</pre>
You know what the problem was? I was** using HTTP instead of HTTPS**. It was working sometimes, but not others, and it would be sporadic.

### Resources

`https://dev.twitter.com/discussions/6530` Defunct URL

&nbsp;