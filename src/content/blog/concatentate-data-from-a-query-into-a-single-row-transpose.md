---
date: '2010-04-15T06:00:52-05:00'
description: Concatenate data from a query into a single row (Transpose)
id: 104
pubDate: '2010-04-15T06:00:52-05:00'
tags:
- SQL
- Programming
title: Concatenate data from a query into a single row (Transpose)
---

Let's say you have a query that returns this
<pre>cert_id
---------
100
200
300
500
600</pre>
What I want is to have it return this in a query
<pre>cert_id
----------
100,200,300.500,600</pre>
After much searching on the topic of transpose, with some complex solutions offered, I stumbled onto the
<pre>GROUP_CONCAT</pre>
function in Mysql.
https://dev.mysql.com/doc/refman/5.0/en/group-by-functions.html

So my query looks like this
/* Show Gift Cert numbers */
(
SELECT GROUP_CONCAT(cert_id) FROM ugiftcert_history uh
WHERE so.entity_id = uh.order_id
GROUP BY uh.order_id
) AS gc_numbers,

Onto greatness.
