---
date: 2022-06-05 12:54:58
description: In Defense of Feature Flags
pubDate: '2022-06-05T12:54:58'
tags:
- Programming
title: In Defense of Feature Flags
heroImage: '/images/in-defense-of-feature-flags-thumbnail.jpg'
---

Feature Flags, Feature Toggles, Feature Switches. What ever you can call them, they are a tool to easily switch a behavior in your code without making code changes. They are behavior based.


## Uses Cases I've had for Feature Flags

1. I have an app that saves emails sent as an html file in S3. Locally, I only want it to do this when I'm working on that part of the app. On stage, I might want to turn off and on depending on the circumstances. In prod, I most likely always want it on. Using a Feature Flag toggle for this allows me to enable this feature without a code change, without a new deploy. Flip the switch, wait a few seconds and the app starts behaving differently.

1. I want to allow a few people to see new behavior in either the UI or the server. Maybe I've developed a new navigation for the dashboard and I don't want everyone to use it right away while I test it out. Using a Feature Toggle gives me the ability to interact with this new nav in production and not worry about the normal users being affected.

1. A/B Testing. Feature Toggles open up the world of A/B testing. Serving Feature A to half of your users and Feature B to the other half gives you the opportunity to test it's effectiveness.

## Pitfalls to Feature Flags

### Dead Code
If you leave a Feature Flag lingering. Let's say you developed a feature and turned it on for everyone. If you don't do your diligent duty and remove the code that enable the toggle, you'll have dead code sitting around. This 

### Complexity in Testing
When you have feature toggles and want to test, do you test with the toggle on or off or both? You may be tempted to say both, and this might work if your feature toggles are simple, but if you don't take care and remove the stale feature flags, then you keep that code testing complexity around. 

> The danger with Feature Flags is that they can introduce considerable complexity. Which version of your code do your test? Feature on or off? Both? If “both”? you are on a journey into exponential complexity growth as you add more flags – Flag “A” on Flag “B” on, Flag “A” on Flag “B” off, and so on! This is a never-ending game that you can’t win in any definitive way.  --Dave Farley

## Definition of Done

This was a recent objection to the use of Feature Flags on a team. The objection was, we have a process and using feature flags mean we'd have to change our process. Some may object to having to create a ticket that removes a Feature Flag. Another option is to just make it a part of your monthly technical debt payments, remove stale feature flags.

## So Why Use Them

Every tool in software engineering is abused by some and improperly implemented by others. Anyone been at a place that Scrum just didn't seem to be implemented correctly and was sometimes going through a charade? Me too. The same with Feature Toggles, done wrong and they will be more of a hassle then they are worth. Done right, they can speed up your development cycles since you're not holding a big feature back in a git branch waiting to make it live.


## References
* https://www.davefarley.net/?p=255
* https://www.martinfowler.com/articles/feature-toggles.html
* [Feature Flags v Environment Variables](/feature-flags-versus-environment-variables/)

Photo by [Erik Mclean](https://unsplash.com/@introspectivedsgn?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/shield?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
