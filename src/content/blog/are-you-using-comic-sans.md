---
date: '2008-10-06T02:29:09-05:00'
description: Are you using Comic Sans?
id: 35
pubDate: '2008-10-06T02:29:09-05:00'
tags:
- Design
title: Are you using Comic Sans?
---

If you are using this 'cute' font on your website or even in your stationary. **STOP**! This very instance. I know when you first discover this font it's different and stands out, so you feel compelled to use it to spice up your text with some groovy font to show you are different, bold, and spontaneous. 

Sorry, this is not the case. Comic Sans font is trite and considered very, very amateur. 

Do yourself a favor, resist temptation, and do not use Comic Sans.

Comic Sans