---
date: '2010-04-11T03:08:15-05:00'
description: Lessons in Magento Today
id: 103
pubDate: '2010-04-11T03:08:15-05:00'
tags:
- Programming
title: Lessons in Magento Today
---

## Printing the SKU in the cart Simple or Configurable

To print the sku in the shopping cart, even for Configurable (it will print the underlying simple product sku).

Place this code somewhere in:
app/design/frontend/default/default/template/checkout/cart/item/default.phtml

if (is_object($this-&gt;getChildProduct())):
echo $this-&gt;getChildProduct()-&gt;getSku();
else:
echo $_item-&gt;getSku();
endif;

This will now print the sku... onto more work.

Reference:
http://www.magentocommerce.com/boards/viewthread/56728/#t214381