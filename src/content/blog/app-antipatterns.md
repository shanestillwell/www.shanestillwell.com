---
date: 2021-02-22 16:31:50
description: App Development Antipatterns
pubDate: '2021-02-22T16:31:50'
title: App Development Antipatterns
tags:
- SAAS
heroImage: /images/app-antipatterns.jpg
---

### Having docker inside your app.

If you're somehow starting a docker container from your app, you're doing it wrong. What do I mean? Your app should not have anything about docker in it save a `Dockerfile`. I was at a place that had some NPM module that allowed you to start the app in docker with an NPM command. This is pretty crazy, keep those concerns separated.

## Having testing suit libraries in your app

I've been guilty of this, but it's time to bite the bullet and remove these test suites. If you're installing cypress / jest in your app, you're doing it wrong.

Why?

Makes it difficult to be flexible in your testing environments.

For instance, you cannot use Docker in Docker if trying to start docker from node.

What about code coverage on end to end tests... this is a red herring. When you're doing end to end testing, you're not testing the code, you're testing the interface.

## Testing for a particular environment

You may seen this many times in your app and never stopped to think, is this the right way?

```javascript
if (process.env.NODE_ENV === 'production') {
  // Do something
}
```

Why is this wrong? Now your app has built in logic as to what it should do in particular environments. Environments are a touchy subject. I've worked in far too many places that have several different types of environments. Production, Staging, Demo, QA, Dev; you name it, companies will always try to invent new environments to server various purposes.

Hey, you're app shouldn't test for the environment it's running in. And clue your app in with `APP_ENV` or even `NODE_ENV` doesn't help. Just pass in environment variables and your app should always act like it's in production (don't let it know that's it's only QA, it will hurt your app's feelings).
