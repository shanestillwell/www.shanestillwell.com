---
date: '2019-02-22T10:08:31-05:00'
description: Essential Skills Every Kickass Developer Should Possess
pubDate: '2019-02-22T10:08:31-05:00'
tags:
- JavaScript
title: Essential Skills Every Kickass Developer Should Possess
heroImage: '/images/essential-skills-of-a-kick-ass-developer-thumbnail.png'
---

That's right. You want to take your skills from beginner, bypassing intermediate, and land on advanced. You may have just started coding a year or two ago, but you're determined to be the best developer on your team. It's going to take hard work, no getting around that, but we like hard work. You're going to feel dumb at times, no problem, you've fully embraced that feeling dumb means you're learning and that's the name of the game. Say 'good-bye' to your comfort zone, and hello to boot camp life.


The following advice has served me well in the past 20 years of full time development, through many languages, platforms, environments, and goals. It will work for you. How do I know this? I've spoken to other 10x developers that agree, these skills will set you apart from your peers and give you the edge you need to get the new job, land the promotion, finish the project on time, and reduce technical debt. Hell, it'll even make your teeth whiter.

So let's begin.

## Put the GUI away.

> There may be small exceptions, but by an large, use the command line as your main tool.
  
You heard me, the first thing you need to do is throw away that pacifier called a GUI (Graphical User Interface) and learn you some commands on the command line. I'm not suggesting you only use the terminal. I use a Macbook Pro, so I'm using a browser for surfing the web, iTunes listening to music, Mail.app for sending email. However, most of my time developing is in a terminal. I can do everything I need to right in there. You can too. It's time you learned [BASH](https://guide.bash.academy/). You're not required to become a BASH guru, but you

* MUST know how to move around the command line (`cd`, `ls`, `mkdir`, `rm`, `touch`). You 
* MUST know how to change permissions on files (`chmod` , `chown`)
* MUST know how to write and run BASH scripts

Get a good terminal. On a Mac, the built in Terminal program is passable, but [iTerm2](https://www.iterm2.com/) is going to be your bread and butter. On a Windows, who knows. I'm not sure, it doesn't even support BASH, sorry, just the truth here.

## Learn Git

> On the command line of course

You don't need a Git client. On every Mac / Linux, `git` comes standard. [Learn](https://try.github.io/) how to use the command line with git

* `git checkout -b new-branch`
* `git add .`
* `git commit -m 'my cool message'`
* `git push origin head`
* `git fetch origin`
* `git reset --hard origin/master`

Do you know what each of those commands do? You should become familiar with git such that you don't wind up in a **git-tastrophe** of code.

## Regular Expressions: Magic you need

> Some people, when confronted with a problem, think "I know, I'll use regular expressions." Now they have two problems.  
> -- Jamie Zawinski

Maybe you've needed to parse a string of text to pull out or match on some variable. Searching StackOverflow turned up a regular express you think would solve your problems. How many people blindly copy / paste the solution into their app and have no clue how it actually works?

This is not you. You will learn how to make kick ass regular expressions. Most 10x devs are not level 99 experts, and you don't need to be either, but you must understanding how they work, and how you can leverage them. All databases, coding languages, and scripting tools support Regular Expressions.

Go over to https://regex101.com/ to start creating and learning reg exp today.

## Networking: not the business card kind

> The internet brought the computer to life.   
> --- Shane Stillwell

You're reading this blog post because of basic networking. Do you understand how it all works? Your computer (or hand held computer we call phones) made a request to find the Internet Address (IP) for **www.shanestillwell.com**. Some self important DNS server who knows everyone said I could be reached at **99.84.254.106** ([Amazon Cloud Front](/using-free-ssl-and-cloudfront-for-an-angular-react-site/)).

Your computer said, "Great!, I'll contact him directly". Your computer made an HTTP *request* to the server at **99.84.254.106** requesting the page **/2019/02/22/essential-skills-of-a-kick-ass-developer/**. In that request it sent some *headers* like "Here are the types I accept", "Here are some features I accept".

The server at **99.84.254.106** responded with the HTML content of this page and also sent along some *headers* such as "Date content was created" or "What type of content" or "Content length".

As a web developer, you'll need to send some **requests** to a server sometime. Maybe REST, if you're lucky GraphQL, dear lord, pray it's NOT SOAP. You could use the capable [Postman](https://www.getpostman.com/) but we're on a mission to rely on the command line. So instead we're going to use [cURL](https://curl.haxx.se/). It's all you'll ever need.

There are a million different cURL examples online. When you need to do something, just Google "How to do X with curl". Have a look at https://linuxize.com/post/curl-command-examples/ to start learning.

## /rant

This was just a small rant. I'm sure there are other hallmarks of an exceptional developer, but these are just a few I decided to commit to disk.

Keep it real.
