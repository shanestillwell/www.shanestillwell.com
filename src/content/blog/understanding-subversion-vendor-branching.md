---
date: '2009-04-04T03:01:19-05:00'
description: Understanding Subversion Vendor Branching
id: 48
pubDate: '2009-04-04T03:01:19-05:00'
tags:
- SVN
- Programming
title: Understanding Subversion Vendor Branching
---

<div style="border: 2px solid #ccc; padding: 20px; font-size: larger;">

### Update

If you haven't switched to [Git](http://git-scm.com/) yet, you should. I can't think of one reason to use Subversion over Git, Git is better in almost every way.
</div>

Since I'm still a newbie at SVN, getting the hang of Vendor Branching has taken me a little longer than I'm willing to admit.

Here are some resources that I found helpful.

*   [http://minimalmedia.com/en/blog/dave/upgrading-drupal-core-with-vendor-branches-and-svnloaddirs-almost-perfect](http://minimalmedia.com/en/blog/dave/upgrading-drupal-core-with-vendor-branches-and-svnloaddirs-almost-perfect)
*   [http://svnbook.red-bean.com/en/1.1/ch07s05.html](http://svnbook.red-bean.com/en/1.1/ch07s05.html)
*   [http://imagexmedia.com/blog/2009/1/updating-drupal-using-svn-vendor-branches](http://imagexmedia.com/blog/2009/1/updating-drupal-using-svn-vendor-branches)

*   [http://gotdrupal.com/videos/drupal-svn-vendor-branches](http://gotdrupal.com/videos/drupal-svn-vendor-branches)

*   [http://betterexplained.com/articles/a-visual-guide-to-version-control/](http://betterexplained.com/articles/a-visual-guide-to-version-control/)The basic concept is this. "Subversion, listen up.&nbsp; I'm going to use svn_load_dirs.pl and your going to find the differences between the current (5.15) and the new version (5.16) and record those changes. Don't forget to tag those changes (-t) as 5.16 when your done." 

`svn_load_dirs.pl file:///Users/shane/Sites/svn/test/vendor/drupal current drupal-5.16 -t 5.16`

Then in your working copy "Subversion, look at me when I'm talking to you. I want you apply those changes I had you tag just a minute ago, to my working copy.&nbsp; Make sure you keep my changes or I'll have your head on a stick."

`svn merge file:///Users/shane/Sites/svn/test/vendor/drupal/5.15 file:///Users/shane/Sites/svn/test/vendor/drupal/current`

Lastly. "Subversion, yeah, I'm talking to you. Commit these changes to my working copy and don't give me any lip."

`svn committ -m "Upgrading from 5.15 to 5.16"`

That's how I understand things to be now.... 

<div class="zemanta-pixie">![](http://img.zemanta.com/pixy.gif?x-id=139fb944-3110-8a6f-9a63-c45695cb5032)</div>