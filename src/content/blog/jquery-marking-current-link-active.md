---
date: '2010-05-22T07:43:40-05:00'
description: jQuery - Marking Current Link Active
id: 109
pubDate: '2010-05-22T07:43:40-05:00'
tags:
- jQuery
- Programming
title: jQuery - Marking Current Link Active
---

Have you ever wanted to have a link show as "Active" when a users is on that link? If you have a navigation at the top of your site and you want to visually indicate that the visitor is on that link, then you would usually add and "active" class to that link and style it appropriately.

This little snippet of code will do that.

&lt;code&gt;
$(document).ready(function(){
var path = location.pathname.substring(1);
if ( path ) {
$('.header a[href$="' + path + '"]').addClass('active');
}
});

&lt;/code&gt;

`location.pathname.substring(1);` will return the url that you are on, subtract the hostname name.
`a[href$="' + path + '"]` looks for all anchor tags inside of the header block whose end match the path.

This is pure genius.

References:
http://docs.jquery.com/Tutorials:Auto-Selecting_Navigation