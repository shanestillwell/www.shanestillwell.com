---
date: '2011-05-23T17:05:39-05:00'
description: Understanding the Nginx map directive
id: 127
pubDate: '2011-05-23T17:05:39-05:00'
tags:
- Server Admin
title: Understanding the Nginx map directive
---

When switching to Nginx I needed to have a variable that signified if the site was in HTTP or HTTPS mode.  So I found this little bit of code.
`
    map $scheme $fastcgi_https { ## Detect when HTTPS is used
        default off;
        https on;
    }
`

This works great, but I really didn't understand it until now so let's take it line by line.

## Line 1

`
    map $scheme $fastcgi_https { ## Detect when HTTPS is used
`
This is where all the glory happens. `$scheme` is an internal variable within Nginx and is either HTTP or HTTPS (maybe some other ones in the future like SPDY).  Then you have `$fastcgi_https`, this is the name of the variable we're creating for future use in our config files. Nothing but comments after that stuff.

## Line 2 and 3

`
        default off;
        https on;
`
This is our map of values.  When Nginx's `$sheme` variable is equal to "HTTPS", then the `$fastcgi_https` variable will equal "on", otherwise the variable just equals "off".

Not too complex, but I just didn't pick up on the format until now.  Read more at
http://wiki.nginx.org/HttpMapModule