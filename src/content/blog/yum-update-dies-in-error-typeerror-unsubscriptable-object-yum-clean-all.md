---
date: '2009-06-26T15:08:24-05:00'
description: 'yum update dies in error ''TypeError: unsubscriptable object''... yum
  clean all'
id: 57
pubDate: '2009-06-26T15:08:24-05:00'
tags:
- Server Admin
title: 'yum update dies in error ''TypeError: unsubscriptable object''... yum clean
  all'
---

While updating one of my servers today I entered this command `yum update file` and received the following error message.

`
Traceback (most recent call last):
  File "/usr/bin/yum", line 29, in ?
    yummain.user_main(sys.argv[1:], exit_code=True)
  File "/usr/share/yum-cli/yummain.py", line 229, in user_main
    errcode = main(args)
  File "/usr/share/yum-cli/yummain.py", line 145, in main
    (result, resultmsgs) = base.buildTransaction() 
  File "/usr/lib/python2.4/site-packages/yum/__init__.py", line 647, in buildTransaction
    (rescode, restring) = self.resolveDeps()
  File "/usr/lib/python2.4/site-packages/yum/depsolve.py", line 704, in resolveDeps
    for po, dep in self._checkFileRequires():
  File "/usr/lib/python2.4/site-packages/yum/depsolve.py", line 939, in _checkFileRequires
    if not self.tsInfo.getOldProvides(filename) and not self.tsInfo.getNewProvides(filename):
  File "/usr/lib/python2.4/site-packages/yum/transactioninfo.py", line 414, in getNewProvides
    for pkg, hits in self.pkgSack.getProvides(name, flag, version).iteritems():
  File "/usr/lib/python2.4/site-packages/yum/packageSack.py", line 300, in getProvides
    return self._computeAggregateDictResult("getProvides", name, flags, version)
  File "/usr/lib/python2.4/site-packages/yum/packageSack.py", line 470, in _computeAggregateDictResult
    sackResult = apply(method, args)
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 861, in getProvides
    return self._search("provides", name, flags, version)
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 43, in newFunc
    return func(*args, **kwargs)
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 837, in _search
    for pkg in self.searchFiles(name, strict=True):
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 43, in newFunc
    return func(*args, **kwargs)
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 586, in searchFiles
    self._sql_pkgKey2po(rep, cur, pkgs)
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 470, in _sql_pkgKey2po
    pkg = self._packageByKey(repo, ob['pkgKey'])
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 413, in _packageByKey
    po = self.pc(repo, cur.fetchone())
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 68, in __init__
    self._read_db_obj(db_obj)
  File "/usr/lib/python2.4/site-packages/yum/sqlitesack.py", line 94, in _read_db_obj
    setattr(self, item, _share_data(db_obj[item]))
TypeError: unsubscriptable object
`

This happened every time until I cleaned the cache. 

`
yum clean all
`

Then it worked without a flaw.