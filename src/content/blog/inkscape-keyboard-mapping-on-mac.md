---
date: '2008-12-21T20:54:24-05:00'
description: Inkscape Keyboard Mapping on Mac
id: 41
pubDate: '2008-12-21T20:54:24-05:00'
tags:
- Apple
title: Inkscape Keyboard Mapping on Mac
---

I was having some problems with the ALT key on my Macbook in Inkscape. To adjust the text space and kerning in Inkscape you need to use the keyboard. This was not working, so I was stuck.  Luckily, as usually I was not alone. I found a way to map the keys to work correctly under X11 and furthermore it makes the CMD key act as the CTRL as well. Double bonus.

Create the file
~/.xmodmap
`
keycode 63 = Control_L
keycode 60 = Alt_L
keycode 66 = Alt_L
clear mod1
clear Control
add mod1 = Alt_L
add Control = Control_L
`

Reference.
http://wiki.inkscape.org/wiki/index.php/FAQ#How_to_make_Alt.2Bclick_and_Alt.2Bdrag_work_on_Mac_OS_X.3F