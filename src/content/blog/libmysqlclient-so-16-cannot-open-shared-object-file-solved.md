---
date: '2005-11-30T00:00:00-05:00'
description: 'libmysqlclient.so.16: cannot open shared object file [solved]'
id: 117
pubDate: '2005-11-30T00:00:00-05:00'
tags:
- Server Admin
title: 'libmysqlclient.so.16: cannot open shared object file [solved]'
---

On a fresh install of Zend Server Community Edition on Centos 5, I tried to run a Zend Framework command line script and it errored out on me complaining it could not find the Mysql drive 

## The Problem

```
Warning: include_once(Doctrine/Adapter/Mysql.php): failed to open stream: No such file or directory in /usr/local/zend/share/ZendFramework/library/Zend/Loader.php on line 146

Warning: include_once(): Failed opening 'Doctrine/Adapter/Mysql.php' for inclusion (include_path='/var/www/website/website/application/../library:/var/www/website/website/library:.:/usr/local/zend/share/ZendFramework/library:/usr/local/zend/share/pear') in /usr/local/zend/share/ZendFramework/library/Zend/Loader.php on line 146
```

The Zend Server Log files has the following included.

```
[21-Sep-2010 15:43:25] PHP Warning:  PHP Startup: Unable to load dynamic library '/usr/local/zend/lib/php_extensions/mcrypt.so' - libmcrypt.so.4: cannot open shared object file: No such file or directory in Unknown on line 0
[21-Sep-2010 15:43:25] PHP Warning:  PHP Startup: Unable to load dynamic library '/usr/local/zend/lib/php_extensions/mysql.so' - libmysqlclient.so.16: cannot open shared object file: No such file or directory in Unknown on line 0
[21-Sep-2010 15:43:25] PHP Warning:  PHP Startup: Unable to load dynamic library '/usr/local/zend/lib/php_extensions/mysqli.so' - libmysqlclient.so.16: cannot open shared object file: No such file or directory in Unknown on line 0
[21-Sep-2010 15:43:25] PHP Warning:  PHP Startup: Unable to load dynamic library '/usr/local/zend/lib/php_extensions/pdo_mysql.so' - libmysqlclient.so.16: cannot open shared object file: No such file or directory in Unknown on line 0
[21-Sep-2010 15:43:25] PHP Warning:  PHP Startup: Unable to load dynamic library '/usr/local/zend/lib/php_extensions/pdo_pgsql.so' - libpq.so.5: cannot open shared object file: No such file or directory in Unknown on line 0
```

## The Solution

All I needed to do was run `ldconfig`. This made the Dynamic Linker Run Time Bindings reread the `/etc/ld.so.conf.d` directory for new library directories.