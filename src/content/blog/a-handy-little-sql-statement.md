---
date: '2009-12-31T11:22:40-05:00'
description: A handy little SQL statement
id: 73
pubDate: '2009-12-31T11:22:40-05:00'
tags:
- SQL
- Programming
title: A handy little SQL statement
---

Have a look at the following SQL

<sql>
SET @salt=SUBSTRING(MD5(RAND()),-20);
</sql>
This will set a variable called `@salt` that has a random string of 20 characters.