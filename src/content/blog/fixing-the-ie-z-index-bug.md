---
date: '2010-05-13T22:18:50-05:00'
description: Fixing the IE z-index bug.
id: 107
pubDate: '2010-05-13T22:18:50-05:00'
tags:
- CSS
- Design
title: Fixing the IE z-index bug.
---

We all know that IE is full of bugs and just plain sucks as a browser... if you didn't know that you can drop your geek badge off on the way out. So today I ran across the Z-Index bug in IE7.

Sometimes you've seen this behavior before:
![Screen shot 2010-05-13 at 12.15.21 PM.png](/images/Screen shot 2010-05-13 at 12_15_21 PM.png)

The fix is to give the parent of the element with a z-index a slightly higher z-index

```html
<div style="z-index: 1000;">
<div style="z-index: 999;">Content Here</div>
</div>
```

Resources:
[http://brenelz.com/blog/squish-the-internet-explorer-z-index-bug/](https://web.archive.org/web/20191106134348/http://brenelz.com/posts/squash-your-zindex-bugs/)
