---
date: '2013-10-08T16:00:08-05:00'
description: They shoot email, don't they?
pubDate: '2013-10-08T16:00:08-05:00'
tags:
- Rant
- Good Ideas
title: They shoot email, don't they?
---

So the year is 2013, you send an email off to two coworkers and cc four other people in the company. Think for a minute the utter black hole of despair you just created for six people. Six poor souls now have to read your email and file it away. Let's take a look at what you've set in motion.

1. **Disruption**  
Undoubtedly six of these targets will get a notification on their screen that there is a new email from you. Some may tremble in fear of what it may say, others may be delighted, but _all_ of them commence to taking action. Think of the productivity you just killed with just one click of the `Send` button. Now they will all drop what they were so successfully accomplishing to view your email. 

2. **Doubt**  
Should they respond? Do they need to read it immediately? Wait? Is this to me, or am I just CC'd? If I do respond, should I `Respond All` or just to the original sender. Hold on, Jeff really needs to know this, should I forward it to him or reply all and include him in this mess. Way to go, six people now have to run through all that, you're getting really good at killing productivity.

3. **Despair**  
Just think, six copies of your email are sitting inside coworker's inboxes right now. That's a warm and fuzzy feeling isn't. At your bidding, you just created work for six people all at once. Those sore six now have to process your email, after they deal with all the other emails lurking in their inbox. You know, I'll bet just by going to their inbox, they'll see other emails vying for their attention that will likely succeed. 

4. **Disconnect**  
Joe responds just to with the answer you're looking for, but Jane responds to all with a different answer. Two hours later, Larry finally opens his email and starts to process them one by one. He gets to yours and responds. As Larry goes through his email, he notices that there has already been responses to your email and then starts replying to those replies. Of course the subject will go way off topic. 

> Any email with more than 10 replies is no longer about the original subject

## STOP, PLEASE STOP!!

If you want tell me how to get around those issues above, **YOU** are stuck in the past trying to make email become something it was never designed to be. It's a brave new world, maybe you didn't realize when or where it changed, but today we have better forms company communication.


## A New and Better Way

### Conversation v Information  

Start thinking about how conversation is different than information. Conversation has a lot of back and forth and will contain or lead to information. Most times you don't want to safe a conversation, but you want to save the information as a result of that conversation. Information is data that you want to save, or that will come in handy later on in life (numbers, dates, URLs, action items, etc).


* **Instant Message for Conversation**  
You should use [Skype](http://www.skype.com) or some equivalent (gTalk, HipChat, Campfire). This will provide instant communication between many different people. You can communicate to people one-on-one or in a chat room. 

* **Collaboration Online for Information**  
For the data that you want to save for future use, you would use some online solution to keep and organize this data. [Trello](https://trello.com) is great for teams that want to organize lists of information into any workflow they desire. Others include, Basecamp, Jira, Sprint.ly. Shoot, you could even use just Google Docs or even Evernote. In any case, it needs to be something that allows everyone to see the changes being made and contribute.

## In Summary

Email is not going away anytime soon, and nothing beats email for sending quick informational bits to people you don't normally interact with on a daily basis. Having said that, email within a company should be minimal at most. Go forth and STOP the madness.
