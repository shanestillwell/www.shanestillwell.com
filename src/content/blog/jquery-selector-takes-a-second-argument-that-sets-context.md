---
date: '2010-03-21T06:12:09-05:00'
description: jQuery selector takes a second argument that sets context.
id: 99
pubDate: '2010-03-21T06:12:09-05:00'
tags:
- jQuery
- Programming
title: jQuery selector takes a second argument that sets context.
---

I was trying to concatenate a select to the almighty <pre>this</pre> in jQuery, but it just wasn't working.

I tried
`
$(this + 'input')
`
Didn't work, so I thought, maybe I need a space between this and input like so:
`
$(this + ' input')
`
No go..

Then I ran across this page.
http://stackoverflow.com/questions/306583/jquery-this-selector-and-children

Of course StackOverFlow has the answer.

The working result

`
$('input', this)
`

Perfect!!!