---
date: '2011-08-17T01:56:47-05:00'
description: The continuing Saga of Lion
id: 131
pubDate: '2011-08-17T01:56:47-05:00'
tags:
- Apple
title: The continuing Saga of Lion
---

Should you upgrade to OS X Lion???   Meh.... it's presented more problems than previous upgrades.

Maybe I should have waited until 10.7.1, so this is my punishment for being bleeding edge.

Another issue I saw was the continual nag when I opened some apps (Billings and Cyberduck) that "This program was downloaded from the Internet... blah blah blah" This normally happens once for a program then goes away after you say "OK", but this was happening every time I opened those two programs.

The solution.

`
sudo xattr -d com.apple.quarantine /Applications/Billings.app
`

This shut the bugger up.

Resources
https://discussions.apple.com/message/6930135#6930135