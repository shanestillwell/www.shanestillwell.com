---
date: '2009-03-24T11:34:04-05:00'
description: Throttle traffic with iptables
id: 42
pubDate: '2009-03-24T11:34:04-05:00'
tags:
- Server Admin
title: Throttle traffic with iptables
---

I was having a problem with a particular virtual private server getting overrun by a surge in traffic (targeted?).  So I decided to throttle the HTTP connections using iptables.

`
/sbin/iptables -A INPUT -i eth0 -p tcp --dport 80 -m state --state NEW -m recent --set --name HTTP; 
/sbin/iptables -A INPUT -i eth0 -p tcp --dport 80 -m state --state NEW -m recent --update --seconds 60 --hitcount 100 --rttl --name HTTP -j DROP
`

The first line just tags the new HTTP connections as HTTP. The second line gives the hitcount of 100\.  So if I get more than 100 HTTP requests a minute, then iptables will just drop the 101+ until the minute is over.  

This has worked splendid on the site.