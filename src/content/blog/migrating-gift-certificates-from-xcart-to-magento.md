---
date: '2009-04-09T23:25:31-05:00'
description: Migrating Gift Certificates from Xcart to Magento
id: 49
pubDate: '2009-04-09T23:25:31-05:00'
tags:
- Programming
title: Migrating Gift Certificates from Xcart to Magento
---

You're migrating to Magento from Xcart and want to offer gift certificates (GC) on Magento and you want to take all the active gift certificates over to Magento as well.

We'll be using the wonderful GC extension at 
[http://www.magentocommerce.com/extension/reviews/module/751/gift-certificates--virtual-cards](http://www.magentocommerce.com/extension/reviews/module/751/gift-certificates--virtual-cards)

1\. Dump the gift cert data from Xcart to a csv file.

`
SELECT gcid as cert_number,debit as balance,'USD' as currency_code,'' as pin, '' as pin_hash,'A' as status, NULL as expire_at, recipient as recipient_name,recipient_email, CONCAT(recipient_address,'\n', recipient_city,', ',recipient_state,' ',recipient_zipcode) as recipient_address, message as recipient_message, 0 as toself_print FROM `xcart_giftcerts` WHERE status = 'A';
`

You can use PHPMySQLadmin to dump that or you can save that query to a text file (gcert.sql) and run it on the command line

`mysql -u [YourUserName] -p [YourDBName] &lt; gcert.sql &gt; datadump.csv
`

2\. Import the data into the Magento table ugiftcert_cert

3\. Then you need to run this SQL to make them show up in the backend.

`
insert into ugiftcert_history (cert_id, action_code, ts, amount, currency_code, status) SELECT cert_id,'create',now(),balance,currency_code,`status` FROM ugiftcert_cert
`

Summary
You need to dump the data from Xcart and use some MySQL magic to format the fields to import into Magento.&nbsp; The Gift Certificate extension tracks transactions for each GC and so the need for step 3 to create the initial history of the GC.

<div class="zemanta-pixie">![](http://img.zemanta.com/pixy.gif?x-id=32096f9e-6fa6-8f4f-a3e2-91868276e8b8)</div>