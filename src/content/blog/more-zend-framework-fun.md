---
date: '2010-01-11T04:55:09-05:00'
description: More Zend Framework Fun
id: 79
pubDate: '2010-01-11T04:55:09-05:00'
tags: []
title: More Zend Framework Fun
---

## Sending email via Gmail

<php>
  $config = array(
      'ssl' => 'tls',
      'port' => 587,
      'auth' => 'login',
      'username' => 'youemail@gmail.com',
      'password' => 'yourpassword'
  );

  $gmail = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
  $mail = new Zend_Mail();
  $mail->addTo($this->to);
  $mail->setSubject($this->subject);
  $mail->setFrom($this->from);
  $mail->setBodyText($this->body);

  $mail->send($gmail);
</php>  

As you can see, you need to create an object for Gmail, then create an email object, then tell the email object to use the gmail object. It works great.

* * *
