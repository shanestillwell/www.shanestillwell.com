---
date: '2010-01-03T01:56:13-05:00'
description: Zend Framework Resources
id: 74
pubDate: '2010-01-03T01:56:13-05:00'
tags:
- Programming
title: Zend Framework Resources
---

I'll try to bring some of the recent lessons learned in Zend Framework here on the blog. The deeper I dig, I realize the framework is very well put together and powerful.

<ul>
<li>[The official Zend Framework Manual](http://framework.zend.com/manual/en/ "Zend Framework: Documentation")
 True to PHP, Zend has a great manual with many examples, though, if you are looking for every explicit directions where to place code you may be disappointed. Since Zend is a very loosely coupled framework, they don't always assume you're using it in the Zend Application setting.</li>
<li>[Alexander Romanenko's Tech Adventures](http://alex-tech-adventures.com/ "Welcome to Alex Tech Adventures")
A self proclaimer beginner, Alex has several screencasts that layout the foundations of getting started with Zend Framework. It's a wonderful resource for those looking to get a visual start on ZF.</li>

more to come...