---
date: '2012-05-15T14:29:42-05:00'
description: Problems using VirtualBox to access sites on the net
id: 328
pubDate: '2012-05-15T14:29:42-05:00'
tags:
- Apple
- Server Admin
title: Problems using VirtualBox to access sites on the net
---

### Background

I've set up my VirtualBox to access my localhost (OSX) by following instructions similar to [this post](http://sandhillcreative.com/kb/2009/11/19/accessing-your-local-mamp-dev-environment-from-virtualbox/comment-page-1/#comment-194), using NAT. Everything was great until I started testing the staging site on the net (not my localhost). Most things worked fine, but uploading larger images would just fail in IE. Uploads worked fine on the localhost, but failed on stage, so I initially thought it was a configuration on the staging server webserver.

&nbsp;

![Open VirtualBox](/images/problems-using-virtualbox-to-access-sites-on-the-net-Screen-Shot-2012-05-15-at-8.59.58-AM.png "Screen Shot 2012-05-15 at 8.59.58 AM")

### Solution

The problem was the network adapter for VirtualBox. I was using NAT as the network adapter setting. This was the cause of the issue.

&nbsp;

My VirtualBox was not receiving the** ICMP Fragmentation Required** bit from the server. That is why larger images would hang, but smaller images would go through fine.

Now the solution is to change the network adapter back and forth between NAT and Bridged when I notice funkiness on remote sites.

### 

### References

http://sandhillcreative.com/kb/2009/11/19/accessing-your-local-mamp-dev-environment-from-virtualbox/comment-page-1/#comment-194
