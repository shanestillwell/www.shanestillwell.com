---
date: 2016-10-18 22:06:48
description: Some Features I Like About GitLab
pubDate: '2016-10-18T22:06:48'
tags:
- Git
- GitLab
title: Some Features I Like About GitLab
heroImage: /images/some-features-i-like-about-gitlab.png
---

## What is GitLab
> An alternative to Github with many more features and an increasingly better offering


## Free Private Git Repositories
On of Github's price points is to offer you a limited number of private repositories. You want more, pay more. Then Bitbucket came along and offered free private repositories, but limited the number of people you could share those repositories, it made for some weird accounting at the end of the day. GitLab however has changed such things. First it allows an unlimited number of private repositories (🎉)

## Free Private Docker Image Repository
That's right, FREE. PRIVATE. DOCKER. REPO. Here again, you can go to Docker Hub and you get one free private repo. At GitLab you get an image repository per project and it's not that hard to set up at all. I toyed around with Amazon's Docker Image Repository, but it was a huge pain.

To get started with GitLab Image Repository, you can set it up like

```
> docker login registry.gitlab.com;
> docker build -t registry.gitlab.com/USERNAME/PROJECT .
> docker push registry.gitlab.com/USERNAME/PROJECT
```

## Free Test Runners
I will have to admit, figuring out how to configure these was a little bit a of a challenge, but once I got it rolling it's nice for it to automatically run my tests. Get this, I can have it automatically run my contract tests. It'll setup a test Mongo database for me, `npm install`, then `npm test` and *BANG*! Tests are run and it will email the results. I just have to figure out how to automatically deploy, for now I have a script that does that for me.

## Conclusion
GitLab is growing by leaps and bounds. The interface could use a little TLC since most its a little cluttered in some areas, but overall, it's a great product. From what I can tell, they have a model that fits almost any situation. Free online, paid online, host your own community, host your own enterprise. It looks like they have your covered.
