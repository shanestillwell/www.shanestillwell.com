---
date: 2021-08-02 23:25:01
description: RSS is the future
pubDate: '2021-08-02T23:25:01'
tags:
- rant
title: RSS is the future
heroImage: '/images/rss-for-the-win-thumbnail.png'
---

RSS is the one weird trick no one really knows about, but pretty much every site supports. It's been around for a long time and started to catch on about 15 years about. Then Twitter came out and RSS lost its momentum. Over the years, Twitter eventually became a cesspool and no longer became a reliable way to consume news, so RSS has seen a renaissance in recent years.

RSS which stands for Really Simple Syndication, is nothing more than subscribing to website updates. Just like podcasts will let you know when there is a new episode out, RSS will let you know when there are new posts to a site.

## Clients I use (macOS & iOS)

Unless you are comfortable reading XML, you need an RSS client to view feeds. When using a client, there are two modes. Stand alone: the RSS client keeps track of your subscriptions and read history. Sync Mode: The client syncs with service so you can view your feeds on different devices and keep track of subscriptions and read history.

* [Reeder](https://apps.apple.com/us/app/reeder-5/id1529448980?mt=12) (macOS)

    A solid RSS reader for your Mac laptop / desktop. I like that it supports the keys `j` and `k` for navigating through articles (Vim is awesome). The newer version supports *mark read on scroll* which I find to be the most important feature of all if you consume a lot of feeds.
    ![Reeder](/images/rss-for-the-win-reeder.png "macOS Reeder")

* [RSS Bot](https://apps.apple.com/us/app/rss-bot-news-notifier/id605732865?mt=12) (macOS)

    A menu bar resident. This isn't a reader, but will alert you when a new post to a subscription you follow is up. I use this to follow status pages of the services I use like [Heroku](), [GitLab](), and [Netlify](). All of their status pages offer RSS feeds.

* [Unread](https://www.goldenhillsoftware.com/unread/) (iPadOS / iOS)

    My favorite iPhone RSS reader. The killer feature is of course the *mark read on scroll*. This means as I scroll past articles it marks them as read when they go out of view. It also does a great job of rendering the article.

    ![Unread](/images/rss-for-the-win-unread.png "Unread")


## Services 

> You do not need to use a service like Feedster or Feedbin, but syncing your feeds across devices and features they offer are invaluable.

I use [Feedbin](https://feedbin.com/) and love the features they offer to turn almost anything into an RSS feed. It costs about $50 / year which I deem to be more than worth it. It helps me consume the interwebs how I want and that's worth more than $50 a year.

* A dedicated email address for email **newsletters**. Yeah, this is their killer feature. They give you an email you can use to sign up for newsletters and each email shows up in your feed. They even automatically "confirm" your subscription to the newsletter with some advanced AI. I use this all over the place for newsletters that do not otherwise support an RSS feed.
    ![Newsletters](/images/rss-for-the-win-newsletters.png "Newsletters")

* **Bookmark** a page that will show up in your RSS feed. Yeah, pretty much a read it later, but without having to have an account on Pocket or Instapaper.
    ![Bookmark](/images/rss-for-the-win-bookmark.png "Bookmark")

* **Image Proxy**
  > Images are another potential source of leaking data. Feedbin has used an image proxy since launch to prevent mixed content warnings. A side benefit of the image proxy, is that your browser only makes requests to the proxy and the proxy gets the image data, preventing your request from reaching the origin.

  Another great win for security and privacy. Since your RSS client only hits Feedbin's image proxy, you cannot be tracked by third parties. Read more about their [Privacy by default](https://feedbin.com/blog/2018/09/11/private-by-default/).


## Serendipitous Benefits

* **Slip past the paywall**. I subscribe to the RSS feed of my local small town newspaper. The paper of course has a [paywall](https://en.wikipedia.org/wiki/Paywall). However, they don't realize that they leak the whole article via RSS. So in my reader I can see the whole article (images and all). If I click to open in a browser, I'm greeted with a paywall. Shhhh, don't tell them, this is an oversight I'm sure.

## Thoughts

* **Tags**: I don't really use them. YMMV.
* Each subscription will ebb and flow. You might like a feed at first, but you'll find yourself losing interest in that feed. You can either mute that feed for a while or unsubscribe. Prune the noise and keep the signal (don't be afraid to drop the feeds that no longer [spark joy](https://konmari.com/marie-kondo-rules-of-tidying-sparks-joy/))
* I love RSS, if you're looking for a reliable way to keep up on news, articles, and information, then you should give it a go.
