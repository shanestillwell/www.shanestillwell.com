---
title: Stop Using Make for Golang
description: Stop Using Make for Golang
pubDate: 2025-02-03T23:24:31.931Z
tags: ['Go']
heroImage: /images/gopher-stay-puft.png
---

> GNU Make is a tool which controls the generation of executables and other non-source files of a program from the program's source files.

If you think you need to use `make` in your Go project, odds are, you're just going to be using it as a glorified task runner. Go already has the tools to detect changes in your go code and build binaries accordingly e.g `go build ./cmd/...`. 

Instead, just use [Just](https://just.systems/man/en/). Just is a task runner (written in Rust) and has some resemblance to make. Some things you'll love about Just

## You can run tasks (recipes) in any language

https://github.com/casey/just?tab=readme-ov-file#shebang-recipes

```
polyglot: python js perl sh ruby nu

python:
  #!/usr/bin/env python3
  print('Hello from python!')

js:
  #!/usr/bin/env node
  console.log('Greetings from JavaScript!')

perl:
  #!/usr/bin/env perl
  print "Larry Wall says Hi!\n";

sh:
  #!/usr/bin/env sh
  hello='Yo'
  echo "$hello from a shell script!"

nu:
  #!/usr/bin/env nu
  let hello = 'Hola'
  echo $"($hello) from a nushell script!"

ruby:
  #!/usr/bin/env ruby
  puts "Hello from ruby!"
```



## Automatically load `.env` files

```
set dotenv-load

serve:
  @echo "Starting server with database $DATABASE_ADDRESS on port $SERVER_PORT…"
  ./server --database $DATABASE_ADDRESS --port $SERVER_PORT
```

## Command line arguments with optional defaults

```
build target="prod":
  @echo 'Building {{target}}…'
  cd {{target}} && make
```

## List all recipes 

```
$ just --list
Available recipes:
    build
    test
    deploy
    lint
```


> Stop using a PHONY task runner for Go and Just use Just

Another up and coming option is [Mise](https://mise.jdx.dev/lang/go.html)
