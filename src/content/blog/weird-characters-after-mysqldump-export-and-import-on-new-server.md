---
date: '2009-10-13T19:10:09-05:00'
description: Weird characters after mysqldump export and import on new server
id: 67
pubDate: '2009-10-13T19:10:09-05:00'
tags:
- MySQL
- Programming
title: Weird characters after mysqldump export and import on new server
---

The other day I had to migrate a WordPress database from one server to the other.  I used `mysqldump` to export the SQL and data from the old server.

Then I used this to import
`
mysql -uusername -ppassword database < backup.sql
`

This went fine, but I found I was seeing weird characters in the front end and through phpmysqladmin.  The "Collation" on both tables seemed the same.

Then I stumbled on a website that offered this change when importing

`
mysql -uusername -ppassword --default-character-set=utf8 database < backup.sql
`

That fixed my troubles... now on to more important things.