---
date: '2008-12-04T12:34:42-05:00'
description: Magento built on Zend Framework
id: 39
pubDate: '2008-12-04T12:34:42-05:00'
tags:
- Programming
title: Magento built on Zend Framework
---

Deeper and deeper into the Magento core I've plunged. It's built on the [Zend Framework](http://framework.zend.com/). I've also been getting acquainted with Zend, with it you can quickly write powerful web applications that can do just whatever you can dream up. Many Google services are built right in.