---
date: 2021-08-18 12:23:31
description: The :key to rerendering in Vue.js
pubDate: '2021-08-18T12:23:31'
tags:
- Vue.js
- JavaScript
title: The :key to rerendering in Vue.js
heroImage: '/images/the-key-to-rerender-in-vuejs-thumbnail.jpg'
---

Every once in a while a technology comes along and fits like a glove. It solves problems in elegant ways and is immediately approachable from day one. One such technology is [Vue.js](https://vuejs.org/). You have to understand, I've used many JavaScript frameworks. You can even say I [wrote the book on advanced JavaScript](https://www.packtpub.com/product/mastering-mean-web-development-expert-full-stack-javascript-video/9781785882159 "Mastering MEAN Web Development").

I started with [Angular](https://angularjs.org/) back in the olden days of 0.8 and loved it, but like many, was wooed away to [React](https://reactjs.org/) while Angular tried to do a full rewrite for their 2.0 version (something you [should never do](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/)). I then wrote React for many years and for the most part enjoyed its unconventional ways.

Then I heard of Vue.js and decided to write a little app in Vue. It was amazing, in about a day I created a cool little scheduling app. Vue has wonderful [docs](https://vuejs.org/v2/guide/), simple to read and understand. I clearly remember coming across an issue *"how do I render two table rows in each iteration of a for loop?"* The answer was to use a `template` tag as the outer component for the loop. It's not so much the solution, but merly, everytime I needed to do something special, Vue had already thought of it and made it convenient to implement. Another nice touch is the [`submit.prevent`](https://vuejs.org/v2/guide/events.html#Event-Modifiers) event modifier.

## Once Again, Vue proves its worth.

The problem?

> I want to force a form to reset after it's been used.


In my case, I had a form for adding a reservation in [CabinKey](https://www.cabinkeyapp.com)®. The resort owner would add a guest using the form, fill out the reservation. The next time they went to add a new reservation, the old values would still be in the form. The reusable form was a component that I called with a `v-model` so it would update some local input object I can use to submit the values.

```html
  <NewGuestForm
    v-model="input"
  ></NewGuestForm>
```

Since Vue is efficient in rendering, when the resort owner moves away from this page, instead of destroying the rendered element, it simply hides until needed again. This is key, since it doesn't destroy the element, it doesn't receive new props or data. I tried a few things, including setting [`watchers`](https://vuejs.org/v2/guide/computed.html#Watchers) in the form, but this became unwieldy.

> When the solution becomes cumbersome, you're heading in the wrong direction.

Then I read **Michael Thiessen's** solution linked below. It dawned on me that the `:key` attribute wasn't just used by the `v-for` loop to give each looped element a distinct id, but any element in Vue could have a `:key` attribute. To make Vue rerender the element, you change the `:key`.

With that new knowledge, all I needed to do was set the `:key` to a new value after it was submitted. The new code looks like this.

```vuejs
<template>
<NewGuestForm
  <!-- input.id is reset below after successful submission -->
  :key="input.id"
  v-model="input"
></NewGuestForm>
</template>
<script>
exports default {
  methods: {
    addReservation ({ id: personId } = {}) {
      this.$apollo.mutate({
        ...
      })
        .then(({ data }) => {
          // set the input to have new data that includes a new `id`. Date.now prints out the epoch in milliseconds
          this.input = { ...DEFAULT_INPUT, id: Date.now() }
        })
    },
  }
}
</script>
```

![party time](https://i.pinimg.com/originals/8d/b8/28/8db828b17a0361a7054fb6f0db151698.gif)

Now that I know about it, I can see straight from the [docs on :key](https://vuejs.org/v2/api/#key).

> The key special attribute is primarily used as a hint for Vue’s virtual DOM algorithm

Maybe I should go back and read the Vue.js docs more thoroughly, what else have I missed? Oh, yeah, `$options`, just figured that one out too. I guess you don't need to put items in `data()` to be accessible to your template. 🙃

## References
* https://michaelnthiessen.com/force-re-render/
* https://vuejs.org/v2/api/#key
* https://itnext.io/how-not-to-vue-18f16fe620b5

Photo by [nextbike](https://unsplash.com/@nextbike) on [Unsplash](https://unsplash.com/s/photos/woman-bicycle)
