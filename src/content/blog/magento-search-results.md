---
date: '2009-07-02T02:00:07-05:00'
description: Magento Search Results
id: 60
pubDate: '2009-07-02T02:00:07-05:00'
tags:
- Programming
title: Magento Search Results
---

Search seems so simple on the surface, but inside it's a complex thing (just ask Microsoft why they still haven't beaten Google at this game).  Magento offerers some search options for products, but it's search is not 'awesome', but just OK.

Here are some options I've found that return a small set of products that are most accurate.

System > Config > Catalog > Catalog Search
1\. Search Type = Combine (Like and Fulltext)
- This will catch plurals pretty good as well as slight spelling variants

2\. Disable quicksearch for 'Descriptions'
- This is importing in cutting down the cruft in search results.

Wish List for Magento Search.

*   Suggested Products.  When someone searches for 'screwdriver' I want to suggest a certain specific product at the top of the search results.
*   Better Synonym support
*   Spelling suggestions
*   Category Search.  If someone searches for 'boots' I want to display categories whose names match that search, e.g. 'Women's boots' 'Men's boots'.