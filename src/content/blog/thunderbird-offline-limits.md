---
date: '2008-10-27T16:18:43-05:00'
description: Thunderbird Offline Limits
id: 37
pubDate: '2008-10-27T16:18:43-05:00'
tags:
- Apple
title: Thunderbird Offline Limits
---

I'm a long time user of Thunderbird and recommend it over most email programs for email usage. I've found problems with the way Thunderbird handles offline email. I have a client that has 50,000 emails in his Archive folder. The offline behavior of Thunderbird for this client is very flaky. 

## A little background.

There are different ways to store emails in files. One way called mbox stores all emails of a folder in one file. So essentially each folder is a file, that file has all the emails in that folder. It uses tricks to differentiate one email from another and uses index files to list them quickly. This is the storage method that Thunderbird uses.

Another method is a separate file for each email, in folders just like you see in your emails. This still has to be indexed for fast display of the email headers. It also takes a little longer to read in hundreds of files, than one big file. This way is commonly called MailDirs. This is a more common method used with servers such as Cyrus IMAP, Dovecot, and Courier.

## Where Thunderbird breaks.

Thunderbird works fine for offline under normal conditions, but add 50,000 emails to a single folder and  you can be stuck pretty hard. It sometimes works, then something changes and it gives this error message.
<quote>
The body of this message has not been downloaded from the server for reading offline. To read this message, you must reconnect to the network, choose Offline from the File menu and then select Work Online.In the future, you can select which messages or folders to read offline. To do this, choose Offline from the file menu and then select Synchronize. You can adjust the Disk Space preference to prevent the downloading of large messages.
</quote>

## The Solution.

Mail.app uses the MailDir storage solution, which can scale to 100K+ of emails. Mail.app has been getting better IMAP support with each new release so there really isn't anything holding me back. The developers of Thunderbird should change the store method so it can scale better. Having an mbox file of 5gigs that has to be read into memory can really break things in an ugly manner. Besides that, other programs have a harder time trying to search Thunderbird's file and map it to an email address, Mail.app can be searched via Mac Spotlight and bring a specific email with that desktop search.

## What Should You Do?

I have 10,000 emails in my Archive folder and Thunderbird Offline handles this with ease. Unless you plan on having 30,000+ emails in a folder, I would stick with Thunderbird.