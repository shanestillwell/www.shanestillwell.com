---
date: '2008-02-20T18:42:54-05:00'
description: Keeping SSH open through firewall while idle
id: 17
pubDate: '2008-02-20T18:42:54-05:00'
tags:
- SSH
- Server Admin
title: Keeping SSH open through firewall while idle
---

We recently installed a Cisco ASA 5505 firewall where I work.  Very powerful firewall, much too powerful for our needs, but oh well.  The problem is that SSH connections from the inside to outside would timeout after 5 minutes.  I think this was a problem with the NAT timeout settings, but I tried many, many different options with no success on the firewall. Then I discovered `ClientAliveInterval` for the OpenSSH server.  You put that in your sshd_config file and restart SSH.  It should work like a charm.