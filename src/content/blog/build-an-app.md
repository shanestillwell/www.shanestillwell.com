---
date: 2020-04-24 13:17:27
description: The fastest way to Senior Developer - Build an app
pubDate: '2020-04-24T13:17:27'
tags:
- PostgreSQL
- Rant
- JavaScript
- GitLab
- Node.js
- Programming
title: The fastest way to Senior Developer - Build an app
heroImage: '/images/build-an-app-thumbnail.jpg'
---

> I'm asked all the time **"How can I learn all this stuff?"**. The simple answer is 'build an app'

## First a little story

The year was 2016. I was a Team Lead at a large clothing company and my team just completed rolling out [Apple Pay on the Web](https://developer.apple.com/documentation/apple_pay_on_the_web) for this company. A month later, most of the contractors at this company were let go; for reasons. I found myself looking for a job, and was out of work for a month. During this time out of work, I built a web app service (SaaS) for leveraging Apple Pay on the Web. It was going to be the PayPal of Apple Pay, sort of. This service made it easy to add Apple Pay buttons to your site. I thought it was a great idea, but guess what, no one else did and I failed to get a single person to sign up. 🙁

Even though I didn't make it rich, in that one month's time working on that app, I really learned many things I needed for future projects I would lead. I learned processing payments with Stripe, deploying code to production using [Docker](/tags/docker), leveled up my [Postgres](/tags/postgresql) skills dramatically, and dove head first into [GraphQL](/tags/graphql). Building that little app that no one used taught me many skills and lessons I would later use at much larger companies.


## Build a Real App

The catch from here is what kind of app to build??? 🤔 Everyone gets tripped up on this step, but the reality is, *it doesn't matter*. That's right, the actual app is not that important. Some ideas would be: 

* A contact manager
* A Calendar or Scheduler
* An invoicing app
* Send an SMS text to someone via the web
* Send out an email via the web
* A text overlay on an image app


I can hear you thinking now **"But they've already been done"**. Google, Facebook, Twitter, Mailchimp, Imgr, etc have all created these apps and you'll never be able to compete with them. **You're missing the point**.

> The idea is irrelevant, the process is EVERYTHING.

## The Process

For clarification, I'm speaking in general terms between a web app (a sophisticated website) and a mobile app. I think either applies. The process of building an app is more than just a simple understanding of the requirements, but all the nuances baked into the app from start to finish. You'll make mistakes, and that's good. You'll quickly discover bad patterns, and that's good. You'll have to deal with your own [Big Ball of Mud](http://laputan.org/mud/), and that's good. Now lets' talk about the requirements.

## The Requirements

What is an app? Just a web page? An installable app in the App Store? Let's sets some ground rules for what I define an app includes. The final product should have these qualities.

1. **Authentication**: Users of your app need to sign in. You need to solve signing in, password resets, signing out, rejecting unauthenticated users to specific content.
2. **Access**: Similar to *Authentication*, need to distinguish between a guest to the app (or web app), a regular user, and possibly an admin or owner.
3. **Payment**: You may be tempted to skip this part, but don't. How is your app going to make money? You'll probably want to consider [Stripe](https://stripe.com)
4. **Deployment**: Getting the code from your computer to a place where people can use it. This should be 100% automated. I'm talking about [CI / CD](https://www.infoworld.com/article/3271126/what-is-cicd-continuous-integration-and-continuous-delivery-explained.html)
5. **Tests**: Automated tests that check your code for problems before it deploys.
6. **Data Storage**: Users, content, settings, and data all have to live somewhere and a data store or database is the answer. [Postgres](/tags/postgresql/) should be your first stop for consideration.

## Why is this the fastest way to becoming a Senior Developer?

Experience, plain and simple. Building an app will expose you to new concepts such as deploying your app to production. Signing up users for your app takes thought and consideration. What can a guest see? How will you split your app between guests and logged in users? What happens if a bad actor or hacker decides to do nasty things with your APIs?

Remember I said not to skip the *Payment* requirement? When you charge for your app you need to think about who you're charging? Are you charging a business to use your app, and that business will have many people using the app under the business account? Will you be charging on a per usage basis, so for every email sent you charge by the email? Deciding who to charge for your service seems simple on the surface, but can have deep impacting consequences.

Deployment to production will involve provisioning a server or services somewhere, initializing your data store. You'll have to contend with credentials in your app for your database and other services.

## Remember

We are NOT trying to invent an app that will make us rich. We just want to solve some problems for the sake of learning. Once you gain the knowledge of building, deploying, funding, and supporting a live app, you have all the tools to take on any position in this wonderful IT field. If no one signs up, that's fine. I built several apps I thought people would need and no one ended up signing up and taking the plunge. It doesn't matter. I learned and applied those skills to places that did pay off... big.


## I Now Dub Thee a Senior Developer 👑

What's in a title? They are pretty much fluff in my opinion. If you can build and deploy a real live app, you're Senior in my book. 😍

* Thank you to you [Kelly Sikkema](https://unsplash.com/@kellysikkema) for the [thumbnail image](https://unsplash.com/photos/Z9AU36chmQI) found on [Unsplash](https://unsplash.com)
