---
date: '2007-01-14T23:53:41-05:00'
description: Time Synchronization with ntpdate
id: 11
pubDate: '2007-01-14T23:53:41-05:00'
tags:
- Server Admin
title: Time Synchronization with ntpdate
---

To synchronize time on a linux machine via a cron job using ntpdate.
`
0 2 * * * /usr/sbin/ntpdate -u pool.ntp.org
`
ntpdate is the program that goes to the time server pool.ntp.org and updates the system time. The -u just tells it to use an unprivileged port.