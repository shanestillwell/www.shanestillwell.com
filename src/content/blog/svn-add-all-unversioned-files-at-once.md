---
date: '2009-08-03T03:27:34-05:00'
description: SVN add all unversioned files at once
id: 64
pubDate: '2009-08-03T03:27:34-05:00'
tags:
- SVN
- Programming
title: SVN add all unversioned files at once
---

If you want to add all the unversioned files at once to subversion, then you either have to type in all the paths or try this.

Add this to your .bash_profile
`
svn_add_all(){
svn status | grep "^?" | awk '{print $2}' | xargs svn add
}
`

Then typing `svn_add_all` will add all the files underneath your current working directory.

<div class="zemanta-pixie">![](http://img.zemanta.com/pixy.gif?x-id=62d41ef9-7c40-82d8-aff8-4fc7fbb98804)</div>