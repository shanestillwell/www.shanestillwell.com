---
date: '2008-06-27T14:27:10-05:00'
description: TextMate Cheat Sheet
id: 21
pubDate: '2008-06-27T14:27:10-05:00'
tags:
- Apple
title: TextMate Cheat Sheet
---

I create **Cheat Sheets** for many different things.  You actually gain more from creating them than actually using them.  
> The best way to learn something is to teach it

Inspired by gunther groenewege's [TextMate cheat sheet](http://www.g-design.net/textmate.pdf "") I decided to create my own and add some other shortcuts.  Since I'm a big advocate of Open Source. I'll post the OpenOffice version and the PDF version.

[TextMate Cheat Sheet (ODS)](http://www.ecitadel.com/files/docs/textmate_cheatsheet.ods)
[TextMate Cheat Sheet (PDF)](http://www.ecitadel.com/files/docs/textmate_cheatsheet.pdf)

Enjoy