---
date: '2009-05-05T23:33:03-05:00'
description: SVN ignore in all subdirectories
id: 50
pubDate: '2009-05-05T23:33:03-05:00'
tags:
- SVN
- Programming
title: SVN ignore in all subdirectories
---

`svn propset svn:ignore '*' .`
[http://www.heavymind.net/2007/08/29/the-nightmare-has-returned-with-new-name-svnignore/](https://web.archive.org/web/20160614112243/http://www.heavymind.net/2007/08/29/the-nightmare-has-returned-with-new-name-svnignore/)