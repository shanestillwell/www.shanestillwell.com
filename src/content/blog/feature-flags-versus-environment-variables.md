---
date: 2021-05-28 16:44:50
description: Feature Flags v Environment Variables
pubDate: '2021-05-28T16:44:50'
tags:
- Programming
title: Feature Flags v Environment Variables
heroImage: '/images/feature-flags-versus-environment-variables-thumbnail.jpg'
---

At first glace, Feature Flags and Environment Variables might seem like they are similar, but as we'll see, they fulfill vastly different roles and conflating the two can be problematic.

To choose between the two

## Feature Flags
* Toggle behavior on the fly
* Independent of environment

## Environment Variables
* If need to decide during app startup
* More rigid between environments

Issue I had... I was starting Apollo Server

If it's a configuration you are passing to a service that is starting, then remote FF may not work. For example, I thought, "Let me have a FF toggle to turn GraphQL introspection on/off on the server".

## Problems.
1. This value cannot be dynamically changed, would take a server restart to pick up a new value.
1. This value is needed immediately on boot up. While using [Unleash](https://www.getunleash.io/), the remote connection to the server was not yet initialized when the Apollo server needed the configuration.

Photo by [Vladislav Klapin](https://unsplash.com/@lemonvlad?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/country-flags?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
