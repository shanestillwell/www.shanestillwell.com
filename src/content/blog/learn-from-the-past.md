---
date: 2019-07-15 20:53:46
description: Learn From the Past
pubDate: '2019-07-15T20:53:46'
title: Learn From the Past
heroImage: '/images/learn-from-the-past-thumbnail.png'
tags:
- Programming
---

Today we are going to look at software developers that have come before us, laying down important truths that we would do well to learn and heed. These old text are still as true today as when they were written, sometimes even more so.

Don't be fooled into thinking that we have entered a period of software dev that is new and different than 30 years ago. This is simply not the case, a new language or technology doesn't change the fundamental underpinnings of how we design and develop software. We still use functions, arrays, booleans, if/else, and all these require us to assemble them together using our minds. As long as humans are involved, these principles will be here to guide us.

**Chronological snobbery**

> is an argument that the thinking, art, or science of an earlier time is **inherently inferior** to that of the present, simply by virtue of its temporal priority or the belief that since civilization has advanced in certain areas, people of earlier periods were less intelligent. The term was coined by [C. S. Lewis](https://en.wikipedia.org/wiki/C._S._Lewis) and [Owen Barfield](https://en.wikipedia.org/wiki/Owen_Barfield), first mentioned by Lewis in his 1955 autobiographical work, [Surprised by Joy.](https://en.wikipedia.org/wiki/Surprised_by_Joy)

C.S. Lewis's term **Chronological Snobbery**, reveals the faulting thinking that somehow people in the past didn't have the same struggles as we do in this present age.

Let's take a look at some great books and articles that helped shaped software development over the years.

## [Mythical Man Month](https://www.amazon.com/gp/offer-listing/0201835959) (book)

Author: Frederick P. Brooks Jr  
Year: 1975

This book was published before I was born. It lays out some truths discovered while designing software that I've seen in my 20+ year career. The term Mythical Man Month is based on the idea that a person (*man*) can accomplish X number of lines of code in a *month*, which is a *myth*. It doesn't work out that way. Some really good truths you'll find in this book

1. Chapter 2 - The Mythical Man-Month. Adding new developers to a software project that is already late will make it even later. The premise is when you add a software developer to the team, they take time and energy away from the veteran team members. So instead of writing software, they are teaching the code base to the new developer on the team. Makes perfect sense when you think about it that way.

2. Chapter 5 - The Second System Effect. Rewriting a system will take longer than you expect. This echo's what Joel Spolsky says in [Things you should never do](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/) aka **Never rewrite your entire software base**. I've seen this on many occasions. You feel tempted to rewrite the whole app. This time you'll fix all the problems, this time you'll write it in some new shiny language, this time it'll have new features. But in the end, it takes waaaaaaay longer than you anticipated. Resist the urge to rewrite the whole app, instead, improve it incrementally.

3. Chapter 11 - Plan to Throw One Away. Seems contrary to the second system effect, but it's a bit different. When you are creating a totally new system, you're going to make a lot of mistakes while you learn the ins and outs how it should operate. The first time around may not be the best, so learn to iterate and fail fast.

4. Chapter 16 - No Silver Bullet. [Don't believe the hype](https://www.youtube.com/watch?v=LK8sxngSWaU). A new language is not going to solve all your problems. If anything, you'll just exchange one set of problems for another set. New technology will not be the answer. That is not to say some new technology will alleviate some pain points, this is very true. Think how [Docker](https://www.docker.com/) has made it so much easier to run different systems together and package up your app to run anywhere.

## [Joel Spolsky](https://www.joelonsoftware.com/) (blog)

> Joel was a prolific blogger 20 years ago. He's still pretty good, but I would argue he doesn't do a lot of programming as Founder of StackOverflow.

1. [Things you should never do](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/) - We already touched on this, but it bares repeating, never rewrite your whole app. Personally, I've been guilty of just this thing, many times. Usually on the frontend since a new cool framework comes out every week. One of my sites began life as an [Angular](https://angular.io/) app, was totally rewritten in [React](https://reactjs.org/), now I'm in the long process of rewriting it in [Vue](https://vuejs.org/). I don't learn. In your mind you think "This code base is a mess, I need to start over, but this time I'll do it all right. You won't. In reality, you're going to rewrite some front end apps, it's pretty much unavoidable, but be smart. Try to figure out what can be abstracted out or piece mealed. Small apps, it's not a problem. Big apps, you're asking for danger.

2. [The Joel Test](https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/) (2000) - This an ad hoc list that Joel came up with, but it's a good guideline that you can use to evolute the health of a company delivering software. Sadly, I've seen many companies check all these off, when in fact they really don't do them.
    1. Do you use source control?
    1. Can you make a build in one step?
    1. Do you make daily builds?
    1. Do you have a bug database?
    1. Do you fix bugs before writing new code?
    1. Do you have an up-to-date schedule?
    1. Do you have a spec?
    1. Do programmers have quiet working conditions?
    1. Do you use the best tools money can buy?
    1. Do you have testers?
    1. Do new candidates write code during their interview?
    1. Do you do hallway usability testing?

## [12 Factor App](https://12factor.net/) (~ 2011)

As web apps became more prevalent and the idea of writing apps that could scale without effort spread, we needed some guidelines to accomplish those goals. [Heroku](https://www.heroku.com/) (Adam Wiggins) wrote the 12 Factor app outlining some of the parameters of well designed apps. I remember coming across these guidelines many years ago as an accomplished PHP dev and not really understanding them. Yet, in Node.js, they make perfect sense. It just took some time for them to sink in as I adapted them into my work flow. The **Config** one is big in my opinion, I've seen too many devs storing creds in code that's checked into source control.

1. Codebase - One codebase tracked in revision control, many deploys
1. Dependencies - Explicitly declare and isolate dependencies
1. Config - Store config in the environment
1. Backing services - Treat backing services as attached resources
1. Build, release, run - Strictly separate build and run stages
1. Processes - Execute the app as one or more stateless processes
1. Port binding - Export services via port binding
1. Concurrency - Scale out via the process model
1. Disposability - Maximize robustness with fast startup and graceful shutdown
1. Dev/prod parity - Keep development, staging, and production as similar as possible
1. Logs - Treat logs as event streams
1. Admin processes - Run admin/management tasks as one-off processes

## [Teach Yourself Programming in Ten Years](https://www.norvig.com/21-days.html)

This is a simple guide to those that think they might want to learn a language in 24 hours. Think long term. There are many languages to learn and it takes years to master them. Go ahead and enjoy the journey, one step at a time.

## [The Big Ball of Mud](http://laputan.org/mud/) 

Software is a difficult animal to tame. This long essay identifies the pitfalls inherent in the process and gives suggestions to avoid or minimize some of the pain.

* BIG BALL OF MUD
* THROWAWAY CODE
* PIECEMEAL GROWTH
* KEEP IT WORKING
* SHEARING LAYERS
* SWEEPING IT UNDER THE RUG
* RECONSTRUCTION


## Bottom Line

Software development is still a young industry and it is difficult at scale. It evolves, but also has a rich history of well established principles. You can either learn these principles **The Hard Way™**, or find these nuggets of wisdom to guide your way to better software development.

![I did it again](/images/learn-from-the-past-2011.11.15_building_software.png "The life of a software engineer - Manu Cornet")
https://bonkersworld.net/building-software
