---
date: '2012-03-29T12:31:46-05:00'
description: MVC for Appcelerator Titanium (understanding Tweetanium)
id: 308
pubDate: '2012-03-29T12:31:46-05:00'
tags:
- iPhone
- Programming
title: MVC for Appcelerator Titanium (understanding Tweetanium)
---

[Tweetianium](https://github.com/appcelerator-titans/tweetanium) builds its own namespace that it operates under rather than calling code that looks like the following to open up a new window.
<pre lang="JavaScript">Ti.UI.createWindow({url:'somefile.js', importantdata: mydata });</pre>
This essentially uses different files to open new windows, it's manageable to point, but I had trouble with not having some sort of global variables I could call on.

## In walks [Tweetanium](https://github.com/appcelerator-titans/tweetanium) and [Struct](https://github.com/krawaller/struct)

These keep the entirety of the code in basically a name spaced object. I have to admit, it took me 2-3 times looking over the code to really get it. Let me try to help.

## app.js

![](/images/mvc-for-appcelerator-titanium-understanding-tweetanium-Screen-Shot-2012-03-27-at-9.10.10-AM.png "app.js")

1.  This is the app.js file
2.  We are including the main include file. As you'll see, files add new methods and properties to the global object, then include other sub files that will add more properties and methods.
3.  _tt.ui.createApplicationWindow()_ creates a Titanium window and is passed to the tt.app.mainWindow property. Then it's opened with _open()_

## /tweetanium/tweetanium.js

![](/images/mvc-for-appcelerator-titanium-understanding-tweetanium-tweetanium2.png "tweetanium2")

1.  This is the _/tweetanium/tweetanium.js_ file. The initial file in our application.
2.  Here we are creating the namespace _tt_, all properties and methods will extend this object.
3.  This is an anonymous self executing function that holds some initial methods. _tt.app_ is used to hold various properties such as _currentWindow_.
4.  Lastly we include sub files. **Notice** how the subfiles are named like their folders. So you have _/tweetanium/ui/ui.js_ and _/tweetanium/model/model.js_. You could name them anything, but in those respective files they include all the files in that folder, let's take a look.

## /tweetanium/ui/ui.js

![](/images/mvc-for-appcelerator-titanium-understanding-tweetanium-Screen-Shot-2012-03-29-at-7.14.25-AM.png "Screen Shot 2012-03-29 at 7.14.25 AM")

1.  Here we are in the _/tweetanium/ui/ui.js_ file.
2.  We now create a subobject called _tt.ui_ that will hold all the user interface properties and methods. Then we can add any utilities used by various interface functions

## There you go

Using this same waterfall approach to building the namespace, you can make the whole Titanium app in a surprisingly short amount of time. Once you get structure to your files, it's easier to stay organized.

Some further files to investigate would be the _/tweetanium/ui/styles.js_ and the _/tweetanium/ui/ApplicationWindow_, noticing that most windows have a corresponding _create****_ method that will create the object and had it back to the caller.

## Resources:

* https://github.com/appcelerator-titans/tweetanium
* https://blog.krawaller.se/posts/titanium-application-structure-learning-from-tweetanium/
