---
date: '2010-11-30T00:00:00-05:00'
description: Zend_Auth and RememberMe cookie sessions
id: 119
pubDate: '2010-11-30T00:00:00-05:00'
tags:
- Programming
title: Zend_Auth and RememberMe cookie sessions
---

It's bad enough that we make people pick a new username / password to access our site, but even worse is by default Zend_Auth sets a cookie that expires when the browser is closed.

Most sites have a "Remember Me" checkbox. This is handy so a user does not have to log into your site every time.  

To accomplish this, you simply add the following lines AFTER the users has been authenticated on your site.  For me, this happens in my Auth_Adapter Class.

`

    $session = new Zend_Session_Namespace('Zend_Auth');
    // Set the time of user logged in
    $session->setExpirationSeconds(24*3600);

    Zend_Session::rememberMe();

`

It's as simple as that.

References:
http://en.2developers.net/post/authorization_using_zend_auth.html