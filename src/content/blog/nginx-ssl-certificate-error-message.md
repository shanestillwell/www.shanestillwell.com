---
date: '2011-05-26T23:13:53-05:00'
description: Nginx SSL certificate error message "key values mismatch"
id: 128
pubDate: '2011-05-26T23:13:53-05:00'
tags:
- Server Admin
title: Nginx SSL certificate error message "key values mismatch"
---

When setting up a SSL certificate and chain file for Nginx, you need to combine them into one file.  If you combine them in the wrong order you'll get a message similar to the following.

`
SSL_CTX_use_PrivateKey_file(" ... /www.nginx.com.key") failed
   (SSL: error:0B080074:x509 certificate routines:
    X509_check_private_key:key values mismatch)
`

This means you either didn't combine them or you combined them in the wrong order.  To combine the two together just do something like this.

`
cat www.example.com.crt www.certificatechain.com.ca > www.example.com.combined.crt
`

Now you'll be happy as a lark.