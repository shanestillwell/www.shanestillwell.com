---
date: '2010-01-06T05:14:45-05:00'
description: Fighting the machine
id: 77
pubDate: '2010-01-06T05:14:45-05:00'
tags:
- Apple
title: Fighting the machine
---

So today I spent an ungodly amount of time trying to figure out why my [Magento](http://www.magentocommerce.com/ "Magento - Home  - eCommerce Software for Growth") module, that worked just fine on my local [MAMP](http://www.mamp.info/en/index.html "MAMP: Mac, Apache, MySQL, PHP") installation, didn't work on the Linux staging server.

I thought it was the cache, so I reloaded the cache, changed the cache backend, then I thought it might be a but, then I thought it might be the layouts.  The solution? Case. Mac is not case sensitive, Linux is. 

My module was named CatalogRequest and Magento is really strict on case for filenames, so a little rule of thumb. Avoid CamelCase in your module names.