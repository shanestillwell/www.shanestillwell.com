---
date: '2011-07-27T16:35:43-05:00'
description: Upgrading to OS X Lion [SOLVED Chrome issue ]
id: 130
pubDate: '2011-07-27T16:35:43-05:00'
tags:
- Apple
title: Upgrading to OS X Lion [SOLVED Chrome issue ]
---

I took the plunge on Monday and upgraded to Lion. I know better than to do an OS upgrade while I have a mountain of work on my plate, but oh well.

The upgrade itself was the easiest ever. Just buy it, download it, then it takes about 30 minutes to install. Done.

## What I like

*   The interface is cleaner
*   Mission control (aka Expose et. al) is a bit better
*   Full screen is nice.
*   Local Time Machine backups are nice

## What I don't like

*   The scrolling. I tried it for a whole day, but it doesn't feel natural at all, so I reverted back to the original scrolling direction.
*   Finder got rid of my "Macintosh HD" in the side bar. I'm sure I can get it back, but I need to go to the root directory once in a while. it was nice having it there.
*   It seems slower than Snow Leopard. Seems like my machine is running at a warmer than normal and slower than normal rate.

## What broke

I was using Zend Server CE as my local web / mysql server. That was utterly broken. Instead of just reinstalling right away I tried out the local web server following this write up by Rob Allen
http://akrabat.com/php/setting-up-php-mysql-on-os-x-10-7-lion/

Worked good, but it seemed really slow (more on that below).

So because of this apparent slowness I installed MAMP. It installed fine, but MAMP had problems resolving host names and doing network calls... Someone said it was a bug in the PHP version that MAMP ships. Anyway, it didn't solve the speed problem either (that should have been my first clue).

I looked into installing Nginx on Mac, but didn't have 'make' installed so I'll have to pass for now.

I redownloaded Zend Server CE and installed that. Worked fine, but it was still slow.

## Chrome is to blame

*   I use the local /etc/hosts file to resolve names for local development. For some reason Chrome under OS X Lion takes about 10 seconds to resolve the name when it has to use the hosts file. So every request was taking about 10 seconds... almost the whole time it was waiting for DNS to time out.
*   Once I opened Safari to do local development the local web server was as zippy as before.....
*   Now I just have to figure out why Chrome is taking so long to resolve a name that is in the local hosts file.
**[UPDATE]**
To fix the Chrome slow issue... I just needed to move each entry in the /etc/hosts file to its own line.

so instead of
`
127.0.0.1 www.example.com www1.example.com
`

It would be
`
127.0.0.1 www.example.com
127.0.0.1 www1.example.com
`

[http://superuser.com/questions/313128/lion-name-resolution-order](http://superuser.com/questions/313128/lion-name-resolution-order)