---
date: '2010-07-19T01:27:42-05:00'
description: Installing IonCube on Zend Server CE
id: 114
pubDate: '2010-07-19T01:27:42-05:00'
tags:
- Server Admin
- Magento
title: Installing IonCube on Zend Server CE
---

## ionCube

http://www.ioncube.com/
For better or worse, ionCube is an encryption method for those that want to protect their code. A pain the neck for system admins, but some want to protect their PHP code, whatever.

## Problem... Zend Optimizer +

Zend Optimizer+ does not play well with ionCube for whatever reason, so it's really picky on what order they get loaded during startup. It's important to put the configurations in the correct order, or it will just error out during start up.

## Installing ionCube

You'll want to grab the loader files from here
http://www.ioncube.com/loaders.php

I downloaded and unzipped them to <pre>/usr/local/ioncube</pre>

## Edit php.ini file

Since ionCube is a Zend Extension and not a Zend Module (what's the difference? I couldn't tell you, but there is a different) we need to open up the file <pre>/usr/local/zend/etc/php.ini</pre> and at the very bottom it should look something like this. 
```
; Local Variables:
; tab-width: 4
; End:
[Zend]
zend_extension=/usr/local/ioncube/ioncube_loader_lin_5.2.so
zend.install_dir=/usr/local/zend
zend.conf_dir=/usr/local/zend/etc
zend.ini_scandir=conf.d
```

Of course the important line is the ioncube line, it's important that it appears before the other lines.

## Big Note about versions

It wasn't immediately obvious to me, but you need to match your PHP version with the version of ionCube (You can tell I'm using PHP version 5.2).

## Verify install

After it's done you can restart the web server (if the web server doesn't start check your log files for clues). Then go to the Zend admin panel to see the PHP INFO page. You should see a section that looks like this....

![php-info-ioncube.png](/images/installing-ioncube-on-zend-server-ce-php-info-ioncube.png)

## BD... BD.. BD.. That's all folks

![porky-pig.jpg](/images/installing-ioncube-on-zend-server-ce-porky-pig.jpg)
