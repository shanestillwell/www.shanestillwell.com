---
date: '2015-12-22T17:23:00-05:00'
description: Using Amazon Route53/S3 to redirect your root domain
pubDate: '2015-12-22T17:23:00-05:00'
tags:
- AWS
title: Using Amazon Route53/S3 to redirect your root domain
---

## The Backstory
You may find yourself needing to redirect all traffic from your root domain `example.com`, otherwise known as the apex, to your real hostname `www.example.com`. Maybe you didn't know this, but you cannot use a CNAME on your apex domain. This will bite you in the butt when you try to use your root domain `example.com` with Heroku's SSL (HTTPS) service. Heroku will give you a hostname and tell you to create a CNAME to that hostname. However, this is not strictly possible. Some registrars can get around this by essentially providing you an HTTP redirect, but his is hack. In short, don't use your apex domain e.g. example.com, even though you see all the cool kids on the block doing it.


## [Just Do It!](https://www.youtube.com/watch?v=ZXsQAXx_ao0)

### Create an Amazon S3 Bucket
First you are going to create a bucket in Amazon S3. You can name it after your domain, but the name is immaterial. The important part is going to be select **Static Website Hosting**. In that section, you're going to select **Enable Website Hosting**. The **Index Document** is also immaterial, you can name it `foo.html`.

The important part is the **Redirection Rules**. You'll use this.

```
<RoutingRules>
    <RoutingRule>
        <Redirect>
            <Protocol>REPLACEME with either HTTP or HTTPS</Protocol>
            <HostName>REPLACEME with www.example.com</HostName>
            <HttpRedirectCode>301</HttpRedirectCode>
        </Redirect>
    </RoutingRule>
</RoutingRules>
```

You wont forget to actually replace the protocol and hostname sections will you?

### Create a Route53 Alias

Now, walk over to Amazon Route53. I'm totally assuming that you're using Route53 for your DNS records, otherwise, this whole article is pretty much pointless. Click on your root (apex) record. Now we want to select **Type: A IPv4 Address**, then you'll select **Alias: Yes**. For the **Alias Target** you will get a dropdown, you'll see one of them listed as **S3 Website Endpoints**, that's the S3 bucket you just set up. Select that.

Now **Save Record Set** and you're done.

## Recap

You created an S3 bucket that acts like a webserver, anything that hits this S3 bucket will be immediately redirected to your desired hostname. You configured your domain *example.com* to hit your S3 bucket.

## Caveat

* This will not work for HTTPS => HTTPS, only HTTP => HTTP(s). So `https://example.com` will not work, it just times out. This might be a deal killer for some in which case you might want to stick with using an A record for your apex domain by setting up a real server.
