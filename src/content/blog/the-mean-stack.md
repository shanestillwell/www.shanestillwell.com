---
date: '2013-06-12T02:13:04-05:00'
description: The MEAN Stack
id: 643
pubDate: '2013-06-12T02:13:04-05:00'
tags:
- AngularJS
- ExpressJS
- MongoDB
- Node.js
- Programming
title: The MEAN Stack
---

Maybe you're familiar with the **LAMP** stack? Linux, Apache, MySql, PHP. That's been a pretty common way to serve websites for many years (circa 1998?). But things have changed, technologies have changed, and it's not 1998 anymore. IE6 is dead, nested tables are dead, and the LAMP stack is the _Old and Busted_.

The _New Hotness_ is the **MEAN** stack, [MongoDB](http://www.mongodb.org/), [ExpressJS](http://expressjs.com/), [AngularJS](http://angularjs.org/), and [NodeJS](http://nodejs.org/). This new stack of technologies is what many new web apps are using these days.

## MongoDB

Ever since I started using MongoDB over a year ago I have not even considered using MySQL for a web project. I suppose it has its place, but the ease of storing your data as it really is instead of weird joins and relations makes a whole lot more sense to me. The beauty of MongoDB and NoSQL in general is the schema is regulated by the app. No more crazy schema migration scripts to keep your MySQL schema intact.

I should also mention the other **M** you'll want to use, when using NodeJS and MongoDB, and that's [MongooseJS](http://mongoosejs.com/). Mongoose makes working with MongoDB a breeze and adds so many convenient operations for querying, saving, updating, and deleting records.

## [ExpressJS](http://expressjs.com/)

I had to try to explain to a coworker the other day how the MEAN stack equates to the LAMP stack and I was at amiss to communicate it. In a nutshell, ExpressJS makes creating a REST API dead simple. Express handles requests, routes, and responses. Throw in middleware and you have a supremely easy way to whip out a REST based API in no time.

Express lets you break the functions for different operations into small reusable chunks of code. Then combine them as you need them for different routes. For example, maybe some of your website is protected by authentication, and other parts are not. The auth protected routes can just call an auth() function and BAM!!! User authentication is now required for that particular route. Just to be clear a route is defined as an HTTP verb (GET, POST) and a URL (/user/profile).

## AngularJS

New kid on the block is AngularJS. Developed for a few years by Misko, Igor, and Vojtia at Google, it's now just come into its own.  AngularJS is client side only and doesn't care what you're running on the backend. Rumor has it, the .Net crowd really loves AngularJS too, go figure.

AngularJS is a Single Page App (SPA). Once loaded into the browser it "fakes" page loads, by just loading different templates and changing the URL. This makes page loads extremely fast as all you're waiting for is the data for that page. The CSS, JS, and most of the HTML is already there in the browser. AngularJS updates the page for you, it detects changes in your data and will update the page on the fly to reflect this change. For example, if you have a form to update some information, AngularJS, will update it live on the page as the form is being filled out. Truly awesome.

## [NodeJS](http://nodejs.org/)

Let's not forget the workhorse of this whole stack, [NodeJS](http://nodejs.org/). Node is fast, efficient, and can do anything. Node's popularity lately has skyrocketed and for good reason, it's fast, modular, testable, and flexible. [Node](http://nodejs.org/) coupled with [Express](http://expressjs.com/) makes for a very powerful web server (hint: most people put Node behind Nginx)