---
date: '2008-07-30T15:46:42-05:00'
description: An Solution To Clearing Floats
id: 28
pubDate: '2008-07-30T15:46:42-05:00'
tags:
- CSS
- Design
title: An Solution To Clearing Floats
---

I used to clear floats by adding the CSS style `clear: both` to the element underneath the floating elements.  This was a pain because sometimes you had to add a `
` tag under it with the `clear: both` set. Then while searching for a better way (there is always a better way) to clearing floats I found the [overflow](http://www.sitepoint.com/blogs/2005/02/26/simple-clearing-of-floats/ "SitePoint Blogs &raquo; Simple Clearing of Floats") rule.

Essentially, just add 
`
overflow: hidden
` 
to the parent of the floating elements. That will clear the floats so that elements under the floats will be cleared. Excellent!