---
date: '2009-05-05T23:16:53-05:00'
description: '[Solved] Samba ''tree connect failed: NT_STATUS_BAD_NETWORK_NAME'''
id: 51
pubDate: '2009-05-05T23:16:53-05:00'
tags:
- Server Admin
title: '[Solved] Samba ''tree connect failed: NT_STATUS_BAD_NETWORK_NAME'''
---

I just reinstalled a new machine and this time left SElinux on.  We'll I should have know it would have caused problems with my Samba set up.

<span style="font-weight: bold;">Error</span>
`
Shane:~ shane$ smbclient -U admin //192.168.178.10/timemachine
Password: 
Domain=[POWEREDGE] OS=[Unix] Server=[Samba 3.0.33-3.7.el5]
tree connect failed: NT_STATUS_BAD_NETWORK_NAME
`

<span style="font-weight: bold;">Solution</span>

`setsebool -P samba_export_all_rw on`