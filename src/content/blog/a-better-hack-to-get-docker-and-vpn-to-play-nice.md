---
date: '2016-04-26T14:08:58-05:00'
description: A Better Hack to Get Docker and VPN to Play Nice
pubDate: '2016-04-26T14:08:58-05:00'
tags:
- Docker
title: A Better Hack to Get Docker and VPN to Play Nice
---

> If you've found this article, then you've banged your head against the problem of being on a restrictive VPN and using Docker at the same time. The culprit is usually Cisco AnyConnect or Junos Pulse.


## The Problem

You use [Docker](https://www.docker.com/) for development. For various reasons you need to connect to a VPN, but as soon as you do, Docker stops working. There are many solutions out there, some work, others do not. The bottom line is there is no elegant solution and this solution here is not elegant, but it will work. What's happening? Yeah, when you connect, AnyConnect blunders in, overwrites all your computer's routes to send them through the VPN tunnel. Luckily, it doesn't route localhost (127.0.0.1) to the tunnel. This is our backdoor to hack ourselves in.

## The Setup

My current setup involves using Docker Machine to create a [Parallels](https://www.parallels.com/) VM. I'm on a Mac, Window/Linux YMMV. VirtualBox should work just fine; VMWare, can't really say. Some really restrictive VPN that doesn't allow split traffic, like Cisco AnyConnect or Junos Pulse.

## The Hack

You'll want to setup your Docker Machine first and get your env setup `eval $(docker-machine env)`. Once you have your docker machine up. You'll want to set up a *Port Forwarding* rule in Parallels. Go to **Preferences > Networking**. Then you'll want to add a new rule like this

!['Port Forwarding Rule](/images/a-hack-to-get-docker-working-while-on-vpn-Screen-Shot-2016-04-11-at-10.11.56-AM.png)

"default" is the name of my VM


You need to start a container that will forward the HTTP port for docker to localhost. Just run this command
```
$(docker run sequenceiq/socat)
```

You can find out more on what is doing at https://hub.docker.com/r/sequenceiq/socat/

Now on the command line, you need to update your **ENVIRONMENT VARIABLES** to use this new localhost incantation. We'll be changing the `DOCKER_HOST` and `DOCKER_TLS_VERIFY`. We set `DOCKER_HOST` to your localhost version. Then we need to disable TLS verification with `DOCKER_TLS_VERIFY`.

```
export DOCKER_HOST=tcp://127.0.0.1:2375 && \
export DOCKER_TLS_VERIFY="" && \
export DOCKER_CERT_PATH=""
```

Now you can connect to your restrictive VPN\* with `docker ps`.


## Caveats

1. You should have your VM up and running and have Docker-Machine env set in your terminal
1. Any ports you want to connect to on your docker will need to be port forwarded with Parallels.

## Notes

1. This will all be obsolete when [Docker for Mac](https://blog.docker.com/2016/03/docker-for-mac-windows-beta/) is released to the general public. Can't wait? Sign up for their [private beta](https://beta.docker.com/form)
