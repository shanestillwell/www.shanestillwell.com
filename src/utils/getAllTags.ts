/* eslint-disable */
import { getCollection } from 'astro:content';
import type { MDXInstance } from 'astro';
import { slugify, deslugify } from './slug';

interface Post {
  title: string;
  description: string;
  pubDate: Date;
  updatedDate: Date | null;
  heroImage: string | null;
  tags: string[];
}

export async function getBlogPosts () {
  return (await getCollection("blog", ({ data }) => {
    return import.meta.env.PROD ? data.draft !== true : true;
  })).sort(
      (a, b) => b.data.pubDate.valueOf() - a.data.pubDate.valueOf()
    )
}

export function getAllTags(posts: MDXInstance<Post>[] = []) {
  const allTags = new Set<string>();
  posts.forEach((post) => {
    post.data?.tags?.map((tag: string) => allTags.add(tag.toLowerCase()));
  });
  return [...allTags];
}

export const getTaxonomy = async (collection: string, name: string) => {
  const singlePages = await getCollection(collection);
  const taxonomyPages = singlePages.map((page) => page.data[name]);
  let taxonomies = [];
  for (let i = 0; i < taxonomyPages.length; i++) {
    const categoryArray = taxonomyPages[i];
    for (let j = 0; j < categoryArray.length; j++) {
      taxonomies.push({
        name: categoryArray[j],
        slug: slugify(categoryArray[j]),
      });
    }
  }
  const taxonomy = [...new Set(taxonomies)];
  return taxonomy;
};

export const getSinglePage = async (collection: any) => {
  const allPage = await getCollection(collection, ({ data }) => {
    return import.meta.env.PROD ? data.draft !== true : true;
  });
  return allPage.filter((data) => data.id.match(/^(?!-)/));
};

export const taxonomyFilter = (posts: any[], name: string, key: any) =>
  posts.filter((post) => post.data[name].map((name: string) => deslugify(name.toLowerCase())).includes(deslugify(key.toLowerCase())));

export function getPostsByTag (posts: any[], tagName: string) {
  return posts.reduce((memo, post) => {
    if (!post?.data?.tags.length) return memo
    if (post.data.tags.some(tag => tag.toLowerCase() === tagName.toLowerCase())) {
      memo.push(post)
    }
    return memo
  }, [])
}
