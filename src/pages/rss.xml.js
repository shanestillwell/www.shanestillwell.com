import path from 'path'
import rss from '@astrojs/rss';
import { getBlogPosts } from '@utils/getAllTags';
import config from '@config/config.json';

export async function GET(context) {
  const posts = await getBlogPosts()
  return rss({
    title: config.site.title,
    description: config.site.description,
    site: context.site,
    items: posts.map((post) => ({
      ...post.data,
      link: `/${post.slug}/`,
      // To get an image in RSS
      // customData: `<enclosure url="${new URL(
      //   path.join(
      //     config.site.base_url,
      //     '/images/',
      //     post.frontmatter.heroImage,
      //   ),
      // )}" length="5000" type="audio/mpeg" />`,
    })),
  });
}
