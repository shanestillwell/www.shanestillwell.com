---
title: Writing Notes
date: 2017-09-20T22:02:49-05:00
layout: ../layouts/Markdown.astro
---

> A general scratch pad for some writing notes

## Articles that have helped my writing

* [The Two Minutes It Takes To Read This Will Improve Your Writing Forever](https://medium.com/an-idea-for-you/the-two-minutes-it-takes-to-read-this-will-improve-your-writing-forever-82a7d01441d1)
* [Words To Avoid in Educational Writing](https://css-tricks.com/words-avoid-educational-writing/)
* [Clumsy Phrases to Avoid](https://www.oxford-royale.co.uk/articles/clumsy-phrases-avoid.html)
* [Words to Avoid](https://darlingmionette.deviantart.com/art/Words-To-Avoid-152886782)

## Tools for Writing Better

* [Proselint: A linter for prose](https://github.com/amperser/proselint)
  * A command line utility to search files and output issues found
  * To install `brew install proselint`
* [Wordy](https://github.com/reedes/vim-wordy)
  * A vim plugin to uncover problems in your writing
* [LanguageTool](https://languagetool.org/)
  * A browser plugin that will suggest word / grammar corrections
  * Very handy since I'm such a terrible spellllller

## Blogging

* [A Blogging Style Guide](https://robertheaton.com/2018/12/06/a-blogging-style-guide/)
