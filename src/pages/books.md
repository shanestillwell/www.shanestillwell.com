---
title: Books I've Read
layout: ../layouts/Markdown.astro
---
> Not a comprehesive list, but one I try to keep as a record of my personal development

## Meditations by Marcus Aurelius
> Thoughts about life and being a stoic

*Marcus Aurelius*

date: 2022-12-12

Mostly listened to this casually while I exercised. It provided some thoughts about how life is to be lived.

----

## Crime and Punishment
> How a criminal mind justifies its actions

*Fyodor Dostoevsky*

date: 2022-12-12

Started, but only half way through. Need to circle back and read.

----

## Cues

> Master the Secret Language of Charismatic Communication

*Vanessa Van Edwards*

date: 2022-11-12

How your voice and behavior give off messages and how to read the cues of others. It covers voice inflections, your hands, eyes, eye brows, and more.

----

## 12 Rules for Life
> Common sense rules to guide your life and rise above the chaos.

*Jordan B. Peterson*

date: 2022-10-10

1. Stand up straight with your shoulders straight
2. Treat yourself like someone you are responsible for helping
3. Befriend people who want the best for you
4. Compare yourself to who you were yesterday, not the useless person you are today
5. Do not let your children do anything that makes you dislike them
6. Set your house in order before you criticise the world
7. Pursue what is meaningful, not what is expedient
8. Tell the truth. Or at least don’t lie
9. Assume the person you are listening to knows something you don’t
10. Be precise in your speech
11. Do not bother children while they are skateboarding
12. Pet a cat when you encounter one in the street

----

## The 5am Club
> Own your morning, elevate your life

*Robin Sharma*

date: 2022-09-10


#### Wake up every morning at 5am.

1. Spend 20 Minutes Exercising
1. Spend 20 Minutes Meditating
1. Spend 20 Minutes Learning

----

## Outliers
> Premise: At first glance, it looks like hard work is the result of success, but if you look deeper there are more ingredients to account for.

*Malcolm Gladwell*

date: 2022-01-15

#### What I learned

In the last 20 years, there was a lot of effort to improve the communication between pilots and co-pilots. This has resulted in almost zero plane crashes. Most crashes were a result of a number of things gong wrong, but miscommunication was the major factor.

----

## David and Goliath
> Premise: the perceived weakness could be a hidden Strength

*Malcom Gladwell*

date: 2021-12-15


#### What I learned

In WW2, England was fearing the eventual bombings that would come to London. They thought if the city was bombed by Germany, it would result in mass hysteria.

To the contrary, it created three types of groups.

1. The dead: If a bomb fell on your neighbor's house, they would likely die. The thing to keep in mind, the dead don't talk and have no influence on the living.
1. The Near Miss: If a bomb fell on your neighbor's house, you might get hurt, but you would have survived. Having survived, you are now more emboldened, thinking that you can survive anything.
1. The Remote: If a bomb fell on the house down the street, it wouldn't really affect you at all.

They found that people started to almost ignore the sirens. Kids playing in the streets didn't stop because the bombers were coming. The exact opposite of what they feared.

----

## Leonardo da Vinci
*Walter Isaacson*

date: 2021-04-10


----

## Never Split The Difference
> Negotiating As If Your Life Depended On It

*Chris Voss*

date: 2021-04-01


----

## Mastery
> The Keys to Success and Long-Term Fulfillment

*George Leonard*

date: 2021-03-20


----

## Man's Search For Meaning

*Viktor Frankl*

date: 2021-03-15

----

## The Richest Man in Babylon

*George S. Clason*

date: 2021-03-10

----

## The "Like" Switch
> An Ex-FBI Agent's Guide to Influencing, Attracting, and Winning People Over

*Jack Schafer*

date: 2021-02-10


### Some notes

#### The Friendship Formula

1. Proximity: In the same geographical space as the person you want to get to know.
2. Frequency: How often you are in the same space.
3. Duration: How long you are in the same space together.
4. Intensity: The level of subjects you discuss.

#### Proximity:


#### Observations

When you first meet someone, lightly touching them on the arm often times builds a positive view of the person you met of yourself. I said arm, NOT the hand. Hands are intimate and will not be taken positive.

----

## Benjamin Franklin
> An American Life

*Walter Isaacson*

date: 2021-01-01




----

## Blink
> The Power of Thinking Without Thinking

*Malcolm Gladwell*

date: 2020-11-15


----

## Talking to Strangers
> What We Should Know About the People We Don’t Know

*Malcolm Gladwell*

date: 2020-11-01


---- 

## Sphere

*Michael Crichton*

date: 2020-10-15


---- 

## Radical Candor
> Be a Kickass Boss Without Losing Your Humanity
*Kim Malone Scott*

date: 2020-09-20

## Predictably Irrational
*Dan Ariely*
> Humans are funny creatures, some things can manipulate our descision process

----

## A Manual for Living
*Epictetus*

---- 

## How to Win Friends and Influence People
*Dale Carnegie*

The worst title for one of the best books out there. The title makes it sound like "How to manipulate people", but this couldn't be further from the book. The premise is how to get your mind off your self for two minutes and take a general interest in people.

To let people know you like them:
1. Smile
1. Remember their name
1. Ask them about their experiences
1. Don't argue
1. Don't complain

----

## Reading Smart

The best book I've found on the techniques of reading

1. Learn about eye saccades
1. Learn to not use your finger
1. Learn to adjust reading to content
