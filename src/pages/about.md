---
title: About Shane
date: '2010-12-07T15:16:41'
pubDate: '2010-12-07T15:16:41'
layout: ../layouts/Markdown.astro
---

* **Name**: Shane A. Stillwell
* **What I do most days**: JavaScript {[Vue.js](https://vuejs.org/), [React](https://react.dev/), [Node.js](https://nodejs.org/en/)} and Golang programming, [GraphQL](https://graphql.org/), [Docker](https://www.docker.com/), [PostgreSQL](https://www.postgresql.org/), [GitLab](https://about.gitlab.com/)
* **Editor**: [Vim](https://neovim.io/), for life
* [Portfolio](/portfolio/)
* [Writing](/writing/)
* [Books](/books/)
* [Inspiration](/inspiration/)
* **Activities**: Camping, Mountain Biking, Hiking.
* **What's important**: Family, everything else is a distant second
* **Places I've Visited**:
  * Auckland, New Zealand: Beautiful
  * Berlin, Germany: Good food, great culture
  * Rome, Italy: My favorite place. So much history
  * Paris, France: So much to see and do
  * Haiti: Tropical, friendly people, lots of poverty 😢
  * Alaska: Remote. Wilderness.
* **My Life Hacks**:
  * Buy a dozen pair of the same exact socks. You'll never have to match socks again.
  * Wooden hangers, get rid of those lame wire or plastic ones.
  * Put a [portable air compressor](https://www.amazon.com/Slime-40051-Digital-Tire-Inflator/dp/B074XHFN6Y?&_encoding=UTF8&tag=shanestillwel-20&linkCode=ur2&linkId=2421824ad6bdbbfbd9cb6feb1be304da&camp=1789&creative=9325) in your car. Beats trying to find a gas station when you have a lower tire.
  * I put my [Sam's Club Credit Card](https://www.samsclub.com/content/credit) in the car and earn 5% cash back on all gas purchases. It adds up.
  * Map your `CAPS LOCK` key to be an `ESC` key. Who uses CAPS LOCK anyway, except by accident?
  * Anything [Maria Kondo](https://konmari.com/) says.

* **Software I use**:
  * [Telegram chat](https://t.me/hapiwandrr)
  * [RayCast](https://www.raycast.com/): Launcher / General desktop utility
  * [TripMode](https://www.tripmode.ch/): When I tether to my phone, I can limit which apps can use the internet.
  * [Notion](https://www.notion.so/): Keep track of everything
  * [Arq](https://www.arqbackup.com/): Backups on my terms. Secure, flexible.
  * [TablePlus](https://tableplus.com/): The best Postgres GUI
  * [Fastmail](https://www.fastmail.com/): Solid email hosting
  * [See more](/what-apps-i-use)

