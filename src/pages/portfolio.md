---
title: Portfolio of Shane A. Stillwell
date: 2010-12-07 15:16:41
layout: ../layouts/Markdown.astro
---

> The illustrious portfolio of Shane A. Stillwell

## Publications

### InfoQ Article on React + GraphQL
I was approached by [InfoQ](http://infoq.com) to write an article of my choosing. At the time, I was knee deep in figuring out all the intricacies of GraphQL. So I wrote the article [Turbocharge React with GraphQL](https://www.infoq.com/articles/turbocharge-react-graphql). Like always, writing the article helped me understand the topic that much better.

### Packt Publishing Video Course
I developed a 4 hour video course for Packt Publishing called [Mastering MEAN Web Development: Expert Full Stack JavaScript](https://www.packtpub.com/web-development/mastering-mean-web-development-expert-full-stack-javascript-video). The course walks a new user into creating an app using Angular2, Node.js, Express, and MongoDB. There are many more items that are explain in the video such as Webpack, Font Awesome, Bootstrap, NPM, Mocha tests, JWT, Worker processes, security, and much more. You can watch a [sample video](https://www.packtpub.com/mapt/video/Web%20Development/9781785882159/8558/8559/The%20Course%20Overview).


## Speaking Engagements (UPDATEME)

### [MidwestJS](https://midwestjs.netlify.app/)

In 2014, gave a talk on using Browserify in your apps. During the talk I explain CommonJS some Angular concepts and few other tidbits. The [video](https://www.youtube.com/watch?v=A2wRXnCpqgI) is pretty old and the audio quality is terrible.

### Cloud Develop

On August 15th, 2013 in Columbus, OH; I spoke about the **The Three Amigos: AngularJS, Node.js, and Heroku**. This talk involved an introduction to Node.js and Heroku, but focused a lot on AngularJS.

You can view the slide deck at
http://www.slideshare.net/shanestillwell/three-amigos

### [Twin Ports Web Pros](http://www.meetup.com/Twin-Ports-Web-Professionals/)

September 26, 2013 I talked about AngularJS, pros and cons.
http://www.meetup.com/Twin-Ports-Web-Professionals/events/140890882/

### [NodeMN](http://www.meetup.com/NodeMN/)

In January, 2014 I did a lightning talk at NodeMN (Node.js Meetup in Minneapolis) about [JSON Web Tokens](http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html).

## Projects of mild interests

### GoScouter
https://www.goscouter.com
Pesonal site that uses React, Node.js, GraphQL, and PostgreSQL. This site is used by more than 1000 youth organizations like the Boy Scouts to track money raised by fundraisers.

## Others I need to expand on

* Divine.ly
* Brng.it
* Two Minute Feedback
* PlayingForChange Day
* Crowdseed
* Faxxboch
* Under Armour
* Meijer
* Quri
* Go Apple Pay

## My Resume Online (a little dated)

https://bit.ly/sstillwell

## Please visit my [Github](https://github.com/shanestillwell) and [LinkedIn](http://www.linkedin.com/in/shanestillwell) profiles
