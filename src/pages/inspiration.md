---
title: Inspiration
date: 2010-12-07 15:16:41
layout: ../layouts/Markdown.astro
---

> Bookmarks to great content


## Randy Pausch "The Last Lecture"
> Diagnosed with terminal cancer, still living life  to the fullest. Watch the video to see how he shares his zest for life.

[![The Last Lecture](https://img.youtube.com/vi/j7zzQpvoYcQ/0.jpg)](https://www.youtube.com/watch?v=j7zzQpvoYcQ)

---

## Admiral William H. McRaven
> Make your bed

[![Make Your Bed](https://img.youtube.com/vi/pxBQLFLei70/0.jpg)](https://www.youtube.com/watch?v=pxBQLFLei70)

## Patrick Winston "How To Speak"

[![How to speak](https://img.youtube.com/vi/Unzc731iCUY/0.jpg)](https://www.youtube.com/watch?v=Unzc731iCU)

--- 

## Epictetus: The Manual For Living

> Simple advice to live a simple life free from vanity and extravagance.

https://www.amazon.com/dp/0062511114

## More to investigate


### Shamelessly ripped off from https://news.ycombinator.com/item?id=19651032

- Li Ka-Shing, Tips on Life. My takeaway: invest in yourself. https://addicted2success.com/success-advice/asias-richest-man-li-ka-shing-shares-advice-for-young-entrepreneurs/
- Randy Pausch, The Last Lecture. My takeaway: don't limit yourself https://www.youtube.com/watch?v=j7zzQpvoYcQ
- Guy Kawasaki, Make Meaning. Aimed for startups, but equally applicable in life. My takeaway: Have the proper motivations for doing things. https://www.youtube.com/watch?v=lQs6IpJQWXc
- Admiral William H. McRaven, Make your bed. My takeaway: if you want to change the world, start by changing yourself. https://www.youtube.com/watch?v=pxBQLFLei70
- David Foster Wallace, This is Water. My takeaway: Take a big picture view, and don't sweat the small stuff. https://fs.blog/2012/04/david-foster-wallace-this-is-water/
- Man's Search for meaning. My takeaway: We make meaning. https://www.goodreads.com/book/show/4069.Man_s_Search_for_Meaning
- Idiot wind by Bob Dylan My takeaway: Your reaction to things can be just as bad as the actual thing. https://songmeanings.com/songs/view/57330/
- Stoicism and Marcus Aurelius, Seneca. My takeaway: Focus on what matters. https://www.goodreads.com/book/show/5617966-a-guide-to-the-good-life
  - (read this one first for a good foundation) https://www.goodreads.com/book/show/97411.Letters_from_a_Stoic
  - https://www.goodreads.com/book/show/30659.Meditations
- Buddhism. Our takeaway: Meditation is important, the doctrine of emptiness (not nihilism!) is super interesting.
  - https://www.goodreads.com/book/show/104949.How_to_See_Yourself_As_You_Really_Are
  - https://en.wikipedia.org/wiki/Dhammapada
  - https://www.amazon.com/Pocket-Dalai-Lama-Shambhala-Classics/dp/1590300017
  - https://www.goodreads.com/book/show/25942786-the-mind-illuminated
  - https://www.goodreads.com/book/show/3346233-buddhism
- Leonardo Da Vinci My take away: There is a beautiful intersection between tech and art. Great things could not exist without a little of both. https://www.goodreads.com/book/show/34684622-leonardo-da-vinci
- The Alchemist My takeaway: It's not the destination it's the journey. https://www.goodreads.com/book/show/865.The_Alchemist
- Zen and the Art of Motorcycle Maintenance. My takeaway: Seek out quality, whatever the hell that is! https://www.goodreads.com/book/show/629.Zen_and_the_Art_of_Motorcycle_Maintenance
- Why we sleep. My takeaway: go the fuck to bed. https://www.goodreads.com/book/show/34466963-why-we-sleep
- Anti-Cancer Diet. My takeaway: don't eat shit. https://www.goodreads.com/book/show/1886829.Anticancer_A_New_Way_of_Life
- Non-violent communication. My takeaway: You can (and should) communicate your needs respectfully. https://www.goodreads.com/book/show/560861.Non_Violent_Communication
- Long Walk to Freedom, Nelson Mandela. My takeaway: There is a difference between institutionalized action and individual action. Forgiveness is the only path forward. https://www.goodreads.com/book/show/318431.Long_Walk_to_Freedom
- A people's history of the US. My takeaway: Things are getting better. https://www.goodreads.com/book/show/2767.A_People_s_History_of_the_United_States
