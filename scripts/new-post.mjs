import * as TitleCase from 'title-case'
import lodash from 'lodash'
import minimist from 'minimist'
import { existsSync, writeFileSync } from 'fs'
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));
const argv = minimist(process.argv.slice(2), {})
const PATH = `${__dirname}/../src/content/blog`
const { kebabCase } = lodash
const title = TitleCase.titleCase(argv._.join(' '))
const filename = `${kebabCase(title)}.md`
const now = new Date()
const fullPathFile = `${PATH}/${filename}`

const template = `---
title: ${title}
description: ${title}
pubDate: ${now.toISOString()}
tags: []
heroImage: /images/blog-placeholder.jpg
draft: true
---


`

if (existsSync(fullPathFile)) {
  console.log('File Already Exists!!!!')
  console.log('=========================')
  console.log(filename)
  console.log('=========================')
  console.log('EXITING WITHOUT WRITING')
  process.exit(1)
}

writeFileSync(fullPathFile, template)

console.log('wrote out:', filename)
