import * as TitleCase from 'title-case'
import lodash from 'lodash'
import minimist from 'minimist'
import { existsSync, writeFileSync } from 'fs'
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const { kebabCase } = lodash
const __dirname = dirname(fileURLToPath(import.meta.url));
const argv = minimist(process.argv.slice(2), {})
const now = new Date()
const CONTENT_DIR = 'src/content/snip'
const PATH = `${__dirname}/../${CONTENT_DIR}`
const title = getTitle(argv._)
const parts = getTimeParts(now)
const filename = getFilename(title, parts)
const fullPathFile = getFullPath(PATH, filename)

const template = `---
title: ${title}
description: ${title}
pubDate: ${now.toISOString()}
tags: []
---


`

if (existsSync(fullPathFile)) {
  console.log('File Already Exists!!!!')
  console.log('=========================')
  console.log(filename)
  console.log('=========================')
  console.log('EXITING WITHOUT WRITING')
  process.exit(1)
}

writeFileSync(fullPathFile, template)

console.log('wrote out:', fullPathFile)

function getTitle (args = []) {
  const input = args.join(' ')
  return TitleCase.titleCase(input)
}

function getFilename(title, timeParts = {}) {
  const { pubYear, pubMonth, pubDay } = timeParts
  return `${pubYear}-${pubMonth}-${pubDay}-${kebabCase(title)}.md`
}

function getTimeParts(now) {
  const pubYear = String(now.getFullYear()).padStart(4, '0');
  const pubMonth = String(now.getMonth() + 1).padStart(2, '0');
  const pubDay = String(now.getDate()).padStart(2, '0');
  return { pubYear, pubMonth, pubDay };
}

function getDirectory(path = '', timeParts = {}) {
  const { pubYear, pubMonth, pubDay } = timeParts
  return `${path}/${pubYear}/${pubMonth}/${pubDay}`
}

function getFullPath(directory, filename) {
  return `${directory}/${filename}`
}
